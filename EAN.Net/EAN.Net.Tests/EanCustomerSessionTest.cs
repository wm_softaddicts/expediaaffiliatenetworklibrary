﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EAN.Net.Arguments;
using EAN.Net.Arguments.Common;
using EAN.Net.Arguments.Response.HotelList;
using EAN.Net.Tests.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;

namespace EAN.Net.Tests
{
	[TestClass]
	public class EanCustomerSessionTest
	{
		private static readonly DateTime ArrivalDate = DateTime.Now;
		private static readonly DateTime DepartureDate = DateTime.Now + TimeSpan.FromDays(1);

		[TestMethod]
		public void TestFillCommonRequestParameters()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			var request = new RestRequest("resource", Method.GET);

			session.FillCommonRequestParameters(request);

			Assert.AreEqual(request.Parameters.Count, 9);
		}

		private static HotelListResponse GetHotelList(EanCustomerSession session, int numberOfResults = 20)
		{
			var rooms = new List<Room>
			{
				new Room {NumberOfAdults = 2, ChildAges = {3, 5}},
				new Room {NumberOfAdults = 3, ChildAges = {1, 2, 5}}
			};

			var searchOptions = new HotelListSearchByAddressOptions
			{
				CountryCode = "US",
				StateProvinceCode = "NY",
				City = "New York"
			};

			return session.GetHotelList(ArrivalDate, DepartureDate, rooms, searchOptions, 
				isIncludeDetails: true, isIncludeHotelFeeBreakdown: true, numberOfResults: numberOfResults);
		}

		[TestMethod]
		public void TestGetHotelList()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			var hotelListResponse = GetHotelList(session);

			Assert.IsNotNull(hotelListResponse);
			Assert.IsFalse(hotelListResponse.IsError);
		}

		[TestMethod]
		public void TestGetHotelListNextResults()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			var hotelListResponse = GetHotelList(session, 1);

			var nextResponse = session.GetHotelListNextResults(hotelListResponse);

			Assert.IsFalse(nextResponse.IsError);
		}

		[TestMethod]
		public void TestGetHotelRoomAvailability()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			var hotelListResponse = GetHotelList(session);

			if (hotelListResponse == null || !hotelListResponse.HotelList.HotelSummaryList.Any())
				throw new Exception("Hotels are not found.");

			var rooms = new List<Room>
			{
				new Room {NumberOfAdults = 2, ChildAges = {3, 5}},
				new Room {NumberOfAdults = 3, ChildAges = {1, 2, 5}}
			};

			var hotelId = hotelListResponse.HotelList.HotelSummaryList.First().HotelId;

			var hotelRoomAvailabilityResponse = session.GetHotelRoomAvailability(ArrivalDate, DepartureDate,
				hotelId, rooms, isIncludeDetails: true, isIncludeHotelFeeBreakdown: true, isIncludeRoomImages: true);

			Assert.IsFalse(hotelRoomAvailabilityResponse.IsError);
		}

		[TestMethod]
		public void TestGetHotelItineraresByCreationDates()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			//var itineraryOptions = new HotelItineraryOptions(177608716, "test@email.com");

			var itineraryResponse = session.GetHotelItineraries(creationDateStart: ArrivalDate, creationDateEnd: DepartureDate);

			Assert.IsFalse(itineraryResponse.IsError);
		}

		[TestMethod]
		public void TestDepartureHotelItinerary()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			//var itineraryOptions = new HotelItineraryOptions(177608716, "test@email.com");

			var itineraryResponse = session.GetHotelItineraries(departureDateStart: ArrivalDate, departureDateEnd: DepartureDate);

			Assert.IsFalse(itineraryResponse.IsError);
		}

		[TestMethod]
		public void TestGetHotelItinerary()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			var itineraryResponse = session.GetHotelItineraries(creationDateStart: ArrivalDate, creationDateEnd: DepartureDate);
			var itinerary = itineraryResponse.ItineraryList.First();

			var result = session.GetHotelItineraries(itinerary.ItineraryId, email: itinerary.Customer.Email);

			Assert.IsFalse(result.IsError);
		}

		[TestMethod]
		public void TestGetRoomImages()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			var hotelListResponse = GetHotelList(session);

			if (hotelListResponse == null || !hotelListResponse.HotelList.HotelSummaryList.Any())
				throw new Exception("Hotels are not found.");

			var hotelId = hotelListResponse.HotelList.HotelSummaryList.First().HotelId;

			var result = session.GetRoomImages(hotelId);

			Assert.IsFalse(result.IsError);
		}

		[TestMethod]
		public void TestGetPaymentTypes()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			var result = session.GetPaymentTypes();

			Assert.IsFalse(result.IsError);
		}

		[TestMethod]
		public void TestGetPaymentTypesForKnownHotel()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			var hotelListResponse = GetHotelList(session);

			if (hotelListResponse == null || !hotelListResponse.HotelList.HotelSummaryList.Any())
				throw new Exception("Hotels are not found.");

			var hotel = hotelListResponse.HotelList.HotelSummaryList.First();

			var result = session.GetPaymentTypes(hotel.HotelId, hotel.SupplierType, hotel.RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfoList.First().RateType);

			Assert.IsFalse(result.IsError);
		}

		[TestMethod]
		public void TestPing()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			var echoValue = "test " + Guid.NewGuid();

			var result = session.Ping(echoValue);

			Assert.IsFalse(result.IsError);
			Assert.AreEqual(result.EchoValue, echoValue);
		}

		[TestMethod]
		public void TestGetHotelInformation()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			var hotelListResponse = GetHotelList(session);

			if (hotelListResponse == null || !hotelListResponse.HotelList.HotelSummaryList.Any())
				throw new Exception("Hotels are not found.");

			var hotelId = hotelListResponse.HotelList.HotelSummaryList.First().HotelId;

			var result = session.GetHotelInformation(hotelId);

			Assert.IsFalse(result.IsError);
		}

		[TestMethod]
		public void TestGetHotelAlternateProperties()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			var hotelListResponse = GetHotelList(session);

			if (hotelListResponse == null || !hotelListResponse.HotelList.HotelSummaryList.Any())
				throw new Exception("Hotels are not found.");

			var hotelId = hotelListResponse.HotelList.HotelSummaryList.First().HotelId;

			var rooms = new List<Room>
			{
				new Room {NumberOfAdults = 2, ChildAges = {3, 5}},
				new Room {NumberOfAdults = 3, ChildAges = {1, 2, 5}}
			};

			var result = session.GetHotelAlternateProperties(hotelId, ArrivalDate, DepartureDate, rooms, 175);

			Assert.IsFalse(result.IsError);
		}

		[TestMethod]
		public void TestGetLocationInfoByAddress()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			var searchOptions = new LocationInfoByAddressOptions
			{
				CountryCode = "US",
				StateProvinceCode = "WA",
				PostalCode = "98004",
				City = "Bellevue",
				Address = "333 108th ST NE"
			};

			var result = session.GetLocationInfo(searchOptions);

			Assert.IsFalse(result.IsError);
		}

		[TestMethod]
		public void TestGetLocationInfoByDestinationId()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			var searchOptions = new LocationInfoByDestinationIdOptions
			{
				DestinationId = "7E2ED1CF-2BE5-437F-95F4-4E2E2EA606F7"
			};

			var result = session.GetLocationInfo(searchOptions);

			Assert.IsFalse(result.IsError);
		}

		[TestMethod]
		public void TestMakeReservation()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			var hotelListResponse = GetHotelList(session);

			if (hotelListResponse == null || !hotelListResponse.HotelList.HotelSummaryList.Any())
				throw new Exception("Hotels are not found.");

			var hotel = hotelListResponse.HotelList.HotelSummaryList.First();
			var roomRateDetails = hotel.RoomRateDetailsList.RoomRateDetails;
			var rateInfo = roomRateDetails.RateInfos.RateInfoList.First();

			var room = rateInfo.RoomGroup.RoomList.First();
			var roomReservation = new RoomReservation
			{
				FirstName = "test",
				LastName = "testers",
				BedTypeId = roomRateDetails.BedTypes.BedTypeList.First().Id,
			};

			Mapper.DynamicMap(room, roomReservation);

			var chargeableRate = rateInfo.ChargeableRateInfo.Total ?? 0.0f;

			var reservationOptions = new ReservationOptions
			{
				Email = "test@yourSite.com",
				FirstName = "tester",
				LastName = "testing",
				HomePhone = "123567890",
				WorkPhone = "0987654321",
				CreditCardType = "CA",
				CreditCardNumber = "5401999999999999",
				CreditCardIdentifier = "123",
				CreditCardExpirationMonth = "11",
				CreditCardExpirationYear = "2015"
			};

			var addressOptions = new AddressOptions
			{
				CountryCode = "US",
				StateProvinceCode = "WA",
				PostalCode = "98004",
				City = "Bellevue",
				Address1 = "travelnow"
			};

			var result = session.MakeReservation(hotel.HotelId, ArrivalDate, DepartureDate,
				"17fbd5b6-5720-4354-a66c-d0c40a9ff272", roomRateDetails.RoomTypeCode, roomRateDetails.RateCode,
				new[] { roomReservation }, reservationOptions, addressOptions,
				chargeableRate, "E");

			//var result = session.MakeReservation(127092, new DateTime(2015, 9, 3), new DateTime(2015, 9, 4),
			//	"469e1aff-49de-4944-a64d-25d96ccde3aa", "200127420", "200706716",
			//	rooms, reservationOptions, addressOptions,
			//	106.17f, "E");

			Assert.IsFalse(result.IsError);
		}

		[TestMethod]
		public void TestCancelReservation()
		{
			var client = DataInitializer.CreateClient();
			var session = DataInitializer.CreateSession(client);

			var result = session.CancelReservation(177592654, "test@yourSite.com", "1235", CancellationReason.Other);

			Assert.IsFalse(result.IsError);
		}
	}
}
