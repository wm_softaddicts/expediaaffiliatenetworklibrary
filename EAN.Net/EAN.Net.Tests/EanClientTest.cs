﻿using System;
using System.Security;
using EAN.Net;
using EAN.Net.Tests.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EAN.Net.Tests
{
	[TestClass]
	public class EanClientTest
	{
		[TestMethod]
		public void TestGetSignature()
		{
			var time = new DateTime(1970, 1, 1);
			var signature = EanWebClient.CalculateSignature(ClientArgs.ApiKey, ClientArgs.SharedSecret, time);

			// http://devsecure.ean.com/files/md5.html
			Assert.AreEqual(signature, "58359e28edf056ef1dbb41eb5b15914f");
		}
	}
}
