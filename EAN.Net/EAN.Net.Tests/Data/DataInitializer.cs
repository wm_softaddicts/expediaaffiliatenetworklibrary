﻿namespace EAN.Net.Tests.Data
{
	public static class DataInitializer
	{
		public static EanWebClient CreateClient()
		{
			return new EanWebClient(ClientArgs.ClientId, ClientArgs.ApiKey, ClientArgs.SharedSecret) { IsDevelopmentMode = true };
		}

		public static EanCustomerSession CreateSession(EanWebClient client)
		{
			return new EanCustomerSession(client, CustomerSessionArgs.IpAddress, CustomerSessionArgs.UserAgent,
				CustomerSessionArgs.CurrencyCode);
		}
	}
}
