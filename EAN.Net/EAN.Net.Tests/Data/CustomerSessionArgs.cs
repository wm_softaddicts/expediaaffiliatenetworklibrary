﻿namespace EAN.Net.Tests.Data
{
	internal static class CustomerSessionArgs
	{
		public const string IpAddress = "127.0.0.1";
		public const string UserAgent = "Mozilla/4.0";
		public const string CurrencyCode = "USD";
	}
}
