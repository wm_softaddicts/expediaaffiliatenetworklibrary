﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using EAN.Net.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

#if DEBUG
[assembly: InternalsVisibleTo("EAN.Net.Tests.Helpers")]
#endif

namespace EAN.Net.Tests.Helpers
{
	[TestClass]
	public class DateTimeHelperTest
	{
		[TestMethod]
		public void TestGetUnixTimeStamp_StartingPoint()
		{
			var time = new DateTime(1970, 1, 1);
			var unixTime = DateTimeHelper.GetUnixTimeStamp(time);

			Assert.AreEqual(unixTime, 0);
		}

		[TestMethod]
		public void TestGetUnixTimeStamp_OneSecondPassed()
		{
			var time = new DateTime(1970, 1, 1, 0, 0, 1);
			var unixTime = DateTimeHelper.GetUnixTimeStamp(time);

			Assert.AreEqual(unixTime, 1);
		}
	}
}
