﻿using System.Collections.Generic;
using System.Linq;
using EAN.Net.Arguments.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;

namespace EAN.Net.Tests
{
	[TestClass]
	public class RestParameterFillerTest
	{
		[TestMethod]
		public void TestRoomsToArgumentsConversion()
		{
			var request = new RestRequest();

			var rooms = new List<Room>
			{
				new Room {NumberOfAdults = 2, ChildAges = {3, 5}},
				new Room {NumberOfAdults = 3, ChildAges = {1, 2, 5}}
			};

			RestParameterFiller.AppendParameter(request, rooms);

			Assert.AreEqual(request.Parameters.Count(p => p.Name.StartsWith("room")), 2);
		}
	}
}
