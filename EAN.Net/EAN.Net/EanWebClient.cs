﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using EAN.Net.Attributes;
using EAN.Net.Constants;
using EAN.Net.Enums;
using EAN.Net.Helpers;

#if DEBUG
[assembly: InternalsVisibleTo("EAN.Net.Tests")]
#endif

namespace EAN.Net
{
	public class EanWebClient
	{
		private EanApiExperience _apiExperience = EanApiExperience.PartnerWebsite;
		public const int ApiRevision = 28;

		public EanWebClient(long clientId, string apiKey, string sharedSecret)
		{
			ClientId = clientId;
			ApiKey = apiKey;

			Signature = CalculateSignature(ApiKey, sharedSecret, DateTime.Now);
		}

		public bool IsDevelopmentMode { get; set; }

		public EanApiExperience ApiExperience
		{
			get { return _apiExperience; }
			set { _apiExperience = value; }
		}

		public long ClientId { get; private set; }

		public string ApiKey { get; private set; }

		protected string Signature { get; private set; }

		internal static string CalculateSignature(string apiKey, string sharedSecret, DateTime time)
		{
			return Md5HashHelper.GetMd5HashString(string.Concat(apiKey, sharedSecret,
					DateTimeHelper.GetUnixTimeStamp(time)));
		}

		internal string GetApiUrl()
		{
			return IsDevelopmentMode
				? EndpointConfiguration.DevApiUrl
				: EndpointConfiguration.ApiUrl;
		}

		internal string GetBookingApiUrl()
		{
			return IsDevelopmentMode
				? EndpointConfiguration.DevBookingApiUrl
				: EndpointConfiguration.BookingApiUrl;
		}
	}
}
