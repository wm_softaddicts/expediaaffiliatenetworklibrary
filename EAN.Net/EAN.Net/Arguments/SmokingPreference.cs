﻿using EAN.Net.Attributes;

namespace EAN.Net.Arguments
{
	public enum SmokingPreference
	{
		[EnumCustomValue("NS")]
		NonSmoking,

		[EnumCustomValue("S")]
		Smoking,

		[EnumCustomValue("E")]
		Either
	}
}
