﻿using System;
using System.ComponentModel;
using EAN.Net.Attributes;

namespace EAN.Net.Arguments
{
	[Flags, DefaultValue(Default)]
	public enum HotelInformationDetailOptions
	{
		[EnumCustomValue("HOTEL_SUMMARY")]
		HotelSummary,

		[EnumCustomValue("HOTEL_DETAILS")]
		HotelDetails,

		[EnumCustomValue("SUPPLIERS")]
		Suppliers,

		[EnumCustomValue("ROOM_TYPES")]
		RoomTypes,

		[EnumCustomValue("ROOM_AMENITIES")]
		RoomAmenities,

		[EnumCustomValue("PROPERTY_AMENITIES")]
		PropertyAmenities,

		[EnumCustomValue("HOTEL_IMAGES")]
		HotelImages,

		[EnumCustomValue("DEFAULT")]
		Default = HotelSummary | HotelDetails | Suppliers | RoomTypes | RoomAmenities | PropertyAmenities | HotelImages
	}
}
