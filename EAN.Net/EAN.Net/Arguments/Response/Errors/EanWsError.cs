﻿using EAN.Net.Arguments.Response.Ping;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.Errors
{
	public class EanWsError
	{
		[DeserializeAs(Name = "itineraryId")]
		public long ItineraryId { get; set; }

		[DeserializeAs(Name = "handling")]
		public string Handling { get; set; }

		[DeserializeAs(Name = "category")]
		public string Category { get; set; }

		[DeserializeAs(Name = "exceptionConditionId")]
		public long ExceptionConditionId { get; set; }

		[DeserializeAs(Name = "presentationMessage")]
		public string PresentationMessage { get; set; }

		[DeserializeAs(Name = "verboseMessage")]
		public string VerboseMessage { get; set; }

		[DeserializeAs(Name = "ServerInfo")]
		public ServerInfo ServerInfo { get; set; }
	}
}
