﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.AlternateProperties
{
	public class Amenities
	{
		[DeserializeAs(Name = "businessCenter")]
		public bool BusinessCenter { get; set; }

		[DeserializeAs(Name = "fitnessCenter")]
		public bool FitnessCenter { get; set; }

		[DeserializeAs(Name = "hotTubOnsite")]
		public bool HotTubOnsite { get; set; }

		[DeserializeAs(Name = "interAccessAvailable")]
		public bool InterAccessAvailable { get; set; }

		[DeserializeAs(Name = "childrensActivities")]
		public bool ChildrensActivities { get; set; }

		[DeserializeAs(Name = "kitchenOrKitchinette")]
		public bool KitchenOrKitchinette { get; set; }

		[DeserializeAs(Name = "petsAllowed")]
		public bool PetsAllowed { get; set; }

		[DeserializeAs(Name = "pool")]
		public bool Pool { get; set; }

		[DeserializeAs(Name = "restaurantOnsite")]
		public bool RestaurantOnsite { get; set; }

		[DeserializeAs(Name = "spaOnsite")]
		public bool SpaOnsite { get; set; }

		[DeserializeAs(Name = "whirlPoolBathAvailable")]
		public bool WhirlPoolBathAvailable { get; set; }
	}
}