﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.AlternateProperties
{
	public class AlternateHotelResponse
	{
		[DeserializeAs(Name = "HotelInfo")]
		public HotelInfo HotelInfo { get; set; }

		[DeserializeAs(Name = "Amenities")]
		public Amenities Amenities { get; set; }

		[DeserializeAs(Name = "LowestRateInfo")]
		public LowestRateInfo LowestRateInfo { get; set; }
	}
}