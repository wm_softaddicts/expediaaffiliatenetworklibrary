﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.AlternateProperties
{
	public class AlternatePropertyListResponse : ResponseBase
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "arrivalDate")]
		public string ArrivalDate { get; set; }

		[DeserializeAs(Name = "departureDate")]
		public string DepartureDate { get; set; }

		[DeserializeAs(Name = "originalHotelId")]
		public int OriginalHotelId { get; set; }

		[DeserializeAs(Name = "AlternateHotelResponse")]
		public List<AlternateHotelResponse> AlternateHotelResponse { get; set; }
	}
}
