﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.AlternateProperties
{
	public class HotelInfo
	{
		[DeserializeAs(Name = "hotelId")]
		public long HotelId { get; set; }

		[DeserializeAs(Name = "name")]
		public string Name { get; set; }

		[DeserializeAs(Name = "address")]
		public string Address { get; set; }

		[DeserializeAs(Name = "city")]
		public string City { get; set; }

		[DeserializeAs(Name = "stateProvinceCode")]
		public string StateProvinceCode { get; set; }

		[DeserializeAs(Name = "countryCode")]
		public string CountryCode { get; set; }

		[DeserializeAs(Name = "postalCode")]
		public string PostalCode { get; set; }

		[DeserializeAs(Name = "starRating")]
		public double StarRating { get; set; }

		[DeserializeAs(Name = "thumbnailUrl")]
		public string ThumbnailUrl { get; set; }

		[DeserializeAs(Name = "locationDescription")]
		public string LocationDescription { get; set; }
	}
}