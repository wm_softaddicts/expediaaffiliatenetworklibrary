﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.AlternateProperties
{
	public class NightlyRates
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "nightlyRate")]
		public List<double> NightlyRateList { get; set; }
	}
}