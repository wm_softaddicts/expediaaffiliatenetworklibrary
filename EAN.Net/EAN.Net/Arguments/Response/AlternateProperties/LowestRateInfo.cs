using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.AlternateProperties
{
	public class LowestRateInfo
	{
		[DeserializeAs(Name = "NightlyRates")]
		public NightlyRates NightlyRates { get; set; }

		[DeserializeAs(Name = "rateCurrencyCode")]
		public string RateCurrencyCode { get; set; }

		[DeserializeAs(Name = "roomDescription")]
		public string RoomDescription { get; set; }

		[DeserializeAs(Name = "rateCode")]
		public string RateCode { get; set; }

		[DeserializeAs(Name = "roomTypeCode")]
		public string RoomTypeCode { get; set; }

		[DeserializeAs(Name = "supplierType")]
		public string SupplierType { get; set; }

		[DeserializeAs(Name = "promotion")]
		public string Promotion { get; set; }
	}
}