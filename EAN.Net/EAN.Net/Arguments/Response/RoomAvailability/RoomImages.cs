﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.RoomAvailability
{
	public class RoomImages
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "RoomImage")]
		public object RoomImage { get; set; }
	}
}