﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.RoomAvailability
{
	public class HotelRoomAvailabilityResponse: ResponseBase
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "hotelId")]
		public long HotelId { get; set; }

		[DeserializeAs(Name = "arrivalDate")]
		public string ArrivalDate { get; set; }

		[DeserializeAs(Name = "departureDate")]
		public string DepartureDate { get; set; }

		[DeserializeAs(Name = "hotelName")]
		public string HotelName { get; set; }

		[DeserializeAs(Name = "hotelAddress")]
		public string HotelAddress { get; set; }

		[DeserializeAs(Name = "hotelCity")]
		public string HotelCity { get; set; }

		[DeserializeAs(Name = "hotelStateProvince")]
		public string HotelStateProvince { get; set; }

		[DeserializeAs(Name = "hotelCountry")]
		public string HotelCountry { get; set; }

		[DeserializeAs(Name = "numberOfRoomsRequested")]
		public int NumberOfRoomsRequested { get; set; }

		[DeserializeAs(Name = "checkInInstructions")]
		public string CheckInInstructions { get; set; }

		[DeserializeAs(Name = "tripAdvisorRating")]
		public double TripAdvisorRating { get; set; }

		[DeserializeAs(Name = "tripAdvisorReviewCount")]
		public int TripAdvisorReviewCount { get; set; }

		[DeserializeAs(Name = "tripAdvisorRatingUrl")]
		public string TripAdvisorRatingUrl { get; set; }

		[DeserializeAs(Name = "HotelRoomResponse")]
		public List<HotelRoomResponse> HotelRoomResponse { get; set; }
	}
}
