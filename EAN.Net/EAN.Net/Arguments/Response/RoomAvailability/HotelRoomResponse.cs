﻿using EAN.Net.Arguments.Common;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.RoomAvailability
{
	public class HotelRoomResponse
	{
		[DeserializeAs(Name = "rateCode")]
		public string RateCode { get; set; }

		[DeserializeAs(Name = "roomTypeCode")]
		public string RoomTypeCode { get; set; }

		[DeserializeAs(Name = "rateDescription")]
		public string RateDescription { get; set; }

		[DeserializeAs(Name = "roomTypeDescription")]
		public string RoomTypeDescription { get; set; }

		[DeserializeAs(Name = "supplierType")]
		public string SupplierType { get; set; }

		[DeserializeAs(Name = "propertyId")]
		public int PropertyId { get; set; }

		[DeserializeAs(Name = "BedTypes")]
		public BedTypes BedTypes { get; set; }

		[DeserializeAs(Name = "smokingPreferences")]
		public string SmokingPreferences { get; set; }

		[DeserializeAs(Name = "rateOccupancyPerRoom")]
		public int RateOccupancyPerRoom { get; set; }

		[DeserializeAs(Name = "quotedOccupancy")]
		public int QuotedOccupancy { get; set; }

		[DeserializeAs(Name = "minGuestAge")]
		public int MinGuestAge { get; set; }

		[DeserializeAs(Name = "RateInfos")]
		public RateInfos RateInfos { get; set; }

		[DeserializeAs(Name = "ValueAdds")]
		public ValueAdds ValueAdds { get; set; }

		[DeserializeAs(Name = "deepLink")]
		public string DeepLink { get; set; }

		[DeserializeAs(Name = "RoomImages")]
		public RoomImages RoomImages { get; set; }
	}
}