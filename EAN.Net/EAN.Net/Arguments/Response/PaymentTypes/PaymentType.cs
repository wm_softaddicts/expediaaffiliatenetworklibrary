﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.PaymentTypes
{
	public class PaymentType
	{
		[DeserializeAs(Name = "code")]
		public string Code { get; set; }

		[DeserializeAs(Name = "name")]
		public string Name { get; set; }
	}
}