﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.PaymentTypes
{
	public class HotelPaymentResponse : ResponseBase
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "@currencyCode")]
		public string CurrencyCode { get; set; }

		[DeserializeAs(Name = "PaymentType")]
		public List<PaymentType> PaymentTypes { get; set; }
	}
}
