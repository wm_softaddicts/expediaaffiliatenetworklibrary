﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.RoomImages
{
	public class HotelRoomImageResponse : ResponseBase
	{
		[DeserializeAs(Name = "RoomImages")]
		public RoomImages RoomImages { get; set; }
	}
}
