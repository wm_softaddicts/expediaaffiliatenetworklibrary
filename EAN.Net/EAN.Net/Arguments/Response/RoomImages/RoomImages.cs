﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.RoomImages
{
	public class RoomImages
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "RoomImage")]
		public List<RoomImage> RoomImageList { get; set; }
	}
}