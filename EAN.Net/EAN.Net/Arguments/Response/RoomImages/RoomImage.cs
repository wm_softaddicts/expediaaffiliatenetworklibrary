﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.RoomImages
{
	public class RoomImage
	{
		[DeserializeAs(Name = "roomTypeCode")]
		public string RoomTypeCode { get; set; }

		[DeserializeAs(Name = "url")]
		public string Url { get; set; }
	}
}