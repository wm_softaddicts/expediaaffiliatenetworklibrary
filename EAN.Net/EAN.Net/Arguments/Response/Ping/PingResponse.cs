﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.Ping
{
	public class PingResponse : ResponseBase
	{
		[DeserializeAs(Name = "echo")]
		public string EchoValue { get; set; }

		[DeserializeAs(Name = "ServerInfo")]
		public ServerInfo ServerInfo { get; set; }
	}
}
