﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.Ping
{
	public class ServerInfo
	{
		[DeserializeAs(Name = "@instance")]
		public string Instance { get; set; }

		[DeserializeAs(Name = "@timestamp")]
		public string Timestamp { get; set; }

		[DeserializeAs(Name = "@serverTime")]
		public string ServerTime { get; set; }
	}
}