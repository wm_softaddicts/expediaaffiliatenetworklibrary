﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.CancelReservation
{
	public class HotelRoomCancellationResponse : ResponseBase
	{
		[DeserializeAs(Name = "cancellationNumber")]
		public string CancellationNumber { get; set; }
	}
}
