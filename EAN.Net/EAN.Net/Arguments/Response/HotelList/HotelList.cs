﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelList
{
	public class HotelList
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "@activePropertyCount")]
		public string ActivePropertyCount { get; set; }

		[DeserializeAs(Name = "HotelSummary")]
		public List<HotelSummary> HotelSummaryList { get; set; }
	}
}