﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelList
{
	public class RoomRateDetailsList
	{
		[DeserializeAs(Name = "RoomRateDetails")]
		public RoomRateDetails RoomRateDetails { get; set; }
	}
}