using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelList
{
	public class HotelListResponse : ResponseBase
	{
		[DeserializeAs(Name = "numberOfRoomsRequested")]
		public int NumberOfRoomsRequested { get; set; }

		[DeserializeAs(Name = "moreResultsAvailable")]
		public bool IsMoreResultsAvailable { get; set; }

		[DeserializeAs(Name = "cacheKey")]
		public string CacheKey { get; set; }

		[DeserializeAs(Name = "cacheLocation")]
		public string CacheLocation { get; set; }

		[DeserializeAs(Name = "cachedSupplierResponse")]
		public CachedSupplierResponse CachedSupplierResponse { get; set; }

		[DeserializeAs(Name = "HotelList")]
		public HotelList HotelList { get; set; }
	}
}