﻿using EAN.Net.Arguments.Common;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelList
{
	public class RoomRateDetails
	{
		[DeserializeAs(Name = "roomTypeCode")]
		public string RoomTypeCode { get; set; }

		[DeserializeAs(Name = "rateCode")]
		public string RateCode { get; set; }

		[DeserializeAs(Name = "rateKey")]
		public string RateKey { get; set; }

		[DeserializeAs(Name = "maxRoomOccupancy")]
		public int MaxRoomOccupancy { get; set; }

		[DeserializeAs(Name = "quotedRoomOccupancy")]
		public int QuotedRoomOccupancy { get; set; }

		[DeserializeAs(Name = "minGuestAge")]
		public int MinGuestAge { get; set; }

		[DeserializeAs(Name = "roomDescription")]
		public string RoomDescription { get; set; }

		[DeserializeAs(Name = "promoId")]
		public string PromoId { get; set; }

		[DeserializeAs(Name = "promoDescription")]
		public string PromoDescription { get; set; }

		[DeserializeAs(Name = "promoDetailText")]
		public string PromoDetailText { get; set; }

		[DeserializeAs(Name = "currentAllotment")]
		public int? CurrentAllotment { get; set; }

		[DeserializeAs(Name = "propertyAvailable")]
		public bool IsPropertyAvailable { get; set; }

		[DeserializeAs(Name = "propertyRestricted")]
		public bool IsPropertyRestricted { get; set; }

		[DeserializeAs(Name = "expediaPropertyId")]
		public string ExpediaPropertyId { get; set; }

		[DeserializeAs(Name = "BedTypes")]
		public BedTypes BedTypes { get; set; }

		[DeserializeAs(Name = "smokingPreferences")]
		public string SmokingPreferences { get; set; }

		[DeserializeAs(Name = "nonRefundable")]
		public bool IsNonRefundable { get; set; }

		[DeserializeAs(Name = "ValueAdds")]
		public ValueAdds ValueAdds { get; set; }

		[DeserializeAs(Name = "RateInfos")]
		public RateInfos RateInfos { get; set; }
	}
}