﻿using System.Collections.Generic;
using EAN.Net.Arguments.Common;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelList
{
	public class RoomGroup
	{
		[DeserializeAs(Name = "Room")]
		public List<Room> RoomList { get; set; }
	}
}
