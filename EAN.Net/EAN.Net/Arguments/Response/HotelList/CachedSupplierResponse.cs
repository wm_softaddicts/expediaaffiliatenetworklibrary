﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelList
{
	public class CachedSupplierResponse
	{
		[DeserializeAs(Name = "@supplierCacheTolerance")]
		public string SupplierCacheTolerance { get; set; }

		[DeserializeAs(Name = "@cachedTime")]
		public string CachedTime { get; set; }

		[DeserializeAs(Name = "@supplierRequestNum")]
		public string SupplierRequestNum { get; set; }

		[DeserializeAs(Name = "@supplierResponseNum")]
		public string SupplierResponseNum { get; set; }

		[DeserializeAs(Name = "@supplierResponseTime")]
		public string SupplierResponseTime { get; set; }

		[DeserializeAs(Name = "@candidatePreptime")]
		public string CandidatePreptime { get; set; }

		[DeserializeAs(Name = "@otherOverheadTime")]
		public string OtherOverheadTime { get; set; }

		[DeserializeAs(Name = "@tpidUsed")]
		public string TpidUsed { get; set; }

		[DeserializeAs(Name = "@matchedCurrency")]
		public string MatchedCurrency { get; set; }

		[DeserializeAs(Name = "@matchedLocale")]
		public string MatchedLocale { get; set; }
	}
}