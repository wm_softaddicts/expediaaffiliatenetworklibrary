﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelList
{
	public class HotelSummary
	{
		[DeserializeAs(Name = "@order")]
		public string Order { get; set; }

		[DeserializeAs(Name = "@ubsScore")]
		public string UbsScore { get; set; }

		[DeserializeAs(Name = "hotelId")]
		public long HotelId { get; set; }

		[DeserializeAs(Name = "name")]
		public string Name { get; set; }

		[DeserializeAs(Name = "address1")]
		public string Address1 { get; set; }

		[DeserializeAs(Name = "address2")]
		public string Address2 { get; set; }

		[DeserializeAs(Name = "city")]
		public string City { get; set; }

		[DeserializeAs(Name = "stateProvinceCode")]
		public string StateProvinceCode { get; set; }

		[DeserializeAs(Name = "postalCode")]
		public string PostalCode { get; set; }

		[DeserializeAs(Name = "countryCode")]
		public string CountryCode { get; set; }

		[DeserializeAs(Name = "airportCode")]
		public string AirportCode { get; set; }

		[DeserializeAs(Name = "supplierType")]
		public string SupplierType { get; set; }

		[DeserializeAs(Name = "propertyCategory")]
		public int PropertyCategory { get; set; }

		[DeserializeAs(Name = "hotelRating")]
		public float HotelRating { get; set; }

		[DeserializeAs(Name = "confidenceRating")]
		public int ConfidenceRating { get; set; }

		[DeserializeAs(Name = "amenityMask")]
		public float AmenityMask { get; set; }

		[DeserializeAs(Name = "tripAdvisorRating")]
		public double TripAdvisorRating { get; set; }

		[DeserializeAs(Name = "tripAdvisorReviewCount")]
		public int TripAdvisorReviewCount { get; set; }

		[DeserializeAs(Name = "tripAdvisorRatingUrl")]
		public string TripAdvisorRatingUrl { get; set; }

		[DeserializeAs(Name = "locationDescription")]
		public string LocationDescription { get; set; }

		[DeserializeAs(Name = "shortDescription")]
		public string ShortDescription { get; set; }

		[DeserializeAs(Name = "highRate")]
		public double HighRate { get; set; }

		[DeserializeAs(Name = "lowRate")]
		public double LowRate { get; set; }

		[DeserializeAs(Name = "rateCurrencyCode")]
		public string RateCurrencyCode { get; set; }

		[DeserializeAs(Name = "latitude")]
		public double Latitude { get; set; }

		[DeserializeAs(Name = "longitude")]
		public double Longitude { get; set; }

		[DeserializeAs(Name = "proximityDistance")]
		public double ProximityDistance { get; set; }

		[DeserializeAs(Name = "proximityUnit")]
		public string ProximityUnit { get; set; }

		[DeserializeAs(Name = "hotelInDestination")]
		public bool IsHotelInDestination { get; set; }

		[DeserializeAs(Name = "thumbNailUrl")]
		public string ThumbNailUrl { get; set; }

		[DeserializeAs(Name = "deepLink")]
		public string DeepLink { get; set; }

		[DeserializeAs(Name = "RoomRateDetailsList")]
		public RoomRateDetailsList RoomRateDetailsList { get; set; }
	}
}