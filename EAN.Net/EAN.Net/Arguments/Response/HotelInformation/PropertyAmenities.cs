﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelInformation
{
	public class PropertyAmenities
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "PropertyAmenity")]
		public List<PropertyAmenity> PropertyAmenityList { get; set; }
	}
}