﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelInformation
{
	public class HotelDetails
	{
		[DeserializeAs(Name = "numberOfRooms")]
		public int NumberOfRooms { get; set; }

		[DeserializeAs(Name = "numberOfFloors")]
		public int NumberOfFloors { get; set; }

		[DeserializeAs(Name = "checkInTime")]
		public string CheckInTime { get; set; }

		[DeserializeAs(Name = "checkOutTime")]
		public string CheckOutTime { get; set; }

		[DeserializeAs(Name = "propertyInformation")]
		public string PropertyInformation { get; set; }

		[DeserializeAs(Name = "areaInformation")]
		public string AreaInformation { get; set; }

		[DeserializeAs(Name = "propertyDescription")]
		public string PropertyDescription { get; set; }

		[DeserializeAs(Name = "hotelPolicy")]
		public string HotelPolicy { get; set; }

		[DeserializeAs(Name = "roomInformation")]
		public string RoomInformation { get; set; }

		[DeserializeAs(Name = "drivingDirections")]
		public string DrivingDirections { get; set; }

		[DeserializeAs(Name = "checkInInstructions")]
		public string CheckInInstructions { get; set; }

		[DeserializeAs(Name = "knowBeforeYouGoDescription")]
		public string KnowBeforeYouGoDescription { get; set; }

		[DeserializeAs(Name = "roomFeesDescription")]
		public string RoomFeesDescription { get; set; }

		[DeserializeAs(Name = "locationDescription")]
		public string LocationDescription { get; set; }

		[DeserializeAs(Name = "diningDescription")]
		public string DiningDescription { get; set; }

		[DeserializeAs(Name = "amenitiesDescription")]
		public string AmenitiesDescription { get; set; }

		[DeserializeAs(Name = "businessAmenitiesDescription")]
		public string BusinessAmenitiesDescription { get; set; }

		[DeserializeAs(Name = "roomDetailDescription")]
		public string RoomDetailDescription { get; set; }
	}
}