﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelInformation
{
	public class RoomAmenities
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "RoomAmenity")]
		public List<RoomAmenity> RoomAmenityList { get; set; }
	}
}