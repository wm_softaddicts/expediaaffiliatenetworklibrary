﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelInformation
{
	public class Supplier
	{
		[DeserializeAs(Name = "@id")]
		public string Id { get; set; }

		[DeserializeAs(Name = "@chainCode")]
		public string ChainCode { get; set; }
	}
}