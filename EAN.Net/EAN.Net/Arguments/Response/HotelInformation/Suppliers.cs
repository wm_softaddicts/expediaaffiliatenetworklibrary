﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelInformation
{
	public class Suppliers
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "Supplier")]
		public List<Supplier> SupplierList { get; set; }
	}
}