﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelInformation
{
	public class RoomType
	{
		[DeserializeAs(Name = "@roomTypeId")]
		public string RoomTypeId { get; set; }

		[DeserializeAs(Name = "@roomCode")]
		public string RoomCode { get; set; }

		[DeserializeAs(Name = "description")]
		public string Description { get; set; }

		[DeserializeAs(Name = "descriptionLong")]
		public string DescriptionLong { get; set; }

		[DeserializeAs(Name = "roomAmenities")]
		public RoomAmenities RoomAmenities { get; set; }
	}
}