using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelInformation
{
	public class RoomTypes
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "RoomType")]
		public List<RoomType> RoomTypeList { get; set; }
	}
}