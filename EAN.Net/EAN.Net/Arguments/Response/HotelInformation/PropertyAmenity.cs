﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelInformation
{
	public class PropertyAmenity
	{
		[DeserializeAs(Name = "amenityId")]
		public int AmenityId { get; set; }

		[DeserializeAs(Name = "amenity")]
		public string Amenity { get; set; }
	}
}