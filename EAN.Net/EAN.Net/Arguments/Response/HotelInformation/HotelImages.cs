﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelInformation
{
	public class HotelImages
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "HotelImage")]
		public List<HotelImage> HotelImageList { get; set; }
	}
}