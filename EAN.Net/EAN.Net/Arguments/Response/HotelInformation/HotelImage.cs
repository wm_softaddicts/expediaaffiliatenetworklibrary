﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelInformation
{
	public class HotelImage
	{
		[DeserializeAs(Name = "hotelImageId")]
		public int HotelImageId { get; set; }

		[DeserializeAs(Name = "name")]
		public string Name { get; set; }

		[DeserializeAs(Name = "name")]
		public int? Category { get; set; }

		[DeserializeAs(Name = "type")]
		public int Type { get; set; }

		[DeserializeAs(Name = "caption")]
		public string Caption { get; set; }

		[DeserializeAs(Name = "url")]
		public string Url { get; set; }

		[DeserializeAs(Name = "thumbnailUrl")]
		public string ThumbnailUrl { get; set; }

		[DeserializeAs(Name = "supplierId")]
		public long SupplierId { get; set; }

		[DeserializeAs(Name = "width")]
		public int Width { get; set; }

		[DeserializeAs(Name = "height")]
		public int Height { get; set; }

		[DeserializeAs(Name = "byteSize")]
		public int ByteSize { get; set; }
	}
}