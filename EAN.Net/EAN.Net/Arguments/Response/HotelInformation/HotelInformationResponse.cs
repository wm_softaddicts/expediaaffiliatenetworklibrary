﻿using EAN.Net.Arguments.Response.HotelList;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelInformation
{
	public class HotelInformationResponse : ResponseBase
	{
		[DeserializeAs(Name = "@hotelId")]
		public string HotelId { get; set; }

		[DeserializeAs(Name = "HotelSummary")]
		public HotelSummary HotelSummary { get; set; }

		[DeserializeAs(Name = "HotelDetails")]
		public HotelDetails HotelDetails { get; set; }

		[DeserializeAs(Name = "Suppliers")]
		public Suppliers Suppliers { get; set; }

		[DeserializeAs(Name = "RoomTypes")]
		public RoomTypes RoomTypes { get; set; }

		[DeserializeAs(Name = "PropertyAmenities")]
		public PropertyAmenities PropertyAmenities { get; set; }

		[DeserializeAs(Name = "HotelImages")]
		public HotelImages HotelImages { get; set; }
	}
}
