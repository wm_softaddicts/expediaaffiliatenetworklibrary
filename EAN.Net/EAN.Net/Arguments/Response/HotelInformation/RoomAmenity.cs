using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.HotelInformation
{
	public class RoomAmenity
	{
		[DeserializeAs(Name = "@amenityId")]
		public string AmenityId { get; set; }

		[DeserializeAs(Name = "amenity")]
		public string Amenity { get; set; }
	}
}