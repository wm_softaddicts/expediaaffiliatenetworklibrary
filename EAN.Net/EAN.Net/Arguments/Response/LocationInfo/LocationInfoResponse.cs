﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.LocationInfo
{
	public class LocationInfoResponse : ResponseBase
	{
		[DeserializeAs(Name = "LocationInfos")]
		public LocationInfos LocationInfos { get; set; }
	}

}
