﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.LocationInfo
{
	public class LocationInfo
	{
		[DeserializeAs(Name = "active")]
		public bool Active { get; set; }

		[DeserializeAs(Name = "type")]
		public int Type { get; set; }

		[DeserializeAs(Name = "address")]
		public string Address { get; set; }

		[DeserializeAs(Name = "city")]
		public string City { get; set; }

		[DeserializeAs(Name = "stateProvinceCode")]
		public string StateProvinceCode { get; set; }

		[DeserializeAs(Name = "postalCode")]
		public string PostalCode { get; set; }

		[DeserializeAs(Name = "countryCode")]
		public string CountryCode { get; set; }

		[DeserializeAs(Name = "countryName")]
		public string CountryName { get; set; }

		[DeserializeAs(Name = "code")]
		public string Code { get; set; }

		[DeserializeAs(Name = "geoAccuracy")]
		public int GeoAccuracy { get; set; }

		[DeserializeAs(Name = "locationInDestination")]
		public bool IsLocationInDestination { get; set; }

		[DeserializeAs(Name = "latitude")]
		public double Latitude { get; set; }

		[DeserializeAs(Name = "longitude")]
		public double Longitude { get; set; }

		[DeserializeAs(Name = "refLocationMileage")]
		public int RefLocationMileage { get; set; }

		[DeserializeAs(Name = "activePropertyCount")]
		public int ActivePropertyCount { get; set; }

		[DeserializeAs(Name = "destinationId")]
		public string DestinationId { get; set; }

		[DeserializeAs(Name = "description")]
		public string Description { get; set; }
	}
}