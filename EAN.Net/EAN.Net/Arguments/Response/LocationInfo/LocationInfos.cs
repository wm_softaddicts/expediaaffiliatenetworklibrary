﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.LocationInfo
{
	public class LocationInfos
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "LocationInfo")]
		public List<LocationInfo> LocationInfoList { get; set; }
	}
}