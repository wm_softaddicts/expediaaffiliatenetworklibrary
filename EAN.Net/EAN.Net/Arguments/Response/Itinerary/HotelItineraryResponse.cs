﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.Itinerary
{
	public class HotelItineraryResponse : ResponseBase
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "Itinerary")]
		public List<Itinerary> ItineraryList { get; set; }
	}
}
