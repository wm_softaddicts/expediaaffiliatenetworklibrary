﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.Itinerary
{
	public class Hotel
	{
		[DeserializeAs(Name = "hotelId")]
		public long HotelId { get; set; }

		[DeserializeAs(Name = "statusCode")]
		public string StatusCode { get; set; }

		[DeserializeAs(Name = "name")]
		public string Name { get; set; }

		[DeserializeAs(Name = "address1")]
		public string Address1 { get; set; }

		[DeserializeAs(Name = "address2")]
		public string Address2 { get; set; }

		[DeserializeAs(Name = "city")]
		public string City { get; set; }

		[DeserializeAs(Name = "countryCode")]
		public string CountryCode { get; set; }

		[DeserializeAs(Name = "postalCode")]
		public string PostalCode { get; set; }

		[DeserializeAs(Name = "phone")]
		public string Phone { get; set; }

		[DeserializeAs(Name = "fax")]
		public string Fax { get; set; }

		[DeserializeAs(Name = "latitude")]
		public double Latitude { get; set; }

		[DeserializeAs(Name = "longitude")]
		public double Longitude { get; set; }

		[DeserializeAs(Name = "coordinateAccuracyCode")]
		public int CoordinateAccuracyCode { get; set; }

		[DeserializeAs(Name = "lowRate")]
		public double LowRate { get; set; }

		[DeserializeAs(Name = "highRate")]
		public double HighRate { get; set; }

		[DeserializeAs(Name = "confidence")]
		public int Confidence { get; set; }

		[DeserializeAs(Name = "hotelRating")]
		public float HotelRating { get; set; }

		[DeserializeAs(Name = "tripAdvisorRating")]
		public double TripAdvisorRating { get; set; }

		[DeserializeAs(Name = "market")]
		public string Market { get; set; }

		[DeserializeAs(Name = "region")]
		public string Region { get; set; }

		[DeserializeAs(Name = "superRegion")]
		public string SuperRegion { get; set; }

		[DeserializeAs(Name = "theme")]
		public string Theme { get; set; }
	}
}