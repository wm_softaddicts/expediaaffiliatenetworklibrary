﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.Itinerary
{
	public class Customer
	{
		[DeserializeAs(Name = "email")]
		public string Email { get; set; }

		[DeserializeAs(Name = "firstName")]
		public string FirstName { get; set; }

		[DeserializeAs(Name = "lastName")]
		public string LastName { get; set; }

		[DeserializeAs(Name = "homePhone")]
		public object HomePhone { get; set; }

		[DeserializeAs(Name = "workPhone")]
		public object WorkPhone { get; set; }

		[DeserializeAs(Name = "CustomerAddresses")]
		public CustomerAddress CustomerAddress { get; set; }

		[DeserializeAs(Name = "extension")]
		public int? Extension { get; set; }
	}
}