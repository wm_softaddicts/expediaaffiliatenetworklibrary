﻿using System.Collections.Generic;
using EAN.Net.Arguments.Common;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.Itinerary
{
	public class HotelConfirmation
	{
		[DeserializeAs(Name = "supplierId")]
		public long SupplierId { get; set; }

		[DeserializeAs(Name = "chainCode")]
		public string ChainCode { get; set; }

		[DeserializeAs(Name = "arrivalDate")]
		public string ArrivalDate { get; set; }

		[DeserializeAs(Name = "departureDate")]
		public string DepartureDate { get; set; }

		[DeserializeAs(Name = "confirmationNumber")]
		public string ConfirmationNumber { get; set; }

		[DeserializeAs(Name = "RateInfos")]
		public RateInfos RateInfos { get; set; }

		[DeserializeAs(Name = "numberOfAdults")]
		public int NumberOfAdults { get; set; }

		[DeserializeAs(Name = "numberOfChildren")]
		public int NumberOfChildren { get; set; }

		[DeserializeAs(Name = "affiliateConfirmationId")]
		public string AffiliateConfirmationId { get; set; }

		[DeserializeAs(Name = "smokingPreference")]
		public string SmokingPreference { get; set; }

		[DeserializeAs(Name = "supplierPropertyId")]
		public int SupplierPropertyId { get; set; }

		[DeserializeAs(Name = "roomTypeCode")]
		public string RoomTypeCode { get; set; }

		[DeserializeAs(Name = "rateCode")]
		public string RateCode { get; set; }

		[DeserializeAs(Name = "rateDescription")]
		public string RateDescription { get; set; }

		[DeserializeAs(Name = "roomDescription")]
		public string RoomDescription { get; set; }

		[DeserializeAs(Name = "status")]
		public string Status { get; set; }

		[DeserializeAs(Name = "locale")]
		public string Locale { get; set; }

		[DeserializeAs(Name = "ReservationGuest")]
		public List<ReservationGuest> ReservationGuests { get; set; }

		[DeserializeAs(Name = "Hotel")]
		public List<Hotel> Hotels { get; set; }

		[DeserializeAs(Name = "nights")]
		public int NightCount { get; set; }
	}
}
