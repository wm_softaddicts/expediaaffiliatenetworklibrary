﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.Itinerary
{
	public class ReservationGuest
	{
		[DeserializeAs(Name = "firstName")]
		public string FirstName { get; set; }

		[DeserializeAs(Name = "lastName")]
		public string LastName { get; set; }
	}
}