﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.Itinerary
{
	public class Itinerary
	{
		[DeserializeAs(Name = "itineraryId")]
		public long ItineraryId { get; set; }

		[DeserializeAs(Name = "affiliateId")]
		public long AffiliateId { get; set; }

		[DeserializeAs(Name = "creationDate")]
		public string CreationDate { get; set; }

		[DeserializeAs(Name = "creationTime")]
		public string CreationTime { get; set; }

		[DeserializeAs(Name = "itineraryStartDate")]
		public string ItineraryStartDate { get; set; }

		[DeserializeAs(Name = "itineraryEndDate")]
		public string ItineraryEndDate { get; set; }

		[DeserializeAs(Name = "Customer")]
		public Customer Customer { get; set; }

		[DeserializeAs(Name = "HotelConfirmation")]
		public List<HotelConfirmation> HotelConfirmations { get; set; }

		[DeserializeAs(Name = "affiliateCustomerId")]
		public string AffiliateCustomerId { get; set; }
	}
}