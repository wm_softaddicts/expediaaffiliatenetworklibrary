﻿using EAN.Net.Arguments.Common;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response.Itinerary
{
	public class CustomerAddress: Address
	{
		[DeserializeAs(Name = "isPrimary")]
		public bool IsPrimary { get; set; }

		[DeserializeAs(Name = "type")]
		public int Type { get; set; }
	}
}