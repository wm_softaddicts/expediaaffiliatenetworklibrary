﻿using System;
using EAN.Net.Arguments.Response.Errors;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Response
{
	public abstract class ResponseBase
	{
		[DeserializeAs(Name = "customerSessionId")]
		public virtual string CustomerSessionId { get; set; }

		[DeserializeAs(Name = "EanWsError")]
		public virtual EanWsError EanError { get; set; }

		public virtual Exception ErrorException { get; set; }

		public virtual bool IsError
		{
			get { return EanError != null || ErrorException != null; }
		}
	}
}
