﻿namespace EAN.Net.Arguments
{
	public class ReservationOptions
	{
		public string Email { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string HomePhone { get; set; }
		public string WorkPhone { get; set; }
		public string PhoneExtension { get; set; }
		public string FaxPhone { get; set; }
		public string CompanyName { get; set; }
		public string CreditCardType { get; set; }
		public string CreditCardNumber { get; set; }
		public string CreditCardIdentifier { get; set; }
		public string CreditCardExpirationMonth { get; set; }
		public string CreditCardExpirationYear { get; set; }
	}
}
