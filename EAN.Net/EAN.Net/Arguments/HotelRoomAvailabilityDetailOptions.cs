﻿using System;
using System.ComponentModel;
using EAN.Net.Attributes;

namespace EAN.Net.Arguments
{
	[Flags, DefaultValue(Default)]
	public enum HotelRoomAvailabilityDetailOptions
	{
		[EnumCustomValue("HOTEL_DETAILS")]
		HotelDetails,

		[EnumCustomValue("ROOM_TYPES")]
		RoomTypes,

		[EnumCustomValue("ROOM_AMENITIES")]
		RoomAmenities,

		[EnumCustomValue("PROPERTY_AMENITIES")]
		PropertyAmenities,

		[EnumCustomValue("HOTEL_IMAGES")]
		HotelImages,

		[EnumCustomValue("DEFAULT")]
		Default = HotelDetails | RoomTypes | RoomAmenities | PropertyAmenities | HotelImages
	}
}
