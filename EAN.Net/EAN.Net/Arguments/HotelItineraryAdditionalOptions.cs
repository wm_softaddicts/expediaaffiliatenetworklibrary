﻿using System.Collections.Generic;

namespace EAN.Net.Arguments
{
	public class HotelItineraryAdditionalOptions
	{
		public bool? IsIncludeChildAffiliates { get; set; }

		public bool IsResendConfirmationEmail { get; set; }

		public IList<string> ConfirmationExtras { get; set; }
	}
}
