﻿using EAN.Net.Arguments.Base;

namespace EAN.Net.Arguments
{
	public sealed class LocationInfoByDestinationIdOptions : SearchByDestinationIdOptions, ILocationInfoOptions
	{
	}
}
