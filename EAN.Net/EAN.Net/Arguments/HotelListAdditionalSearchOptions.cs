﻿namespace EAN.Net.Arguments
{
	public class HotelListAdditionalSearchOptions
	{
		public string Address { get; set; }

		public string PostalCode { get; set; }

		public string PropertyName { get; set; }
	}
}
