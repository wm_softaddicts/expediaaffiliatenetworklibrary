﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace EAN.Net.Arguments
{
	public sealed class HotelListSearchByHotelIdListOptions : IHotelListSearchOptions
	{
		private readonly IList<long> _hotelIdList = new ObservableCollection<long>();

		public IList<long> HotelIdList
		{
			get { return _hotelIdList; }
		}
	}
}
