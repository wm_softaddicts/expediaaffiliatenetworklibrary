﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public class CancelPolicyInfo
	{
		[DeserializeAs(Name = "versionId")]
		public int VersionId { get; set; }

		[DeserializeAs(Name = "cancelTime")]
		public string CancelTime { get; set; }

		[DeserializeAs(Name = "startWindowHours")]
		public int StartWindowHours { get; set; }

		[DeserializeAs(Name = "nightCount")]
		public int NightCount { get; set; }

		[DeserializeAs(Name = "currencyCode")]
		public string CurrencyCode { get; set; }

		[DeserializeAs(Name = "timeZoneDescription")]
		public string TimeZoneDescription { get; set; }
	}
}