﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public class Surcharge
	{
		[DeserializeAs(Name = "@type")]
		public string Type { get; set; }

		[DeserializeAs(Name = "@amount")]
		public float Amount { get; set; }
	}
}