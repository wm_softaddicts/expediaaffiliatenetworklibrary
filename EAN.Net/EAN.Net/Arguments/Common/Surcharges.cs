using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public class Surcharges
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "Surcharge")]
		public List<Surcharge> SurchargeList { get; set; }
	}
}