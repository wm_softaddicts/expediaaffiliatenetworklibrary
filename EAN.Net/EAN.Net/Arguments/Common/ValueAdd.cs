﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public class ValueAdd
	{
		[DeserializeAs(Name = "description")]
		public string Description { get; set; }
	}
}