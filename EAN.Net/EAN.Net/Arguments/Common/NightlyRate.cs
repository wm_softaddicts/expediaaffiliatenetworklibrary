﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public class NightlyRate
	{
		[DeserializeAs(Name = "@baseRate")]
		public float BaseRate { get; set; }

		[DeserializeAs(Name = "@rate")]
		public float Rate { get; set; }

		[DeserializeAs(Name = "@promo")]
		public bool IsPromo { get; set; }
	}
}