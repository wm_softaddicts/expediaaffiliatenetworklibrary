﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public class ChargeableRateInfo
	{
		[DeserializeAs(Name = "@averageBaseRate")]
		public float? AverageBaseRate { get; set; }

		[DeserializeAs(Name = "@averageRate")]
		public float? AverageRate { get; set; }

		[DeserializeAs(Name = "@commissionableUsdTotal")]
		public float? CommissionableUsdTotal { get; set; }

		[DeserializeAs(Name = "@currencyCode")]
		public string CurrencyCode { get; set; }

		[DeserializeAs(Name = "@maxNightlyRate")]
		public float? MaxNightlyRate { get; set; }

		[DeserializeAs(Name = "@nightlyRateTotal")]
		public float? NightlyRateTotal { get; set; }

		[DeserializeAs(Name = "@surchargeTotal")]
		public float? SurchargeTotal { get; set; }

		[DeserializeAs(Name = "@total")]
		public float? Total { get; set; }

		[DeserializeAs(Name = "NightlyRatesPerRoom")]
		public NightlyRatesPerRoom NightlyRatesPerRoom { get; set; }

		[DeserializeAs(Name = "Surcharges")]
		public Surcharges Surcharges { get; set; }
	}
}