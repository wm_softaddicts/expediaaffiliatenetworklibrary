﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public class HotelFees
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "RateInfo")]
		public List<HotelFee> HotelFeeList { get; set; }
	}
}
