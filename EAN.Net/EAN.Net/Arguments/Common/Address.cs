﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public abstract class Address
	{
		[DeserializeAs(Name = "countryCode")]
		public string CountryCode { get; set; }

		[DeserializeAs(Name = "stateProvinceCode")]
		public string StateProvinceCode { get; set; }

		[DeserializeAs(Name = "city")]
		public string City { get; set; }

		[DeserializeAs(Name = "postalCode")]
		public string PostalCode { get; set; }

		[DeserializeAs(Name = "address1")]
		public string Address1 { get; set; }

		[DeserializeAs(Name = "address2")]
		public string Address2 { get; set; }

		[DeserializeAs(Name = "address3")]
		public string Address3 { get; set; }
	}
}
