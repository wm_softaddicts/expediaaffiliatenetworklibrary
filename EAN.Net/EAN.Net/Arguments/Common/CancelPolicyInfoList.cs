﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public class CancelPolicyInfoList
	{
		[DeserializeAs(Name = "CancelPolicyInfo")]
		public List<CancelPolicyInfo> CancelPolicyInfo { get; set; }
	}
}