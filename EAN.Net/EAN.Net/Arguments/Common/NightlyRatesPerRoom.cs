﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public class NightlyRatesPerRoom
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "NightlyRate")]
		public List<NightlyRate> NightlyRateList { get; set; }
	}
}