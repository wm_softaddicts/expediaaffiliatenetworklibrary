﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public class HotelFeeBreakdown
	{
		[DeserializeAs(Name = "unit")]
		public string Unit { get; set; }

		[DeserializeAs(Name = "frequency")]
		public string Frequency { get; set; }
	}
}
