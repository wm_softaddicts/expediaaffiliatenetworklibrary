﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public class RateInfos
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "RateInfo")]
		public List<RateInfo> RateInfoList { get; set; }
	}
}