﻿using EAN.Net.Arguments.Response.HotelList;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public class RateInfo
	{
		[DeserializeAs(Name = "@priceBreakdown")]
		public string PriceBreakdown { get; set; }

		[DeserializeAs(Name = "@promo")]
		public bool IsPromo { get; set; }

		[DeserializeAs(Name = "@rateChange")]
		public string RateChange { get; set; }

		[DeserializeAs(Name = "RoomGroup")]
		public RoomGroup RoomGroup { get; set; }

		[DeserializeAs(Name = "ChargeableRateInfo")]
		public ChargeableRateInfo ChargeableRateInfo { get; set; }

		[DeserializeAs(Name = "cancellationPolicy")]
		public string CancellationPolicy { get; set; }

		[DeserializeAs(Name = "CancelPolicyInfoList")]
		public CancelPolicyInfoList CancelPolicyInfoList { get; set; }

		[DeserializeAs(Name = "nonRefundable")]
		public bool NonRefundable { get; set; }

		[DeserializeAs(Name = "online")]
		public bool IsOnline { get; set; }

		[DeserializeAs(Name = "HotelFees")]
		public HotelFees HotelFees { get; set; }

		[DeserializeAs(Name = "rateType")]
		public string RateType { get; set; }

		[DeserializeAs(Name = "currentAllotment")]
		public int CurrentAllotment { get; set; }

		[DeserializeAs(Name = "promoId")]
		public string PromoId { get; set; }

		[DeserializeAs(Name = "promoDescription")]
		public string PromoDescription { get; set; }

		[DeserializeAs(Name = "promoType")]
		public string PromoType { get; set; }


	}
}