﻿namespace EAN.Net.Arguments.Common
{
	public class RoomReservation: Room
	{
		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string BedTypeId { get; set; }

		public int? NumberOfBeds { get; set; }

		public SmokingPreference? SmokingPreference { get; set; }
	}
}
