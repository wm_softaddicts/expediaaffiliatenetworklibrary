﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public class Room
	{
		private readonly IList<int> _childAges = new ObservableCollection<int>();

		[DeserializeAs(Name = "numberOfAdults")]
		public int NumberOfAdults { get; set; }

		[DeserializeAs(Name = "numberOfChildren")]
		public int NumberOfChildren
		{
			get { return ChildAges.Count; }
		}

		[DeserializeAs(Name = "childAges")]
		public IList<int> ChildAges
		{
			get { return _childAges; }
		}

		[DeserializeAs(Name = "rateKey")]
		public string RateKey { get; set; }
	}
}
