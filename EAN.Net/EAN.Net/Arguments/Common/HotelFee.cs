﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public class HotelFee
	{
		[DeserializeAs(Name = "@description")]
		public string Description { get; set; }

		[DeserializeAs(Name = "@amount")]
		public float Amount { get; set; }

		[DeserializeAs(Name = "HotelFeeBreakdown")]
		public HotelFeeBreakdown HotelFeeBreakdown { get; set; }
	}
}
