﻿using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public class BedType
	{
		[DeserializeAs(Name = "@id")]
		public string Id { get; set; }

		[DeserializeAs(Name = "description")]
		public string Description { get; set; }
	}
}