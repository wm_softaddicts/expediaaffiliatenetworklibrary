﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.Common
{
	public class ValueAdds
	{
		[DeserializeAs(Name = "@size")]
		public string Size { get; set; }

		[DeserializeAs(Name = "ValueAdd")]
		public List<ValueAdd> ValueAddList { get; set; }
	}
}