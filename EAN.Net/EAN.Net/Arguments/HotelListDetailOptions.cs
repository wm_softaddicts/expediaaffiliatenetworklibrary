﻿using System.ComponentModel;
using EAN.Net.Attributes;

namespace EAN.Net.Arguments
{
	[DefaultValue(Default)]
	public enum HotelListDetailOptions
	{
		[EnumCustomValue("HOTEL_SUMMARY")]
		HotelSummary,

		[EnumCustomValue("ROOM_RATE_DETAILS")]
		RoomRateDetails,

		[EnumCustomValue("DEEP_LINKS")]
		DeepLinks,

		[EnumCustomValue("DEFAULT")]
		Default,
	}
}
