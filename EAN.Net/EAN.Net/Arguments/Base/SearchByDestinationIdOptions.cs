﻿namespace EAN.Net.Arguments.Base
{
	public abstract class SearchByDestinationIdOptions
	{
		public string DestinationId { get; set; }
	}
}
