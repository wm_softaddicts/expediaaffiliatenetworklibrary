﻿namespace EAN.Net.Arguments.Base
{
	public abstract class SearchByDestinationStringOptions
	{
		public string DestinationString { get; set; }
	}
}
