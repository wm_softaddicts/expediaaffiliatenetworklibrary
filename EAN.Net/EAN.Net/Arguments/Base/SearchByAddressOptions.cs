﻿namespace EAN.Net.Arguments.Base
{
	public abstract class SearchByAddressOptions
	{
		public string CountryCode { get; set; }

		public string StateProvinceCode { get; set; }

		public string City { get; set; }

		public string PostalCode { get; set; }

		public string Address { get; set; }
	}
}
