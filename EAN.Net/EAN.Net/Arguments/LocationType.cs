﻿using EAN.Net.Attributes;

namespace EAN.Net.Arguments
{
	public enum LocationType
	{
		[EnumCustomValue(1)]
		CityLocation,

		[EnumCustomValue(2)]
		LandmarkLocation
	}
}
