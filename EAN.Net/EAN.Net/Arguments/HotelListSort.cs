﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EAN.Net.Attributes;

namespace EAN.Net.Arguments
{
	public enum HotelListSort
	{
		[EnumCustomValue("NO_SORT")]
		NoSort,

		[EnumCustomValue("CITY_VALUE")]
		CityValue,

		[EnumCustomValue("OVERALL_VALUE")]
		OverallValue,

		[EnumCustomValue("PROMO")]
		Promo,

		[EnumCustomValue("PRICE")]
		Price,

		[EnumCustomValue("PRICE_REVERSE")]
		PriceReverse,

		[EnumCustomValue("PRICE_AVERAGE")]
		PriceAverage,

		[EnumCustomValue("QUALITY")]
		Quality,

		[EnumCustomValue("QUALITY_REVERSE")]
		QualityReverse,

		[EnumCustomValue("ALPHA")]
		Alpha,

		[EnumCustomValue("PROXIMITY")]
		Proximity,

		[EnumCustomValue("POSTAL_CODE")]
		PostalCode,

		[EnumCustomValue("TRIP_ADVISOR")]
		TripAdvisor
	}
}
