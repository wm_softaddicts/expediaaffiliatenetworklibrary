﻿using System;
using EAN.Net.Attributes;

namespace EAN.Net.Arguments
{
	[Flags]
	public enum PropertyCategory
	{
		[EnumCustomValue(1)]
		Hotel,

		[EnumCustomValue(2)]
		Suite,

		[EnumCustomValue(3)]
		Resort,

		[EnumCustomValue(4)]
		VacationRental,

		[EnumCustomValue(5)]
		BedBreakfast,

		[EnumCustomValue(6)]
		AllInclusive
	}
}
