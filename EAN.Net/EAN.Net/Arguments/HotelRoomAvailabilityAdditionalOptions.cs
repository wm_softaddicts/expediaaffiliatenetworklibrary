﻿namespace EAN.Net.Arguments
{
	public class HotelRoomAvailabilityAdditionalOptions
	{
		public string SupplierType { get; set; }

		public string RateKey { get; set; }

		public int? NumberOfBedRooms { get; set; }

		public string RoomTypeCode { get; set; }
	}
}
