﻿using EAN.Net.Arguments.Base;

namespace EAN.Net.Arguments
{
	public sealed class LocationInfoByDestinationStringOptions : SearchByDestinationStringOptions, ILocationInfoOptions
	{
		public LocationType LocationType { get; set; }

		public string PostalCode { get; set; }
	}
}
