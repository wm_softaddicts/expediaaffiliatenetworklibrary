﻿using EAN.Net.Arguments.Base;

namespace EAN.Net.Arguments
{
	public sealed class HotelListSearchByAddressOptions: SearchByAddressOptions, IHotelListSearchOptions
	{
		public string PropertyName { get; set; }
	}
}
