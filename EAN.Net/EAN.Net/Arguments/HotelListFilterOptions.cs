﻿namespace EAN.Net.Arguments
{
	public class HotelListFilterOptions
	{
		public bool? IsIncludeSurrounding { get; set; }

		public PropertyCategory? PropertyCategory { get; set; }

		public string Amenities { get; set; }

		public float? MinStarRating { get; set; }

		public float? MaxStarRating { get; set; }

		public float? MinRate { get; set; }

		public float? MaxRate { get; set; }

		public float? NumberOfBedRooms { get; set; }

		public float? MaxRatePlanCount { get; set; }
	}
}
