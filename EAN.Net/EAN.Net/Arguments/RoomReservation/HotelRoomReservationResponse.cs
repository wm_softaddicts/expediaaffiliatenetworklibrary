﻿using System.Collections.Generic;
using EAN.Net.Arguments.Common;
using EAN.Net.Arguments.Response;
using RestSharp.Deserializers;

namespace EAN.Net.Arguments.RoomReservation
{
	public class HotelRoomReservationResponse: ResponseBase
	{
		[DeserializeAs(Name = "itineraryId")]
		public int ItineraryId { get; set; }

		[DeserializeAs(Name = "confirmationNumbers")]
		public List<int> ConfirmationNumbers { get; set; }

		[DeserializeAs(Name = "processedWithConfirmation")]
		public bool IsProcessedWithConfirmation { get; set; }

		[DeserializeAs(Name = "errorText")]
		public string ErrorText { get; set; }

		[DeserializeAs(Name = "hotelReplyText")]
		public string HotelReplyText { get; set; }

		[DeserializeAs(Name = "supplierType")]
		public string SupplierType { get; set; }

		[DeserializeAs(Name = "reservationStatusCode")]
		public string ReservationStatusCode { get; set; }

		[DeserializeAs(Name = "existingItinerary")]
		public bool IsExistingItinerary { get; set; }

		[DeserializeAs(Name = "numberOfRoomsBooked")]
		public int NumberOfRoomsBooked { get; set; }

		[DeserializeAs(Name = "drivingDirections")]
		public string DrivingDirections { get; set; }

		[DeserializeAs(Name = "checkInInstructions")]
		public string CheckInInstructions { get; set; }

		[DeserializeAs(Name = "arrivalDate")]
		public string ArrivalDate { get; set; }

		[DeserializeAs(Name = "departureDate")]
		public string DepartureDate { get; set; }

		[DeserializeAs(Name = "hotelName")]
		public string HotelName { get; set; }

		[DeserializeAs(Name = "hotelAddress")]
		public string HotelAddress { get; set; }

		[DeserializeAs(Name = "hotelCity")]
		public string HotelCity { get; set; }

		[DeserializeAs(Name = "hotelStateProvinceCode")]
		public string HotelStateProvinceCode { get; set; }

		[DeserializeAs(Name = "hotelPostalCode")]
		public string HotelPostalCode { get; set; }

		[DeserializeAs(Name = "hotelCountryCode")]
		public string HotelCountryCode { get; set; }

		[DeserializeAs(Name = "roomDescription")]
		public string RoomDescription { get; set; }

		[DeserializeAs(Name = "tripAdvisorRating")]
		public float TripAdvisorRating { get; set; }

		[DeserializeAs(Name = "tripAdvisorReviewCount")]
		public int TripAdvisorReviewCount { get; set; }

		[DeserializeAs(Name = "tripAdvisorRatingUrl")]
		public string TripAdvisorRatingUrl { get; set; }

		[DeserializeAs(Name = "rateOccupancyPerRoom")]
		public int RateOccupancyPerRoom { get; set; }

		[DeserializeAs(Name = "RateInfos")]
		public RateInfos RateInfos { get; set; }
	}
}
