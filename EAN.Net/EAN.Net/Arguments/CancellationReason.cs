﻿using EAN.Net.Attributes;

namespace EAN.Net.Arguments
{
	public enum CancellationReason
	{
		[EnumCustomValue("HOC")]
		HotelAskedMeToCancel,

		[EnumCustomValue("COP")]
		ChangeOfPlans,

		[EnumCustomValue("FBP")]
		FoundABetterPrice,

		[EnumCustomValue("FBH")]
		FoundABetterHotel,

		[EnumCustomValue("CNL")]
		DecidedToCancelMyPlans,

		[EnumCustomValue("NSY")]
		RatherNotSay,

		[EnumCustomValue("ILL")]
		IllnessOrInjury,

		[EnumCustomValue("DEA")]
		DeathInTheFamily,

		[EnumCustomValue("OTH")]
		Other
	}
}
