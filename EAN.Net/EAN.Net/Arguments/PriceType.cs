﻿using EAN.Net.Attributes;

namespace EAN.Net.Arguments
{
	public enum PriceType
	{
		[EnumCustomValue(1)]
		PricesAreLowerThanOriginalRate,

		[EnumCustomValue(2)]
		PricesAreHigherThanOriginalRate
	}
}
