﻿namespace EAN.Net.Arguments
{
	public sealed class HotelListSearchByGeographicalAreaOptions: IHotelListSearchOptions
	{
		public float Latitude { get; set; }
		public float Longitude { get; set; }
		public int? SearchRadius { get; set; }
		public string SearchRadiusUnit { get; set; }
		public string Sort { get; set; }
	}
}
