﻿namespace EAN.Net.Constants
{
	internal static class ResponseParameters
	{
		public static class Common
		{
			public const string CustomerSessionId = "customerSessionId";
			public const string Error = "EanWSError";
		}

		public static class ResponseRootElements
		{
			public const string HotelListRootElement = "HotelListResponse";
			public const string HotelRoomAvailabilityRootElement = "HotelRoomAvailabilityResponse";
			public const string HotelItineraryRootElement = "HotelItineraryResponse";
			public const string RoomImagesRootElement = "HotelRoomImageResponse";
			public const string PaymentTypesRootElement = "HotelPaymentResponse";
			public const string Ping = "PingResponse";
			public const string HotelInformation = "HotelInformationResponse";
			public const string HotelAlternateProperties = "AlternatePropertyListResponse";
			public const string LocationInfo = "LocationInfoResponse";
			public const string HotelRoomReservation = "HotelRoomReservationResponse";
			public const string HotelRoomCancelReservation = "HotelRoomCancellationResponse";
		}
	}
}
