﻿namespace EAN.Net.Constants
{
	internal static class RequestParameters
	{
		public static class Common
		{
			// ReSharper disable MemberHidesStaticFromOuterClass
			public const string ClientId = "cid";
			public const string ApiKey = "apiKey";
			public const string MinorRev = "minorRev";
			public const string CustomerLocale = "locale";
			public const string CustomerCurrencyCode = "currencyCode";
			public const string CustomerSessionId = "customerSessionId";
			public const string CustomerIpAddress = "customerIpAddress";
			public const string CustomerUserAgent = "customerUserAgent";
			public const string SignatureAuthentication = "sig";
			public const string ResponseFormat = "_type";
			public const string ApiExperience = "apiExperience";
			// ReSharper restore MemberHidesStaticFromOuterClass
		}

		public static class ApiExperience
		{
			public const string PartnerCallCenter = "PARTNER_CALL_CENTER";
			public const string PartnerWebsite = "PARTNER_WEBSITE";
			public const string PartnerMobileWeb = "PARTNER_MOBILE_WEB";
			public const string PartnerMobileApp = "PARTNER_MOBILE_APP";
			public const string PartnerBotCache = "PARTNER_BOT_CACHE";
			public const string PartnerBotReporting = "PARTNER_BOT_REPORTING";
		}

		public static class Room
		{
			public const string NumberOfAdults = "numberOfAdults";
			public const string NumberOfChildren = "numberOfChildren";
			public const string ChildAges = "childAges";
		}

		public static class HotelListCommon
		{
			// ReSharper disable MemberHidesStaticFromOuterClass
			public const string ArrivalDate = "arrivalDate";
			public const string DepartureDate = "departureDate";
			public const string Rooms = "RoomGroup";
			public const string IsIncludeDetails = "includeDetails";
			public const string IsIncludeHotelFeeBreakdown = "includeHotelFeeBreakdown";
			public const string NumberOfResults = "numberOfResults";
			public const string Sort = "sort";
			// ReSharper restore MemberHidesStaticFromOuterClass
		}

		public static class HoteListSearch
		{
			public const string City = "city";
			public const string StateProvinceCode = "stateProvinceCode";
			public const string CountryCode = "countryCode";
			public const string Address = "address";
			public const string PostalCode = "postalCode";
			public const string PropertyName = "propertyName";

			public const string DestinationString = "destinationString";

			public const string DestinationId = "destinationId";

			public const string HotelIdList = "hotelIdList";

			public const string Latitude = "latitude";
			public const string Longitude = "longitude";
			public const string SearchRadius = "searchRadius";
			public const string SearchRadiusUnit = "searchRadiusUnit";
			public const string Sort = "sort";

			public const string DetailOptions = "options";
		}

		public static class HoteListSearchNext
		{
			public const string CacheLocation = "cacheLocation";
			public const string CacheKey = "cacheKey";
		}

		public static class HotelListFilter
		{
			public const string IsIncludeSurrounding = "includeSurrounding";
			public const string MinStarRating = "minStarRating";
			public const string MaxStarRating = "maxStarRating";
			public const string MinRate = "minRate";
			public const string MaxRate = "maxRate";
			public const string NumberOfBedRooms = "numberOfBedRooms";
			public const string MaxRatePlanCount = "maxRatePlanCount";
		}

		public static class HotelListAdditionalSearch
		{
			public const string Address = "address";
			public const string PostalCode = "postalCode";
			public const string PropertyName = "propertyName";
		}

		public static class RoomAvaliability
		{
			public const string HotelId = "hotelId";
			public const string ArrivalDate = "arrivalDate";
			public const string DepartureDate = "departureDate";
			public const string NumberOfBedRooms = "numberOfBedRooms";
			public const string SupplierType = "supplierType";
			public const string RateKey = "rateKey";
			public const string RoomTypeCode = "roomTypeCode";
			public const string RateCode = "rateCode";
			public const string IsIncludeDetails = "includeDetails";
			public const string IsIncludeRoomImages = "includeRoomImages";
			public const string IsIncludeHotelFeeBreakdown = "includeHotelFeeBreakdown";
			public const string DetailOptions = "options";
		}

		public static class Itinerary
		{
			public const string ItineraryId = "itineraryId";
			public const string AffiliateConfirmationId = "affiliateConfirmationId";
			public const string Email = "email";
			public const string CreditCardNumber = "creditCardNumber";
			public const string LastName = "lastName";
			public const string ConfirmationExtras = "confirmationExtras";
			public const string IsResendConfirmationEmail = "resendConfirmationEmail";
			public const string CreationDateStart = "creationDateStart";
			public const string CreationDateEnd = "creationDateEnd";
			public const string DepartureDateStart = "departureDateStart";
			public const string DepartureDateEnd = "departureDateEnd";
			public const string IsIncludeChildAffiliates = "includeChildAffiliates";
		}

		public static class RoomImages
		{
			public const string HotelId = "hotelId";
		}

		public static class PaymentTypes
		{
			public const string HotelId = "hotelId";
			public const string SupplierType = "supplierType";
			public const string RateType = "rateType";
		}

		public static class Ping
		{
			public const string EchoValue = "echo";
		}

		public static class HotelInformation
		{
			public const string HotelId = "hotelId";
			public const string DetailOptions = "options";
		}

		public static class HotelAlternateProperties
		{
			public const string HotelId = "originalHotelId";
			public const string ArrivalDate = "arrivalDate";
			public const string DepartureDate = "departureDate";
			public const string OriginalAverageNightlyRate = "originalAverageNightlyRate";
			public const string PriceType = "priceType";
		}

		public static class LocationInfo
		{
			public const string LocationType = "type";
			public const string DestinationString = "destinationString";
			public const string Address = "address";
			public const string City = "city";
			public const string StateProvinceCode = "stateProvinceCode";
			public const string CountryCode = "countryCode";
			public const string PostalCode = "postalCode";
			public const string DestinationId = "destinationId";
			public const string IsIgnoreSearchWeight = "ignoreSearchWeight";
			public const string IsUseGeoCoder = "useGeoCoder";
		}

		public static class RoomReservation
		{
			public const string FirstName = "FirstName";
			public const string LastName = "LastName";
			public const string BedTypeId = "BedTypeId";
			public const string NumberOfBeds = "NumberOfBeds";
			public const string SmokingPreference = "SmokingPreference";
		}

		public static class HotelRoomReservation
		{
			public const string AdditionalData = "additionalData";
			
			public const string HotelId = "hotelId";
			public const string ArrivalDate = "arrivalDate";
			public const string DepartureDate = "departureDate";
			public const string SupplierType = "supplierType";
			public const string RateKey = "rateKey";
			public const string RoomTypeCode = "roomTypeCode";
			public const string RateCode = "rateCode";
			public const string AffiliateConfirmationId = "affiliateConfirmationId";
			public const string AffiliateCustomerId = "affiliateCustomerId";
			public const string ItineraryId = "itineraryId";
			public const string ChargeableRate = "chargeableRate";
			public const string SpecialInformation = "specialInformation";
			public const string IsSendReservationEmail = "sendReservationEmail";

			public const string Email = "email";
			public const string FirstName = "firstName";
			public const string LastName = "lastName";
			public const string HomePhone = "homePhone";
			public const string WorkPhone = "workPhone";
			public const string Extension = "extension";
			public const string FaxPhone = "faxPhone";
			public const string CompanyName = "companyName";
			public const string CreditCardType = "creditCardType";
			public const string CreditCardNumber = "creditCardNumber";
			public const string CreditCardIdentifier = "creditCardIdentifier";
			public const string CreditCardExpirationMonth = "creditCardExpirationMonth";
			public const string CreditCardExpirationYear = "creditCardExpirationYear";

			public const string Address1 = "address1";
			public const string Address2 = "address2";
			public const string Address3 = "address3";
			public const string City = "city";
			public const string StateProvinceCode = "stateProvinceCode";
			public const string CountryCode = "countryCode";
			public const string PostalCode = "postalCode";
		}

		public static class HotelRoomCancelReservation
		{
			public const string ItineraryId = "itineraryId";
			public const string Email = "email";
			public const string ConfirmationNumber = "confirmationNumber";
			public const string Reason = "reason";
		}
	}
}