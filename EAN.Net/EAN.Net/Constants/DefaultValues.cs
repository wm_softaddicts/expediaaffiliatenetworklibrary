﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAN.Net.Constants
{
	internal static class DefaultValues
	{
		public static class Common
		{
			public const string CurrencyCode = "USD";
			public const string Locale = "en_US";
		}
	}
}
