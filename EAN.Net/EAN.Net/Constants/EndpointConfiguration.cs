﻿namespace EAN.Net.Constants
{
	public static class EndpointConfiguration
	{
		public const string ApiUrl = "http://api.ean.com/ean-services/rs/hotel/v3/";
		public const string DevApiUrl = "http://dev.api.ean.com/ean-services/rs/hotel/v3/";
		public const string BookingApiUrl = "https://book.api.ean.com/ean-services/rs/hotel/v3";
		public const string DevBookingApiUrl = "https://dev.api.ean.com/ean-services/rs/hotel/v3";

		public const string HotelListUrlSection = "list";
		public const string HotelRoomAvailabilityUrlSection = "avail";
		public const string HotelItineraryUrlSection = "itin";
		public const string RoomImagesUrlSection = "roomImages";
		public const string PaymentTypesUrlSection = "paymentInfo";
		public const string Ping = "ping";
		public const string HotelInformation = "info";
		public const string HotelAlternateProperties = "altProps";
		public const string LocationInfo = "geoSearch";
		public const string HotelRoomReservation = "res";
		public const string HotelRoomCancelReservation = "cancel";
	}
}
