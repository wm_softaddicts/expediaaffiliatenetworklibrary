﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using EAN.Net.Arguments;
using EAN.Net.Arguments.Common;
using EAN.Net.Attributes;
using EAN.Net.Constants;
using EAN.Net.Extensions;
using RestSharp;

#if DEBUG
[assembly: InternalsVisibleTo("EAN.Net.Tests")]
#endif

namespace EAN.Net
{
	internal static class RestParameterFiller
	{
		public static void AppendParameter(IRestRequest restRequest, string name, object value)
		{
			if (value == null)
				return;

			restRequest.AddParameter(name, value);
		}

		public static void AppendParameter(IRestRequest restRequest, string name, float value)
		{
			restRequest.AddParameter(name, value.ToString(CultureInfo.InvariantCulture));
		}

		public static void AppendParameter(IRestRequest restRequest, string name, double value)
		{
			restRequest.AddParameter(name, value.ToString(CultureInfo.InvariantCulture));
		}

		public static void AppendParameter(IRestRequest restRequest, string name, DateTime value)
		{
			if (value == default(DateTime))
				return;

			//AppendParameter(restRequest, name, value.ToString("MM/dd/yyyy"));
			AppendParameter(restRequest, name, value.ToString(@"MM\/dd\/yyyy"));
		}

		public static void AppendParameter(IRestRequest restRequest, string name, DateTime? value)
		{
			if (value == null)
				return;

			AppendParameter(restRequest, name, (DateTime) value);
		}

		public static void AppendParameter(IRestRequest restRequest, string name, ICollection<string> value)
		{
			if (value == null || !value.Any())
				return;

			AppendParameter(restRequest, name, string.Join(",", value));
		}

		public static void AppendParameter<TKey, TValue>(IRestRequest restRequest, string name, IDictionary<TKey, TValue> value)
		{
			if (value == null)
				return;

			var paramValue = string.Join(";", value.Select(kvp => string.Join("=", kvp.Key, kvp.Value)));

			restRequest.AddParameter(name, paramValue);
		}

		private static object GetEnumValue(Enum en)
		{
			var customValueAttr = en.GetType().GetField(en.ToString()).GetFirstAttribute<EnumCustomValueAttribute>();

			return customValueAttr != null
				? customValueAttr.Value
				: en;
		}

		public static void AppendParameter(IRestRequest restRequest, string name, Enum value)
		{
			if (value == null)
				return;

			var isFlag = value.GetType().GetFirstAttribute<FlagsAttribute>() != null;

			if (isFlag)
			{
				var flags = (from Enum e in Enum.GetValues(value.GetType()) where value.HasFlag(e) select GetEnumValue(e));

				AppendParameter(restRequest, name, string.Join(",", flags));
			}
			else
			{
				AppendParameter(restRequest, name, GetEnumValue(value));
			}
		}

		public static void AppendParameter(IRestRequest restRequest, string name, Room value)
		{
			AppendParameter(restRequest, name, string.Join(",", new[] { value.NumberOfAdults }.Concat(value.ChildAges)));

			var roomReservaion = value as RoomReservation;
			if (roomReservaion == null)
				return;

			AppendParameter(restRequest, name + RequestParameters.RoomReservation.FirstName, roomReservaion.FirstName);
			AppendParameter(restRequest, name + RequestParameters.RoomReservation.LastName, roomReservaion.LastName);
			AppendParameter(restRequest, name + RequestParameters.RoomReservation.BedTypeId, roomReservaion.BedTypeId);
			AppendParameter(restRequest, name + RequestParameters.RoomReservation.NumberOfBeds, roomReservaion.NumberOfBeds);
			AppendParameter(restRequest, name + RequestParameters.RoomReservation.SmokingPreference, roomReservaion.SmokingPreference);
		}

		public static void AppendParameter(IRestRequest restRequest, IEnumerable<Room> value)
		{
			var roomNum = 1;

			foreach (var room in value)
			{
				AppendParameter(restRequest, string.Format("room{0}", roomNum++), room);
			}
		}

		public static void AppendParameter(IRestRequest restRequest, HotelListSearchByAddressOptions value)
		{
			if (value == null)
				return;

			AppendParameter(restRequest, RequestParameters.HoteListSearch.CountryCode, value.CountryCode);
			AppendParameter(restRequest, RequestParameters.HoteListSearch.StateProvinceCode, value.StateProvinceCode);
			AppendParameter(restRequest, RequestParameters.HoteListSearch.City, value.City);
			AppendParameter(restRequest, RequestParameters.HoteListSearch.PostalCode, value.PostalCode);
			AppendParameter(restRequest, RequestParameters.HoteListSearch.Address, value.Address);
			AppendParameter(restRequest, RequestParameters.HoteListSearch.PropertyName, value.PropertyName);
		}

		public static void AppendParameter(IRestRequest restRequest, HotelListSearchByDestinationIdOptions value)
		{
			if (value == null)
				return;

			AppendParameter(restRequest, RequestParameters.HoteListSearch.DestinationId, value.DestinationId);
		}

		public static void AppendParameter(IRestRequest restRequest, HotelListSearchByDestinationStringOptions value)
		{
			if (value == null)
				return;

			AppendParameter(restRequest, RequestParameters.HoteListSearch.DestinationString, value.DestinationString);
		}

		public static void AppendParameter(IRestRequest restRequest, HotelListSearchByGeographicalAreaOptions value)
		{
			if (value == null)
				return;

			AppendParameter(restRequest, RequestParameters.HoteListSearch.Latitude, value.Latitude);
			AppendParameter(restRequest, RequestParameters.HoteListSearch.Longitude, value.Longitude);
			AppendParameter(restRequest, RequestParameters.HoteListSearch.SearchRadius, value.SearchRadius);
			AppendParameter(restRequest, RequestParameters.HoteListSearch.SearchRadiusUnit, value.SearchRadiusUnit);
			AppendParameter(restRequest, RequestParameters.HoteListSearch.Sort, value.Sort);
		}

		public static void AppendParameter(IRestRequest restRequest, HotelListSearchByHotelIdListOptions value)
		{
			if (value == null)
				return;

			AppendParameter(restRequest, RequestParameters.HoteListSearch.HotelIdList, string.Join(",", value.HotelIdList));
		}

		public static void AppendParameter(IRestRequest restRequest, IHotelListSearchOptions value)
		{
			if (value == null)
				return;

			AppendParameter(restRequest, value as HotelListSearchByAddressOptions);
			AppendParameter(restRequest, value as HotelListSearchByDestinationIdOptions);
			AppendParameter(restRequest, value as HotelListSearchByDestinationStringOptions);
			AppendParameter(restRequest, value as HotelListSearchByGeographicalAreaOptions);
			AppendParameter(restRequest, value as HotelListSearchByHotelIdListOptions);
		}

		public static void AppendParameter(IRestRequest restRequest, HotelListFilterOptions value)
		{
			if (value == null)
				return;

			AppendParameter(restRequest, RequestParameters.HotelListFilter.IsIncludeSurrounding, value.IsIncludeSurrounding);
			AppendParameter(restRequest, RequestParameters.HotelListFilter.MinStarRating, value.MinStarRating);
			AppendParameter(restRequest, RequestParameters.HotelListFilter.MaxStarRating, value.MaxStarRating);
			AppendParameter(restRequest, RequestParameters.HotelListFilter.MinRate, value.MinRate);
			AppendParameter(restRequest, RequestParameters.HotelListFilter.MaxRate, value.MaxRate);
			AppendParameter(restRequest, RequestParameters.HotelListFilter.NumberOfBedRooms, value.NumberOfBedRooms);
			AppendParameter(restRequest, RequestParameters.HotelListFilter.MaxRatePlanCount, value.MaxRatePlanCount);
		}

		public static void AppendParameter(IRestRequest restRequest, HotelListAdditionalSearchOptions value)
		{
			if (value == null)
				return;

			AppendParameter(restRequest, RequestParameters.HotelListAdditionalSearch.Address, value.Address);
			AppendParameter(restRequest, RequestParameters.HotelListAdditionalSearch.PostalCode, value.PostalCode);
			AppendParameter(restRequest, RequestParameters.HotelListAdditionalSearch.PropertyName, value.PropertyName);
		}

		public static void AppendParameter(IRestRequest restRequest, HotelRoomAvailabilityAdditionalOptions value)
		{
			if (value == null)
				return;

			AppendParameter(restRequest, RequestParameters.RoomAvaliability.SupplierType, value.SupplierType);
			AppendParameter(restRequest, RequestParameters.RoomAvaliability.RateKey, value.RateKey);
			AppendParameter(restRequest, RequestParameters.RoomAvaliability.NumberOfBedRooms, value.NumberOfBedRooms);
			AppendParameter(restRequest, RequestParameters.RoomAvaliability.RoomTypeCode, value.RoomTypeCode);
		}

		public static void AppendParameter(IRestRequest restRequest, HotelItineraryAdditionalOptions value)
		{
			if (value == null)
				return;

			AppendParameter(restRequest, RequestParameters.Itinerary.IsIncludeChildAffiliates, value.IsIncludeChildAffiliates);
			AppendParameter(restRequest, RequestParameters.Itinerary.IsResendConfirmationEmail, value.IsResendConfirmationEmail);
			AppendParameter(restRequest, RequestParameters.Itinerary.ConfirmationExtras, value.ConfirmationExtras);
		}

		public static void AppendParameter(IRestRequest restRequest, LocationInfoByAddressOptions value)
		{
			if (value == null)
				return;

			AppendParameter(restRequest, RequestParameters.LocationInfo.CountryCode, value.CountryCode);
			AppendParameter(restRequest, RequestParameters.LocationInfo.StateProvinceCode, value.StateProvinceCode);
			AppendParameter(restRequest, RequestParameters.LocationInfo.City, value.City);
			AppendParameter(restRequest, RequestParameters.LocationInfo.PostalCode, value.PostalCode);
			AppendParameter(restRequest, RequestParameters.LocationInfo.Address, value.Address);
		}

		public static void AppendParameter(IRestRequest restRequest, LocationInfoByDestinationIdOptions value)
		{
			if (value == null)
				return;

			AppendParameter(restRequest, RequestParameters.LocationInfo.DestinationId, value.DestinationId);
		}

		public static void AppendParameter(IRestRequest restRequest, LocationInfoByDestinationStringOptions value)
		{
			if (value == null)
				return;

			AppendParameter(restRequest, RequestParameters.LocationInfo.DestinationString, value.DestinationString);
			AppendParameter(restRequest, RequestParameters.LocationInfo.LocationType, value.LocationType);
			AppendParameter(restRequest, RequestParameters.LocationInfo.PostalCode, value.PostalCode);
		}

		public static void AppendParameter(IRestRequest restRequest, ILocationInfoOptions value)
		{
			AppendParameter(restRequest, value as LocationInfoByAddressOptions);
			AppendParameter(restRequest, value as LocationInfoByDestinationIdOptions);
			AppendParameter(restRequest, value as LocationInfoByDestinationStringOptions);
		}

		public static void AppendParameter(IRestRequest restRequest, ReservationOptions value)
		{
			if (value == null)
				return;

			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.Email, value.Email);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.FirstName, value.FirstName);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.LastName, value.LastName);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.HomePhone, value.HomePhone);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.WorkPhone, value.WorkPhone);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.Extension, value.PhoneExtension);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.FaxPhone, value.FaxPhone);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.CompanyName, value.CompanyName);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.CreditCardType, value.CreditCardType);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.CreditCardNumber, value.CreditCardNumber);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.CreditCardIdentifier, value.CreditCardIdentifier);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.CreditCardExpirationMonth, value.CreditCardExpirationMonth);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.CreditCardExpirationYear, value.CreditCardExpirationYear);
		}

		public static void AppendParameter(IRestRequest restRequest, AddressOptions value)
		{
			if (value == null)
				return;

			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.CountryCode, value.CountryCode);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.StateProvinceCode, value.StateProvinceCode);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.City, value.City);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.PostalCode, value.PostalCode);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.Address1, value.Address1);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.Address2, value.Address2);
			AppendParameter(restRequest, RequestParameters.HotelRoomReservation.Address3, value.Address3);
		}
	}
}
