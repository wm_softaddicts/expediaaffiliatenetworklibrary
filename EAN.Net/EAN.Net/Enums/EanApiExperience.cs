﻿using EAN.Net.Attributes;
using EAN.Net.Constants;

namespace EAN.Net.Enums
{
	public enum EanApiExperience
	{
		[EnumCustomValue(RequestParameters.ApiExperience.PartnerCallCenter)]
		PartnerCallCenter,

		[EnumCustomValue(RequestParameters.ApiExperience.PartnerWebsite)]
		PartnerWebsite,

		[EnumCustomValue(RequestParameters.ApiExperience.PartnerMobileWeb)]
		PartnerMobileWeb,

		[EnumCustomValue(RequestParameters.ApiExperience.PartnerMobileApp)]
		PartnerMobileApp,

		[EnumCustomValue(RequestParameters.ApiExperience.PartnerBotCache)]
		PartnerBotCache,

		[EnumCustomValue(RequestParameters.ApiExperience.PartnerBotReporting)]
		PartnerBotReporting
	}
}
