﻿using EAN.Net.Attributes;

namespace EAN.Net.Enums
{
	public enum ResponseFormat
	{
		[EnumCustomValue("xml")]
		Xml,

		[EnumCustomValue("json")]
		Json
	}
}
