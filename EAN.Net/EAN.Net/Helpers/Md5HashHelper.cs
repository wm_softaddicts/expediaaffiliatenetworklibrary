﻿using System.Security.Cryptography;
using System.Text;

namespace EAN.Net.Helpers
{
	internal static class Md5HashHelper
	{
		public static string GetMd5HashString(string input)
		{
			using (var md5Hash = MD5.Create())
			{
				var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
				
				var builder = new StringBuilder();
				foreach (var ch in data)
				{
					builder.Append(ch.ToString("x2"));
				}

				return builder.ToString();
			}
		}
	}
}
