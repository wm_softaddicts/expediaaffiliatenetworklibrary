﻿using System;

namespace EAN.Net.Helpers
{
	internal static class DateTimeHelper
	{
		public static int GetUnixTimeStamp(DateTime time)
		{
			return Convert.ToInt32((time.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
		}
	}
}
