﻿using System;
using System.Collections.Generic;
using System.Net;
using EAN.Net.Arguments;
using EAN.Net.Arguments.Common;
using EAN.Net.Arguments.Response;
using EAN.Net.Arguments.Response.AlternateProperties;
using EAN.Net.Arguments.Response.CancelReservation;
using EAN.Net.Arguments.Response.HotelInformation;
using EAN.Net.Arguments.Response.HotelList;
using EAN.Net.Arguments.Response.Itinerary;
using EAN.Net.Arguments.Response.LocationInfo;
using EAN.Net.Arguments.Response.PaymentTypes;
using EAN.Net.Arguments.Response.Ping;
using EAN.Net.Arguments.Response.RoomAvailability;
using EAN.Net.Arguments.Response.RoomImages;
using EAN.Net.Arguments.RoomReservation;
using EAN.Net.Constants;
using EAN.Net.Enums;
using EAN.Net.Extensions;
using RestSharp;

namespace EAN.Net
{
	public class EanCustomerSession
	{
		private string _currencyCode = DefaultValues.Common.CurrencyCode;
		private string _locale = DefaultValues.Common.Locale;

		public EanCustomerSession(EanWebClient client, string ipAddress, string userAgent, string currencyCode = DefaultValues.Common.CurrencyCode)
		{
			SessionId = Guid.NewGuid().ToString();

			Client = client;
			IpAddress = IPAddress.Parse(ipAddress);
			UserAgent = userAgent;
			CurrencyCode = currencyCode;
		}

		public EanWebClient Client { get; private set; }

		public string SessionId { get; private set; }

		public IPAddress IpAddress { get; private set; }

		public string UserAgent { get; private set; }

		public string Locale
		{
			get { return _locale; }
			set { _locale = value; }
		}

		public string CurrencyCode
		{
			get { return _currencyCode; }
			set { _currencyCode = value; }
		}

		internal void FillCommonRequestParameters(IRestRequest restRequest)
		{
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Common.MinorRev, EanWebClient.ApiRevision);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Common.ClientId, Client.ClientId);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Common.ApiKey, Client.ApiKey);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Common.CustomerSessionId, SessionId);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Common.CustomerIpAddress, IpAddress);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Common.CustomerUserAgent, UserAgent);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Common.CustomerLocale, Locale);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Common.CustomerCurrencyCode, CurrencyCode);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Common.ResponseFormat, ResponseFormat.Json);
		}

		private static TResponse GetResponse<TResponse>(IRestClient restClient, IRestRequest restRequest)
			where TResponse: ResponseBase, new()
		{
			var response = restClient.CustomExecute<TResponse>(restRequest);
			var responseData = response.Data ?? new TResponse();

			responseData.ErrorException = response.ErrorException;

			return responseData;
		}

		public HotelListResponse GetHotelList(DateTime arrivalDate, DateTime departureDate, 
			IEnumerable<Room> roomOptions, IHotelListSearchOptions searchOptions, 
			HotelListAdditionalSearchOptions additionalSearchOptions = null,
			HotelListFilterOptions filterOptions = null, HotelListDetailOptions detailOptions = HotelListDetailOptions.Default,
			bool isIncludeDetails = false, bool isIncludeHotelFeeBreakdown = false, 
			int numberOfResults = 20, HotelListSort sort = HotelListSort.NoSort)
		{
			var restClient = new RestClient(Client.GetApiUrl());
			var restRequest = new RestRequest(EndpointConfiguration.HotelListUrlSection, Method.GET)
			{
				RootElement = ResponseParameters.ResponseRootElements.HotelListRootElement
			};

			FillCommonRequestParameters(restRequest);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Common.ApiExperience, Client.ApiExperience);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelListCommon.ArrivalDate, arrivalDate);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelListCommon.DepartureDate, departureDate);

			RestParameterFiller.AppendParameter(restRequest, roomOptions);
			RestParameterFiller.AppendParameter(restRequest, searchOptions);
			RestParameterFiller.AppendParameter(restRequest, additionalSearchOptions);
			RestParameterFiller.AppendParameter(restRequest, filterOptions);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelListCommon.IsIncludeDetails, isIncludeDetails);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelListCommon.IsIncludeHotelFeeBreakdown, isIncludeHotelFeeBreakdown);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelListCommon.NumberOfResults, numberOfResults);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelListCommon.Sort, sort);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HoteListSearch.DetailOptions, detailOptions);

			return GetResponse<HotelListResponse>(restClient, restRequest);
		}

		public HotelListResponse GetHotelListNextResults(string cacheLocation, string cacheKey)
		{
			if (string.IsNullOrEmpty(cacheLocation) || string.IsNullOrEmpty(cacheKey))
				return null;

			var restClient = new RestClient(Client.GetApiUrl());
			var restRequest = new RestRequest(EndpointConfiguration.HotelListUrlSection, Method.GET)
			{
				RootElement = ResponseParameters.ResponseRootElements.HotelListRootElement,
			};

			FillCommonRequestParameters(restRequest);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HoteListSearchNext.CacheLocation, cacheLocation);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HoteListSearchNext.CacheKey, cacheKey);

			return GetResponse<HotelListResponse>(restClient, restRequest);
		}

		public HotelListResponse GetHotelListNextResults(HotelListResponse previousResponse)
		{
			if (previousResponse == null || string.IsNullOrEmpty(previousResponse.CacheLocation) || string.IsNullOrEmpty(previousResponse.CacheKey))
				return null;

			return GetHotelListNextResults(previousResponse.CacheLocation, previousResponse.CacheKey);
		}

		public HotelRoomAvailabilityResponse GetHotelRoomAvailability(DateTime arrivalDate, DateTime departureDate, long hotelId,
			IEnumerable<Room> roomOptions, HotelRoomAvailabilityAdditionalOptions additionalOptions = null, 
			HotelRoomAvailabilityDetailOptions detailOptions = HotelRoomAvailabilityDetailOptions.Default,
			bool isIncludeDetails = false, bool isIncludeHotelFeeBreakdown = false, bool isIncludeRoomImages = false)
		{
			var restClient = new RestClient(Client.GetApiUrl());
			var restRequest = new RestRequest(EndpointConfiguration.HotelRoomAvailabilityUrlSection, Method.GET)
			{
				RootElement = ResponseParameters.ResponseRootElements.HotelRoomAvailabilityRootElement,
			};

			FillCommonRequestParameters(restRequest);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.RoomAvaliability.ArrivalDate, arrivalDate);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.RoomAvaliability.DepartureDate, departureDate);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.RoomAvaliability.HotelId, hotelId);

			RestParameterFiller.AppendParameter(restRequest, roomOptions);
			RestParameterFiller.AppendParameter(restRequest, additionalOptions);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.RoomAvaliability.DetailOptions, detailOptions);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.RoomAvaliability.IsIncludeDetails, hotelId);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.RoomAvaliability.IsIncludeHotelFeeBreakdown, isIncludeHotelFeeBreakdown);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.RoomAvaliability.IsIncludeRoomImages, isIncludeRoomImages);

			return GetResponse<HotelRoomAvailabilityResponse>(restClient, restRequest);
		}

		public HotelItineraryResponse GetHotelItineraries(long? itineraryId = null, string affiliateConfirmationId = null, string email = null,
			string lastName = null, string creditCardNumber = null,
			DateTime? creationDateStart = null, DateTime? creationDateEnd = null,
			DateTime? departureDateStart = null, DateTime? departureDateEnd = null,
			HotelItineraryAdditionalOptions additionalOptions = null)
		{
			var restClient = new RestClient(Client.GetApiUrl());
			var restRequest = new RestRequest(EndpointConfiguration.HotelItineraryUrlSection, Method.GET)
			{
				RootElement = ResponseParameters.ResponseRootElements.HotelItineraryRootElement,
			};

			FillCommonRequestParameters(restRequest);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Itinerary.ItineraryId, itineraryId);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Itinerary.AffiliateConfirmationId, affiliateConfirmationId);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Itinerary.Email, email);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Itinerary.LastName, lastName);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Itinerary.CreditCardNumber, creditCardNumber);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Itinerary.CreationDateStart, creationDateStart);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Itinerary.CreationDateEnd, creationDateEnd);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Itinerary.DepartureDateStart, departureDateStart);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Itinerary.DepartureDateEnd, departureDateEnd);

			RestParameterFiller.AppendParameter(restRequest, additionalOptions);

			return GetResponse<HotelItineraryResponse>(restClient, restRequest);
		}

		public HotelRoomImageResponse GetRoomImages(long hotelId)
		{
			var restClient = new RestClient(Client.GetApiUrl());
			var restRequest = new RestRequest(EndpointConfiguration.RoomImagesUrlSection, Method.GET)
			{
				RootElement = ResponseParameters.ResponseRootElements.RoomImagesRootElement,
			};

			FillCommonRequestParameters(restRequest);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.RoomImages.HotelId, hotelId);

			return GetResponse<HotelRoomImageResponse>(restClient, restRequest);
		}

		public HotelPaymentResponse GetPaymentTypes(long? hotelId, string supplierType, string rateType)
		{
			var restClient = new RestClient(Client.GetApiUrl());
			var restRequest = new RestRequest(EndpointConfiguration.PaymentTypesUrlSection, Method.GET)
			{
				RootElement = ResponseParameters.ResponseRootElements.PaymentTypesRootElement,
			};

			FillCommonRequestParameters(restRequest);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.PaymentTypes.HotelId, hotelId);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.PaymentTypes.SupplierType, supplierType);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.PaymentTypes.RateType, rateType);

			return GetResponse<HotelPaymentResponse>(restClient, restRequest);
		}

		public HotelPaymentResponse GetPaymentTypes()
		{
			return GetPaymentTypes(null, null, null);
		}

		public PingResponse Ping(string echoValue = null)
		{
			var restClient = new RestClient(Client.GetApiUrl());
			var restRequest = new RestRequest(EndpointConfiguration.Ping, Method.GET)
			{
				RootElement = ResponseParameters.ResponseRootElements.Ping,
			};

			FillCommonRequestParameters(restRequest);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Ping.EchoValue, echoValue);

			return GetResponse<PingResponse>(restClient, restRequest);
		}

		public HotelInformationResponse GetHotelInformation(long hotelId,
			HotelInformationDetailOptions detailOptions = HotelInformationDetailOptions.Default)
		{
			var restClient = new RestClient(Client.GetApiUrl());
			var restRequest = new RestRequest(EndpointConfiguration.HotelInformation, Method.GET)
			{
				RootElement = ResponseParameters.ResponseRootElements.HotelInformation,
			};

			FillCommonRequestParameters(restRequest);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelInformation.HotelId, hotelId);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelInformation.DetailOptions, detailOptions);

			return GetResponse<HotelInformationResponse>(restClient, restRequest);
		}

		public AlternatePropertyListResponse GetHotelAlternateProperties(long hotelId, DateTime arrivalDate, DateTime departureDate, 
			IEnumerable<Room> roomOptions, float averageNightlyRate, PriceType? priceType = null)
		{
			var restClient = new RestClient(Client.GetApiUrl());
			var restRequest = new RestRequest(EndpointConfiguration.HotelAlternateProperties, Method.GET)
			{
				RootElement = ResponseParameters.ResponseRootElements.HotelAlternateProperties,
			};

			FillCommonRequestParameters(restRequest);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelAlternateProperties.HotelId, hotelId);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelAlternateProperties.ArrivalDate, arrivalDate);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelAlternateProperties.DepartureDate, departureDate);

			RestParameterFiller.AppendParameter(restRequest, roomOptions);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelAlternateProperties.OriginalAverageNightlyRate, averageNightlyRate);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelAlternateProperties.PriceType, priceType);

			return GetResponse<AlternatePropertyListResponse>(restClient, restRequest);
		}

		public LocationInfoResponse GetLocationInfo(ILocationInfoOptions searchOptions, bool isIgnoreSearchWeight = false,
			bool isUseGeoCoder = false)
		{
			var restClient = new RestClient(Client.GetApiUrl());
			var restRequest = new RestRequest(EndpointConfiguration.LocationInfo, Method.GET)
			{
				RootElement = ResponseParameters.ResponseRootElements.LocationInfo,
			};

			FillCommonRequestParameters(restRequest);

			RestParameterFiller.AppendParameter(restRequest, searchOptions);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.LocationInfo.IsIgnoreSearchWeight, isIgnoreSearchWeight);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.LocationInfo.IsUseGeoCoder, isUseGeoCoder);

			return GetResponse<LocationInfoResponse>(restClient, restRequest);
		}

		public HotelRoomReservationResponse MakeReservation(long hotelId, DateTime arrivalDate, DateTime departureDate,
			string rateKey, string roomTypeCode, string rateCode,
			IEnumerable<RoomReservation> roomOptions, ReservationOptions reservationOptions, AddressOptions addressOptions,
			float chargeableRate, string supplierType, 
			IDictionary<string, string> additionalData = null, string affiliateConfirmationId = null, 
			string affiliateCustomerId = null, long? itineraryId = null, string specialInformation = null, 
			bool isSendReservationEmail = false)
		{
			var restClient = new RestClient(Client.GetBookingApiUrl());
			var restRequest = new RestRequest(EndpointConfiguration.HotelRoomReservation, Method.POST)
			{
				RootElement = ResponseParameters.ResponseRootElements.HotelRoomReservation,
			};

			FillCommonRequestParameters(restRequest);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.Common.ApiExperience, Client.ApiExperience);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomReservation.HotelId, hotelId);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomReservation.ArrivalDate, arrivalDate);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomReservation.DepartureDate, departureDate);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomReservation.RateKey, rateKey);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomReservation.RoomTypeCode, roomTypeCode);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomReservation.RateCode, rateCode);

			RestParameterFiller.AppendParameter(restRequest, roomOptions);
			RestParameterFiller.AppendParameter(restRequest, reservationOptions);
			RestParameterFiller.AppendParameter(restRequest, addressOptions);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomReservation.ChargeableRate, chargeableRate);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomReservation.SupplierType, supplierType);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomReservation.AdditionalData, additionalData);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomReservation.AffiliateConfirmationId, affiliateConfirmationId);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomReservation.AffiliateCustomerId, affiliateCustomerId);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomReservation.ItineraryId, itineraryId);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomReservation.SpecialInformation, specialInformation);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomReservation.IsSendReservationEmail, isSendReservationEmail);

			return GetResponse<HotelRoomReservationResponse>(restClient, restRequest);
		}

		public HotelRoomCancellationResponse CancelReservation(long? itineraryId, string email, string confirmationNumber, CancellationReason reason)
		{
			var restClient = new RestClient(Client.GetBookingApiUrl());
			var restRequest = new RestRequest(EndpointConfiguration.HotelRoomCancelReservation, Method.GET)
			{
				RootElement = ResponseParameters.ResponseRootElements.HotelRoomCancelReservation,
			};

			FillCommonRequestParameters(restRequest);

			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomCancelReservation.ItineraryId, itineraryId);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomCancelReservation.Email, email);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomCancelReservation.ConfirmationNumber, confirmationNumber);
			RestParameterFiller.AppendParameter(restRequest, RequestParameters.HotelRoomCancelReservation.Reason, reason);

			return GetResponse<HotelRoomCancellationResponse>(restClient, restRequest);
		}
	}
}
