﻿using System;

namespace EAN.Net.Attributes
{
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
	public class EnumCustomValueAttribute: Attribute
	{
		public object Value { get; private set; }

		public EnumCustomValueAttribute(object value)
		{
			Value = value;
		}
	}
}
