﻿using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using RestSharp;

namespace EAN.Net.Extensions
{
	public static class RestRequestExtension
	{
		public static bool CheckRemoteSertificate(object sender, X509Certificate certificate, X509Chain chain,
			SslPolicyErrors sslPolicyErrors)
		{
			return true;
		}

		public static IRestResponse CustomExecute(this IRestClient restClient, IRestRequest request)
		{
			ServicePointManager.ServerCertificateValidationCallback += CheckRemoteSertificate;

			var result = restClient.Execute(request);

			// ReSharper disable once DelegateSubtraction
			ServicePointManager.ServerCertificateValidationCallback -= CheckRemoteSertificate;

			return result;
		}

		public static IRestResponse<T> CustomExecute<T>(this IRestClient restClient, IRestRequest request)
			where T : new()
		{
			ServicePointManager.ServerCertificateValidationCallback += CheckRemoteSertificate;

			var result = restClient.Execute<T>(request);

			// ReSharper disable once DelegateSubtraction
			ServicePointManager.ServerCertificateValidationCallback -= CheckRemoteSertificate;

			return result;
		}
	}
}
