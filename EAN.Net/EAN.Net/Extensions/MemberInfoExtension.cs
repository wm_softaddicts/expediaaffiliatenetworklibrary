﻿using System;
using System.Linq;
using System.Reflection;

namespace EAN.Net.Extensions
{
	public static class MemberInfoExtension
	{
		public static TAttribute GetFirstAttribute<TAttribute>(this MemberInfo memberInfo)
			where TAttribute: Attribute
		{
			return memberInfo.GetCustomAttributes(typeof (TAttribute), true)
				.Cast<TAttribute>()
				.FirstOrDefault();
		}
	}
}
