﻿using System;
using EAN.Net.Arguments.Response;
using EAN.Net.Arguments.Response.Errors;

namespace EAN.Net.TestTool.Exceptions
{
	public class EanApiException: ApplicationException
	{
		public EanWsError EanError { get; private set; }

		private static string GetMessage(ResponseBase response)
		{
			if (response.ErrorException != null && !string.IsNullOrEmpty(response.ErrorException.Message))
				return response.ErrorException.Message;

			if (response.EanError != null && !string.IsNullOrEmpty(response.EanError.VerboseMessage))
				return response.EanError.VerboseMessage;

			return null;
		}

		public EanApiException(ResponseBase response)
			: base(GetMessage(response), response.ErrorException)
		{
			EanError = response.EanError;
		}
	}
}
