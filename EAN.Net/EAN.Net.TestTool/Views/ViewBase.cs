﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace EAN.Net.TestTool.Views
{
	public abstract class ViewBase: UserControl
	{
		public static readonly DependencyProperty HasNoErrorsProperty = DependencyProperty.Register("HasNoErrors", typeof(bool), typeof(ViewBase), new PropertyMetadata(true));

		private int _errorCount;

		protected ViewBase()
		{
			AddHandler(Validation.ErrorEvent, new RoutedEventHandler(OnErrorEvent));
		}

		public bool HasNoErrors
		{
			get { return (bool)GetValue(HasNoErrorsProperty); }
			set { SetValue(HasNoErrorsProperty, value); }
		}

		private void OnErrorEvent(object sender, RoutedEventArgs e)
		{
			var validationEventArgs = e as ValidationErrorEventArgs;
			if (validationEventArgs == null)
				return;

			switch (validationEventArgs.Action)
			{
				case ValidationErrorEventAction.Added:
					{
						_errorCount++;
						break;
					}
				case ValidationErrorEventAction.Removed:
					{
						_errorCount--;
						break;
					}
				default:
					{
						throw new Exception("Unknown action");
					}
			}

			HasNoErrors = _errorCount <= 0;
		}
	}
}
