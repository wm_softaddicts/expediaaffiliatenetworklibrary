﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EAN.Net.TestTool.Views
{
	/// <summary>
	/// Interaction logic for CancelReservationOptionsView.xaml
	/// </summary>
	public partial class CancelReservationOptionsView : UserControl
	{
		public static readonly DependencyProperty HasNoErrorsProperty = DependencyProperty.Register("HasNoErrors", typeof(bool), typeof(CancelReservationOptionsView), new PropertyMetadata(true));

		private int _errorCount;

		public CancelReservationOptionsView()
		{
			InitializeComponent();

			this.AddHandler(Validation.ErrorEvent, new RoutedEventHandler(OnErrorEvent));
		}

		public bool HasNoErrors
		{
			get { return (bool)GetValue(HasNoErrorsProperty); }
			set { SetValue(HasNoErrorsProperty, value); }
		}

		private void OnErrorEvent(object sender, RoutedEventArgs e)
		{
			var validationEventArgs = e as ValidationErrorEventArgs;
			if (validationEventArgs == null)
				return;

			switch (validationEventArgs.Action)
			{
				case ValidationErrorEventAction.Added:
					{
						_errorCount++;
						break;
					}
				case ValidationErrorEventAction.Removed:
					{
						_errorCount--;
						break;
					}
				default:
					{
						throw new Exception("Unknown action");
					}
			}

			HasNoErrors = _errorCount <= 0;
		}
	}
}
