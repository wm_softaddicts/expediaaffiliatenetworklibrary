﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EAN.Net.TestTool.ViewModels
{
	public abstract class HotelRoomOptionsViewModel<TViewModel> : ViewModelBase<TViewModel>
		where TViewModel : HotelRoomOptionsViewModel<TViewModel>
	{
		private readonly RootViewModel _rootModel;

		private long? _hotelId = 113690;
		private DateTime _arrivalDate = DateTime.Now.AddMonths(1);
		private DateTime _departureDate = DateTime.Now.AddMonths(1).AddDays(2);
		private SupplierTypeViewModel _supplierType;
		private string _roomTypeCode;
		private string _rateKey;
		private string _rateCode;
		private readonly RoomOptionsViewModel _roomOptions = new RoomOptionsViewModel();

		protected HotelRoomOptionsViewModel(RootViewModel rootModel)
		{
			_rootModel = rootModel;

			_supplierType = SupplierTypes.First();
		}

		public RootViewModel RootModel
		{
			get { return _rootModel; }
		}

		public virtual IList<SupplierTypeViewModel> SupplierTypes
		{
			get { return Catalog.SupplierTypes; }
		}

		public long? HotelId
		{
			get { return _hotelId; }
			set
			{
				_hotelId = value;
				NotifyPropertyChanged(x => x.HotelId);
			}
		}

		public DateTime ArrivalDate
		{
			get { return _arrivalDate; }
			set
			{
				_arrivalDate = value;
				NotifyPropertyChanged(x => x.ArrivalDate);
			}
		}

		public DateTime DepartureDate
		{
			get { return _departureDate; }
			set
			{
				_departureDate = value;
				NotifyPropertyChanged(x => DepartureDate);
			}
		}

		public SupplierTypeViewModel SupplierType
		{
			get { return _supplierType; }
			set
			{
				_supplierType = value;
				NotifyPropertyChanged(x => SupplierType);
			}
		}

		public string RoomTypeCode
		{
			get { return _roomTypeCode; }
			set
			{
				_roomTypeCode = value;
				NotifyPropertyChanged(x => RoomTypeCode);
			}
		}

		public string RateKey
		{
			get { return _rateKey; }
			set
			{
				_rateKey = value;
				NotifyPropertyChanged(x => RateKey);
			}
		}

		public string RateCode
		{
			get { return _rateCode; }
			set
			{
				_rateCode = value;
				NotifyPropertyChanged(x => RateCode);
			}
		}

		public RoomOptionsViewModel RoomOptions
		{
			get { return _roomOptions; }
		}
	}
}
