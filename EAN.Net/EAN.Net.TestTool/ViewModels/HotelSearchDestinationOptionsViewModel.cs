﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AutoMapper;
using EAN.Net.TestTool.Common;

namespace EAN.Net.TestTool.ViewModels
{
	public class HotelSearchDestinationOptionsViewModel : ViewModelBase<HotelSearchDestinationOptionsViewModel>
	{
		private bool _isSearchByAddress = true;
		private bool _isSearchByDestinationString;
		private bool _isSearchByDestinationId;
		private bool _isSearchByHotelIdList;
		private bool _isSearchByGeoLocation;

		private string _city;
		private StateViewModel _state;
		private CountryViewModel _country;
		private string _destinationString;
		private string _destinationId;
		private readonly RelayCommand _destinationCommand;

		private long? _hotelId1;
		private long? _hotelId2;
		private long? _hotelId3;
	
		private float _latitude;
		private float _longitude;
		private int? _searchRadius;
		private HotelSearchRadiusUnitViewModel _searchRadiusUnit;
		private string _filterPropertyName;
		private string _filterAddress;
		private string _filterPostalCode;
		private HotelSearchSortViewModel _sorting;
		private HotelSearchDestinationViewModel _destination;

		public HotelSearchDestinationOptionsViewModel()
		{
			SearchRadiusUnit = SearchRadiusUnits.First();
			Country = Countries.First();
			State = Country.States.FirstOrDefault();
			Destination = Destinations.First();
			Sorting = SortKinds.First();

			HotelId1 = 113690;

			_destinationCommand = new RelayCommand(_ =>
			{
				if (Destination == null)
					return;

				try
				{
					Mapper.DynamicMap(Destination, this);
				}
				catch (Exception exc)
				{
					Debug.WriteLine(exc);
				}
			});
		}

		public IList<HotelSearchSortViewModel> SortKinds
		{
			get { return Catalog.HotelSearchSortKinds; }
		}

		public IList<HotelSearchRadiusUnitViewModel> SearchRadiusUnits
		{
			get { return Catalog.SearchRadiusUnits; }
		}

		public IList<CountryViewModel> Countries
		{
			get { return Catalog.Countries; }
		}

		public IList<HotelSearchDestinationViewModel> Destinations
		{
			get { return Catalog.Destinations; }
		}

		public string City
		{
			get { return _city; }
			set
			{
				_city = value;
				NotifyPropertyChanged(x => City);
			}
		}

		public bool IsSearchByAddress
		{
			get { return _isSearchByAddress; }
			set
			{
				_isSearchByAddress = value;
				NotifyPropertyChanged(x => IsSearchByAddress);
				NotifyPropertyChanged(x => City);
			}
		}

		public bool IsSearchByDestinationString
		{
			get { return _isSearchByDestinationString; }
			set
			{
				_isSearchByDestinationString = value;
				NotifyPropertyChanged(x => IsSearchByDestinationString);
				NotifyPropertyChanged(x => DestinationString);
			}
		}

		public bool IsSearchByDestinationId
		{
			get { return _isSearchByDestinationId; }
			set
			{
				_isSearchByDestinationId = value;
				NotifyPropertyChanged(x => IsSearchByDestinationId);
				NotifyPropertyChanged(x => DestinationId);
			}
		}

		public bool IsSearchByHotelIdList
		{
			get { return _isSearchByHotelIdList; }
			set
			{
				_isSearchByHotelIdList = value;
				NotifyPropertyChanged(x => IsSearchByHotelIdList);
				NotifyPropertyChanged(x => HotelId1);
				NotifyPropertyChanged(x => HotelId2);
				NotifyPropertyChanged(x => HotelId3);
			}
		}

		public bool IsSearchByGeoLocation
		{
			get { return _isSearchByGeoLocation; }
			set
			{
				_isSearchByGeoLocation = value;
				NotifyPropertyChanged(x => IsSearchByGeoLocation);
				NotifyPropertyChanged(x => Latitude);
				NotifyPropertyChanged(x => Longitude);
			}
		}

		public StateViewModel State
		{
			get { return _state; }
			set
			{
				_state = value;
				NotifyPropertyChanged(x => State);
			}
		}

		public CountryViewModel Country
		{
			get { return _country; }
			set
			{
				_country = value;
				NotifyPropertyChanged(x => Country);
				NotifyPropertyChanged(x => Country.States);
			}
		}

		public HotelSearchDestinationViewModel Destination
		{
			get { return _destination; }
			set
			{
				_destination = value;
				NotifyPropertyChanged(x => x.Destination);
			}
		}

		public string DestinationString
		{
			get { return _destinationString; }
			set
			{
				_destinationString = value;
				NotifyPropertyChanged(x => DestinationString);
			}
		}

		public string DestinationId
		{
			get { return _destinationId; }
			set
			{
				_destinationId = value;
				NotifyPropertyChanged(x => DestinationId);
			}
		}

		public long? HotelId1
		{
			get { return _hotelId1; }
			set
			{
				_hotelId1 = value;
				NotifyPropertyChanged(x => x.HotelId1);
				NotifyPropertyChanged(x => x.HotelId2);
				NotifyPropertyChanged(x => x.HotelId3);
			}
		}

		public long? HotelId2
		{
			get { return _hotelId2; }
			set
			{
				_hotelId2 = value;
				NotifyPropertyChanged(x => x.HotelId1);
				NotifyPropertyChanged(x => x.HotelId2);
				NotifyPropertyChanged(x => x.HotelId3);
			}
		}

		public long? HotelId3
		{
			get { return _hotelId3; }
			set
			{
				_hotelId3 = value;
				NotifyPropertyChanged(x => x.HotelId1);
				NotifyPropertyChanged(x => x.HotelId2);
				NotifyPropertyChanged(x => x.HotelId3);
			}
		}

		public float Latitude
		{
			get { return _latitude; }
			set
			{
				_latitude = value;
				NotifyPropertyChanged(x => Latitude);
			}
		}

		public float Longitude
		{
			get { return _longitude; }
			set
			{
				_longitude = value;
				NotifyPropertyChanged(x => Longitude);
			}
		}

		public int? SearchRadius
		{
			get { return _searchRadius; }
			set
			{
				_searchRadius = value;
				NotifyPropertyChanged(x => SearchRadius);
			}
		}

		public HotelSearchRadiusUnitViewModel SearchRadiusUnit
		{
			get { return _searchRadiusUnit; }
			set
			{
				_searchRadiusUnit = value;
				NotifyPropertyChanged(x => SearchRadiusUnit);
			}
		}

		public HotelSearchSortViewModel Sorting
		{
			get { return _sorting; }
			set
			{
				_sorting = value;
				NotifyPropertyChanged(x => x.Sorting);
			}
		}

		public string FilterPropertyName
		{
			get { return _filterPropertyName; }
			set
			{
				_filterPropertyName = value;
				NotifyPropertyChanged(x => FilterPropertyName);
			}
		}

		public string FilterAddress
		{
			get { return _filterAddress; }
			set
			{
				_filterAddress = value;
				NotifyPropertyChanged(x => FilterAddress);
			}
		}

		public string FilterPostalCode
		{
			get { return _filterPostalCode; }
			set
			{
				_filterPostalCode = value;
				NotifyPropertyChanged(x => FilterPostalCode);
			}
		}

		public RelayCommand DestinationCommand
		{
			get { return _destinationCommand; }
		}

		protected override string ValidateProperty(string propertyName)
		{
			if (IsSearchByAddress && propertyName == Prop(x => x.City) && string.IsNullOrWhiteSpace(City))
				return "City is not filled";

			if (IsSearchByDestinationId && propertyName == Prop(x => x.DestinationId) && string.IsNullOrWhiteSpace(DestinationId))
				return "DestinationId is not filled";

			if (IsSearchByDestinationString && propertyName == Prop(x => x.DestinationString) && string.IsNullOrWhiteSpace(DestinationString))
				return "Destination String is not filled";

			if (IsSearchByHotelIdList && new[] { Prop(x => x.HotelId1), Prop(x => x.HotelId2), Prop(x => x.HotelId3) }.Contains(propertyName) 
					&& new[] { HotelId1, HotelId2, HotelId3 }.All(id => id == null))
				return "Hotel Id List is not filled";

			return base.ValidateProperty(propertyName);
		}
	}
}
