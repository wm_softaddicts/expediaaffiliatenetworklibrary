﻿using System.Collections.Generic;

namespace EAN.Net.TestTool.ViewModels
{
	public class HotelSearchCacheOptionsViewModel: ViewModelBase<HotelSearchCacheOptionsViewModel>
	{
		private string _suppliesChacheTolerance = "MIN";

		public IList<string> SuppliesChacheToleranceValues
		{
			get { return Catalog.SupplierCacheToleranceValues; }
		}

		public string SuppliesChacheTolerance
		{
			get { return _suppliesChacheTolerance; }
			set
			{
				_suppliesChacheTolerance = value;
				NotifyPropertyChanged(x => x.SuppliesChacheTolerance);
			}
		}
	}
}
