﻿using System;
using System.Windows.Input;
using EAN.Net.TestTool.Common;
using EAN.Net.TestTool.Exceptions;
using Newtonsoft.Json;

namespace EAN.Net.TestTool.ViewModels
{
	public class PingOptionsViewModel : ViewModelBase<PingOptionsViewModel>
	{
		private readonly RootViewModel _rootModel;

		private string _results = string.Empty;

		private string _echoValue = string.Empty;

		private int _selectedTabIndex;
		private readonly RelayCommand _searchCommand;
		private readonly RelayCommand _backCommand;

		public PingOptionsViewModel(RootViewModel rootModel)
		{
			_rootModel = rootModel;

			_searchCommand = new RelayCommand(_ => Ping());
			_backCommand = new RelayCommand(_ => SelectedTabIndex = 0);
		}

		public RootViewModel RootModel
		{
			get { return _rootModel; }
		}

		public string EchoValue
		{
			get { return _echoValue; }
			set
			{
				_echoValue = value;
				NotifyPropertyChanged(x => x.EchoValue);
			}
		}

		public RelayCommand SearchCommand
		{
			get { return _searchCommand; }
		}

		public RelayCommand BackCommand
		{
			get { return _backCommand; }
		}

		public int SelectedTabIndex
		{
			get { return _selectedTabIndex; }
			set
			{
				_selectedTabIndex = value;
				NotifyPropertyChanged(x => x.SelectedTabIndex);
			}
		}

		public string Results
		{
			get { return _results; }
			set
			{
				_results = value;
				NotifyPropertyChanged(x => x.Results);
			}
		}

		private void Ping()
		{
			try
			{
				Mouse.OverrideCursor = Cursors.Wait;

				var client = RootModel.CreateClient();
				var session = RootModel.CreateSession(client);


				Results = string.Empty;

				var result = session.Ping(EchoValue);

				if (result.IsError)
					throw new EanApiException(result);

				Results = JsonConvert.SerializeObject(result, Formatting.Indented);

				SelectedTabIndex = 1;
			}
			catch (Exception exc)
			{
				RootModel.ShowErrorMessage(exc);
			}
			finally
			{
				Mouse.OverrideCursor = null;
			}
		}
	}
}
