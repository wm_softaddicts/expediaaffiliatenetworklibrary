﻿using System;

namespace EAN.Net.TestTool.ViewModels
{
	public class MinMaxViewModel<T> : ViewModelBase<MinMaxViewModel<T>>
		where T : struct, IComparable
	{
		private T _minValue;
		private T _maxValue;

		public T MinValue
		{
			get { return _minValue; }
			set
			{
				_minValue = value;

				if (MaxValue.CompareTo(_minValue) < 0)
				{
					MaxValue = _minValue;
				}

				NotifyPropertyChanged(x => x.MinValue);
			}
		}

		public T MaxValue
		{
			get { return _maxValue; }
			set
			{
				_maxValue = value;

				if (MinValue.CompareTo(_maxValue) > 0)
				{
					MinValue = _maxValue;
				}

				NotifyPropertyChanged(x => x.MaxValue);
			}
		}
	}
}
