﻿using System.Collections.Generic;
using System.Linq;
using Monads.NET;

namespace EAN.Net.TestTool.ViewModels
{
	public class HotelRoomReservationAddressOptionsViewModel : ViewModelBase<HotelRoomReservationAddressOptionsViewModel>
	{
		private string _city;

		private StateViewModel _state;

		private CountryViewModel _country;
		private string _postalCode;
		private string _address1;
		private string _address2;
		private string _address3;

		public HotelRoomReservationAddressOptionsViewModel()
		{
			Country = Countries.FirstOrDefault(c => c.Code == "US");
			State = Country.With(c => c.States.FirstOrDefault(s => s.Code == "WA"));
			PostalCode = "98004";
			City = "Bellevue";
			Address1 = "travelnow";
		}

		public IList<CountryViewModel> Countries
		{
			get { return Catalog.Countries; }
		}

		public CountryViewModel Country
		{
			get { return _country; }
			set
			{
				_country = value;
				NotifyPropertyChanged(x => Country);
				NotifyPropertyChanged(x => Country.States);
			}
		}

		public StateViewModel State
		{
			get { return _state; }
			set
			{
				_state = value;
				NotifyPropertyChanged(x => State);
			}
		}

		public string PostalCode
		{
			get { return _postalCode; }
			set
			{
				_postalCode = value;
				NotifyPropertyChanged(x => PostalCode);
			}
		}

		public string City
		{
			get { return _city; }
			set
			{
				_city = value;
				NotifyPropertyChanged(x => City);
			}
		}

		public string Address1
		{
			get { return _address1; }
			set
			{
				_address1 = value;
				NotifyPropertyChanged(x => Address1);
			}
		}

		public string Address2
		{
			get { return _address2; }
			set
			{
				_address2 = value;
				NotifyPropertyChanged(x => Address2);
			}
		}

		public string Address3
		{
			get { return _address3; }
			set
			{
				_address3 = value;
				NotifyPropertyChanged(x => Address3);
			}
		}

		protected override string ValidateProperty(string propertyName)
		{
			if (propertyName == Prop(x => x.City) && string.IsNullOrEmpty(City))
				return "City is not filled";

			if (propertyName == Prop(x => x.Address1) && string.IsNullOrEmpty(Address1))
				return "Address1 is not filled";

			return base.ValidateProperty(propertyName);
		}
	}
}
