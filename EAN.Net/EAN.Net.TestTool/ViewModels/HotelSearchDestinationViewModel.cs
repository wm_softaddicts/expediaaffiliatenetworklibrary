﻿using System.Linq;
using Monads.NET;

namespace EAN.Net.TestTool.ViewModels
{
	public class HotelSearchDestinationViewModel : ViewModelBase<HotelSearchDestinationViewModel>
	{
		private readonly CountryViewModel _country;
		private readonly StateViewModel _state;
		private readonly string _city;

		public HotelSearchDestinationViewModel(string city, string stateProvinceCode, string countryCode)
		{
			_city = city;
			_country = Catalog.Countries.FirstOrDefault(c => c.Code == countryCode);
			_state = _country.With(c => c.States.FirstOrDefault(s => s.Code == stateProvinceCode));
		}

		public HotelSearchDestinationViewModel(string city, string countryCode)
			: this(city, null, countryCode) { }

		public CountryViewModel Country
		{
			get { return _country; }
		}

		public StateViewModel State
		{
			get { return _state; }
		}

		public string City
		{
			get { return _city; }
		}

		public override string ToString()
		{
			return string.Join(", ",
				new[] { City, State.With(s => s.Code), Country.With(c => c.Code) }
					.Where(s => !string.IsNullOrEmpty(s)));
		}
	}
}
