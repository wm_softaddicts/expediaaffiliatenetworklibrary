﻿using System;
using System.Windows;
using EAN.Net.TestTool.Exceptions;

namespace EAN.Net.TestTool.ViewModels
{
	public class RootViewModel
	{
		private readonly CommonOptionsViewModel _commonOptions;
		private readonly HotelSearchOptionsViewModel _hotelSearchOptions;
		private readonly HotelRoomAvailabilityOptionsViewModel _roomAvailabilityOptions;
		private readonly ItineraryInformationOptionsViewModel _itineraryInformationOptions;
		private readonly GeoLocationOptionsViewModel _geoLocationOptions;
		private readonly CancelReservationOptionsViewModel _cancelReservationOptions;
		private readonly HotelRoomReservationOptionsViewModel _roomReservationOptions;
		private readonly HotelRoomImagesOptionsViewModel _roomImagesOptions;
		private readonly PaymentTypesOptionsViewModel _paymentTypesOptions;
		private readonly AlternatePropertiesOptionsViewModel _alternatePropertiesOptions;
		private readonly HotelInformationOptionsViewModel _hotelInformationOptions;
		private readonly PingOptionsViewModel _pingOptions;

		public RootViewModel()
		{
			_commonOptions = new CommonOptionsViewModel(this);
			_hotelSearchOptions = new HotelSearchOptionsViewModel(this);
			_roomAvailabilityOptions = new HotelRoomAvailabilityOptionsViewModel(this);
			_itineraryInformationOptions = new ItineraryInformationOptionsViewModel(this);
			_geoLocationOptions = new GeoLocationOptionsViewModel(this);
			_cancelReservationOptions = new CancelReservationOptionsViewModel(this);
			_roomReservationOptions = new HotelRoomReservationOptionsViewModel(this);
			_roomImagesOptions = new HotelRoomImagesOptionsViewModel(this);
			_paymentTypesOptions = new PaymentTypesOptionsViewModel(this);
			_alternatePropertiesOptions = new AlternatePropertiesOptionsViewModel(this);
			_hotelInformationOptions = new HotelInformationOptionsViewModel(this);
			_pingOptions = new PingOptionsViewModel(this);
		}

		public CommonOptionsViewModel CommonOptions
		{
			get { return _commonOptions; }
		}

		public HotelSearchOptionsViewModel HotelSearchOptions
		{
			get { return _hotelSearchOptions; }
		}

		public HotelRoomAvailabilityOptionsViewModel RoomAvailabilityOptions
		{
			get { return _roomAvailabilityOptions; }
		}

		public ItineraryInformationOptionsViewModel ItineraryInformationOptions
		{
			get { return _itineraryInformationOptions; }
		}

		public GeoLocationOptionsViewModel GeoLocationOptions
		{
			get { return _geoLocationOptions; }
		}

		public CancelReservationOptionsViewModel CancelReservationOptions
		{
			get { return _cancelReservationOptions; }
		}

		public HotelRoomReservationOptionsViewModel RoomReservationOptions
		{
			get { return _roomReservationOptions; }
		}

		public HotelRoomImagesOptionsViewModel RoomImagesOptions
		{
			get { return _roomImagesOptions; }
		}

		public PaymentTypesOptionsViewModel PaymentTypesOptions
		{
			get { return _paymentTypesOptions; }
		}

		public AlternatePropertiesOptionsViewModel AlternatePropertiesOptions
		{
			get { return _alternatePropertiesOptions; }
		}

		public HotelInformationOptionsViewModel HotelInformationOptions
		{
			get { return _hotelInformationOptions; }
		}

		public PingOptionsViewModel PingOptions
		{
			get { return _pingOptions; }
		}

		public EanWebClient CreateClient()
		{
			return new EanWebClient(CommonOptions.CId, CommonOptions.ApiKey, CommonOptions.SharedSecret)
			{
				IsDevelopmentMode = true
			};
		}

		public EanCustomerSession CreateSession(EanWebClient client)
		{
			return new EanCustomerSession(client, "127.0.0.1", "Mozilla/4.0", CommonOptions.Currency.Code);
		}

		public void ShowErrorMessage(Exception exc)
		{
			var isEanError = exc is EanApiException;

			MessageBox.Show(
				Application.Current.MainWindow,
				exc.Message, isEanError ? "EAN API returned error" : "Error exception occured",
				MessageBoxButton.OK,
				isEanError ? MessageBoxImage.Exclamation : MessageBoxImage.Error);
		}
	}
}
