﻿namespace EAN.Net.TestTool.ViewModels
{
	public class HotelRoomAvailabilityDetailsOptionsViewModel : ViewModelBase<HotelRoomAvailabilityDetailsOptionsViewModel>
	{
		private bool _isIncludeHotelDetails;
		private bool _isIncludeRoomTypes;
		private bool _isIncludeRoomAmenities;
		private bool _isIncludePropertyAmenities;
		private bool _isIncludeHotelImages;

		public bool IsIncludeHotelDetails
		{
			get { return _isIncludeHotelDetails; }
			set
			{
				_isIncludeHotelDetails = value;
				NotifyPropertyChanged(x => IsIncludeHotelDetails);
			}
		}

		public bool IsIncludeRoomTypes
		{
			get { return _isIncludeRoomTypes; }
			set
			{
				_isIncludeRoomTypes = value;
				NotifyPropertyChanged(x => IsIncludeRoomTypes);
			}
		}

		public bool IsIncludeRoomAmenities
		{
			get { return _isIncludeRoomAmenities; }
			set
			{
				_isIncludeRoomAmenities = value;
				NotifyPropertyChanged(x => IsIncludeRoomAmenities);
			}
		}

		public bool IsIncludePropertyAmenities
		{
			get { return _isIncludePropertyAmenities; }
			set
			{
				_isIncludePropertyAmenities = value;
				NotifyPropertyChanged(x => IsIncludePropertyAmenities);
			}
		}

		public bool IsIncludeHotelImages
		{
			get { return _isIncludeHotelImages; }
			set
			{
				_isIncludeHotelImages = value;
				NotifyPropertyChanged(x => IsIncludeHotelImages);
			}
		}
	}
}
