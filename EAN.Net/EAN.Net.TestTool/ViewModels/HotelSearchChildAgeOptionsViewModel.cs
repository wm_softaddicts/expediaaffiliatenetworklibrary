﻿using System.Collections.ObjectModel;

namespace EAN.Net.TestTool.ViewModels
{
	public class HotelSearchChildAgeOptionsViewModel : ViewModelCollection<HotelSearchChildAgeOptionsViewModel, HotelSearchChildAgeViewModel>
	{
		public ObservableCollection<HotelSearchChildAgeViewModel> ChildAges
		{
			get { return Items; }
		}

		public int NumberOfChildren
		{
			get { return ItemCount; }
			set
			{
				ItemCount = value;
				NotifyPropertyChanged(x => x.NumberOfChildren);
			}
		}
	}
}
