﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EAN.Net.TestTool.ViewModels
{
	public class HotelSearchFilteringOptionsViewModel : ViewModelBase<HotelSearchFilteringOptionsViewModel>
	{
		private DateTime _arrivalDate = DateTime.Now;
		private DateTime _departureDate = DateTime.Now.AddDays(2);
		private readonly MinMaxViewModel<float> _rate = new MinMaxViewModel<float> { MinValue = 30, MaxValue = 3000};
		private readonly MinMaxViewModel<float> _starRating = new MinMaxViewModel<float> { MinValue = 1, MaxValue = 5};
		private HotelSearchPropertyCategoryViewModel _propertyCategory;
		private int _numberOfBedrooms = 1;
		private SupplierTypeViewModel _supplierType;
		private int? _maxRatePlanCount = 1;
		private int _numberOfResults = 20;
		private HotelListDetailOptionsViewModel _detailOptions;

		public HotelSearchFilteringOptionsViewModel()
		{
			PropertyCategory = PropertyCategories.First();
			SupplierType = SupplierTypes.First();
			DetailOptions = HotelListDetailOptionList.First();
		}

		public IList<HotelSearchPropertyCategoryViewModel> PropertyCategories
		{
			get { return Catalog.PropertyCategories; }
		}

		public IList<SupplierTypeViewModel> SupplierTypes
		{
			get { return Catalog.SupplierTypes; }
		}

		public IList<HotelListDetailOptionsViewModel> HotelListDetailOptionList
		{
			get { return Catalog.HotelListDetailOptionList; }
		}

		public DateTime ArrivalDate
		{
			get { return _arrivalDate; }
			set
			{
				_arrivalDate = value;
				NotifyPropertyChanged(x => x.ArrivalDate);
			}
		}

		public DateTime DepartureDate
		{
			get { return _departureDate; }
			set
			{
				_departureDate = value;
				NotifyPropertyChanged(x => DepartureDate);
			}
		}

		public MinMaxViewModel<float> Rate
		{
			get { return _rate; }
		}

		public MinMaxViewModel<float> StarRating
		{
			get { return _starRating; }
		}

		public HotelSearchPropertyCategoryViewModel PropertyCategory
		{
			get { return _propertyCategory; }
			set
			{
				_propertyCategory = value;
				NotifyPropertyChanged(x => x.PropertyCategory);
			}
		}

		public int NumberOfBedrooms
		{
			get { return _numberOfBedrooms; }
			set
			{
				_numberOfBedrooms = value;
				NotifyPropertyChanged(x => x.NumberOfBedrooms);
			}
		}

		public SupplierTypeViewModel SupplierType
		{
			get { return _supplierType; }
			set
			{
				_supplierType = value;
				NotifyPropertyChanged(x => x.SupplierType);
			}
		}

		public int? MaxRatePlanCount
		{
			get { return _maxRatePlanCount; }
			set
			{
				_maxRatePlanCount = value;
				NotifyPropertyChanged(x => x.MaxRatePlanCount);
			}
		}

		public int NumberOfResults
		{
			get { return _numberOfResults; }
			set
			{
				_numberOfResults = value;
				NotifyPropertyChanged(x => x.NumberOfResults);
			}
		}

		public HotelListDetailOptionsViewModel DetailOptions
		{
			get { return _detailOptions; }
			set
			{
				_detailOptions = value;
				NotifyPropertyChanged(x => x.DetailOptions);
			}
		}
	}
}
