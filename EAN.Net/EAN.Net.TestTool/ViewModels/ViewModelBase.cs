﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;

namespace EAN.Net.TestTool.ViewModels
{
	public abstract class ViewModelBase<TViewModel> : INotifyPropertyChanged, IDataErrorInfo
		where TViewModel: ViewModelBase<TViewModel>
	{
		private static PropertyChangedEventArgs CreatePropertyChangedEventArgs(Expression<Func<TViewModel, object>> propertyExpression)
		{
			var lambda = propertyExpression as LambdaExpression;
			MemberExpression memberExpression;
			if (lambda.Body is UnaryExpression)
			{
				var unaryExpression = lambda.Body as UnaryExpression;
				memberExpression = unaryExpression.Operand as MemberExpression;
			}
			else
			{
				memberExpression = lambda.Body as MemberExpression;
			}

			if (memberExpression == null)
				return null;

			var propertyInfo = memberExpression.Member as PropertyInfo;

			if (propertyInfo == null)
				return null;

			return new PropertyChangedEventArgs(propertyInfo.Name);
		}

		protected void NotifyPropertyChanged(Expression<Func<TViewModel, object>> propertyExpression)
		{
			if (PropertyChanged == null) 
				return;

			var args = CreatePropertyChangedEventArgs(propertyExpression);
			if (args == null)
				return;

			PropertyChanged(this, args);
		}

		protected static string Prop<TReturn>(Expression<Func<TViewModel, TReturn>> expression)
		{
			var body = (MemberExpression)expression.Body;
			return body.Member.Name;
		}

		protected virtual string ValidateProperty(string propertyName)
		{
			return null;
		}

		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion

		#region IDataErrorInfo Members

		public string Error
		{
			get { throw new NotImplementedException(); }
		}

		public string this[string columnName]
		{
			get { return ValidateProperty(columnName); }
		}

		#endregion
	}
}
