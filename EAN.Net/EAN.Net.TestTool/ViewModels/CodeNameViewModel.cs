﻿namespace EAN.Net.TestTool.ViewModels
{
	public abstract class CodeNameViewModel<TCode> : ViewModelBase<CodeNameViewModel<TCode>>
	{
		private TCode _code;
		private string _name;

		public TCode Code
		{
			get { return _code; }
			set
			{
				_code = value;
				NotifyPropertyChanged(x => x.Code);
			}
		}

		public string Name
		{
			get { return _name; }
			set
			{
				_name = value;
				NotifyPropertyChanged(x => x.Name);
			}
		}

		public override string ToString()
		{
			return Name;
		}
	}
}
