﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EAN.Net.TestTool.ViewModels
{
	public class HotelSearchChildAgeViewModel : ViewModelBase<HotelSearchChildAgeViewModel>
	{
		private int _age = 1;

		public int Age
		{
			get { return _age; }
			set
			{
				_age = value;
				NotifyPropertyChanged(x => Age);
			}
		}
	}
}
