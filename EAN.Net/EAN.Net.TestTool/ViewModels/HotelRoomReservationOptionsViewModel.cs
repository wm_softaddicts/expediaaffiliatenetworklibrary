﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using EAN.Net.Arguments;
using EAN.Net.Arguments.Common;
using EAN.Net.TestTool.Common;
using EAN.Net.TestTool.Exceptions;
using Monads.NET;
using Newtonsoft.Json;

namespace EAN.Net.TestTool.ViewModels
{
	public class HotelRoomReservationOptionsViewModel : HotelRoomOptionsViewModel<HotelRoomReservationOptionsViewModel>
	{
		private string _affiliateConfirmationId;
		private string _affiliateCustomerId;
		private long? _itineraryId;
		private float? _chargeableRate;
		private string _specialInformation;
		private bool _isSendReservationEmail;

		private BedType _bedType;

		private readonly HotelRoomReservationAddressOptionsViewModel _addressOptions = new HotelRoomReservationAddressOptionsViewModel();
		private readonly HotelRoomReservationPaymentOptionsViewModel _paymentOptions = new HotelRoomReservationPaymentOptionsViewModel();

		private string _results = string.Empty;

		private int _selectedTabIndex;
		private readonly RelayCommand _fillCommand;
		private readonly RelayCommand _searchCommand;
		private readonly RelayCommand _backCommand;

		public HotelRoomReservationOptionsViewModel(RootViewModel rootModel)
			: base(rootModel)
		{
			_fillCommand = new RelayCommand(_ => FillWithTestData());
			_searchCommand = new RelayCommand(_ => MakeReservation());
			_backCommand = new RelayCommand(_ => SelectedTabIndex = 0);
		}

		public override IList<SupplierTypeViewModel> SupplierTypes
		{
			get { return Catalog.SupplierTypes.Where(x => x.Code != null).ToArray(); }
		}

		public string AffiliateConfirmationId
		{
			get { return _affiliateConfirmationId; }
			set
			{
				_affiliateConfirmationId = value;
				NotifyPropertyChanged(x => x.AffiliateConfirmationId);
			}
		}

		public string AffiliateCustomerId
		{
			get { return _affiliateCustomerId; }
			set
			{
				_affiliateCustomerId = value;
				NotifyPropertyChanged(x => x.AffiliateCustomerId);
			}
		}

		public long? ItineraryId
		{
			get { return _itineraryId; }
			set
			{
				_itineraryId = value;
				NotifyPropertyChanged(x => x.ItineraryId);
			}
		}

		public float? ChargeableRate
		{
			get { return _chargeableRate; }
			set
			{
				_chargeableRate = value;
				NotifyPropertyChanged(x => x.ChargeableRate);
			}
		}

		public string SpecialInformation
		{
			get { return _specialInformation; }
			set
			{
				_specialInformation = value;
				NotifyPropertyChanged(x => x.SpecialInformation);
			}
		}

		public bool IsSendReservationEmail
		{
			get { return _isSendReservationEmail; }
			set
			{
				_isSendReservationEmail = value;
				NotifyPropertyChanged(x => x.IsSendReservationEmail);
			}
		}

		public HotelRoomReservationAddressOptionsViewModel AddressOptions
		{
			get { return _addressOptions; }
		}

		public HotelRoomReservationPaymentOptionsViewModel PaymentOptions
		{
			get { return _paymentOptions; }
		}

		public RelayCommand FillCommand
		{
			get { return _fillCommand; }
		}

		public RelayCommand SearchCommand
		{
			get { return _searchCommand; }
		}

		public RelayCommand BackCommand
		{
			get { return _backCommand; }
		}

		public int SelectedTabIndex
		{
			get { return _selectedTabIndex; }
			set
			{
				_selectedTabIndex = value;
				NotifyPropertyChanged(x => x.SelectedTabIndex);
			}
		}

		public string Results
		{
			get { return _results; }
			set
			{
				_results = value;
				NotifyPropertyChanged(x => x.Results);
			}
		}

		private void FillWithTestData()
		{
			try
			{
				Mouse.OverrideCursor = Cursors.Wait;

				var client = RootModel.CreateClient();
				var session = RootModel.CreateSession(client);

				var rooms = new List<Room>();

				foreach (var r in RoomOptions.Rooms)
				{
					var room = new Room
					{
						NumberOfAdults = r.NumberOfAdults,
					};
					foreach (var a in r.ChildAgeOptions.ChildAges)
					{
						room.ChildAges.Add(a.Age);
					}

					rooms.Add(room);
				}

				NotifyPropertyChanged(x => x.PaymentOptions);

				var hotelSearchOptions = new HotelListSearchByAddressOptions
				{
					CountryCode = AddressOptions.Country.With(c => c.Code),
					StateProvinceCode = AddressOptions.State.With(s => s.Code),
					City = AddressOptions.City,
					PostalCode = AddressOptions.PostalCode
				};

				var hotelListResponse = session.GetHotelList(ArrivalDate, DepartureDate, rooms, hotelSearchOptions, isIncludeDetails: true);
				if (hotelListResponse.IsError)
					return;

				var hotel = hotelListResponse.HotelList.HotelSummaryList.First();
				var roomRateDetails = hotel.RoomRateDetailsList.RoomRateDetails;
				var rateInfo = roomRateDetails.RateInfos.RateInfoList.First();
				var hotelRoom = rateInfo.RoomGroup.RoomList.First();

				_bedType = roomRateDetails.BedTypes.BedTypeList.FirstOrDefault();

				AffiliateConfirmationId = Guid.NewGuid().ToString();
				RoomTypeCode = roomRateDetails.RoomTypeCode;
				RateCode = roomRateDetails.RateCode;
				RateKey = roomRateDetails.RateKey ?? hotelRoom.RateKey;

				ChargeableRate = rateInfo.ChargeableRateInfo.Total ?? 0.0f;
			}
			catch (Exception exc)
			{
				RootModel.ShowErrorMessage(exc);
			}
			finally
			{
				Mouse.OverrideCursor = null;
			}
		}

		private void MakeReservation()
		{
			try
			{
				Mouse.OverrideCursor = Cursors.Wait;

				var client = RootModel.CreateClient();
				var session = RootModel.CreateSession(client);

				var rooms = new List<RoomReservation>();

				foreach (var r in RoomOptions.Rooms)
				{
					var room = new RoomReservation
					{
						NumberOfAdults = r.NumberOfAdults,
						FirstName = PaymentOptions.FirstName,
						LastName = PaymentOptions.LastName,
						SmokingPreference = SmokingPreference.NonSmoking,
						BedTypeId = _bedType.With(t => t.Id)
					};
					foreach (var a in r.ChildAgeOptions.ChildAges)
					{
						room.ChildAges.Add(a.Age);
					}

					rooms.Add(room);
				}

				var reservationOptions = new ReservationOptions
				{
					Email = PaymentOptions.Email,
					FirstName = PaymentOptions.FirstName,
					LastName = PaymentOptions.LastName,
					HomePhone = PaymentOptions.HomePhone,
					WorkPhone = PaymentOptions.WorkPhone,
					PhoneExtension = PaymentOptions.PhoneExtension,
					FaxPhone = PaymentOptions.FaxPhone,
					CompanyName = PaymentOptions.CompanyName,
					CreditCardType = PaymentOptions.CreditCardType,
					CreditCardNumber = PaymentOptions.CreditCardNumber,
					CreditCardIdentifier = PaymentOptions.CreditCardIdentifier,
					CreditCardExpirationMonth = PaymentOptions.CreditCardExpirationMonth,
					CreditCardExpirationYear = PaymentOptions.CreditCardExpirationYear,
				};

				var addressOptions = new AddressOptions
				{
					CountryCode = AddressOptions.Country.With(c => c.Code),
					StateProvinceCode = AddressOptions.State.With(s => s.Code),
					City = AddressOptions.City,
					PostalCode = AddressOptions.PostalCode,
					Address1 = AddressOptions.Address1,
					Address2 = AddressOptions.Address2,
					Address3 = AddressOptions.Address3,
				};

				

				Results = string.Empty;

				var result = session.MakeReservation(HotelId ?? 0, ArrivalDate, DepartureDate, RateKey, RoomTypeCode, RateCode, rooms,
					reservationOptions, addressOptions, ChargeableRate ?? 0, SupplierType.Code, null, AffiliateConfirmationId,
					AffiliateCustomerId, ItineraryId, SpecialInformation, IsSendReservationEmail);

				if (result.IsError)
					throw new EanApiException(result);

				Results = JsonConvert.SerializeObject(result, Formatting.Indented);

				SelectedTabIndex = 1;
			}
			catch (Exception exc)
			{
				RootModel.ShowErrorMessage(exc);
			}
			finally
			{
				Mouse.OverrideCursor = null;
			}
		}

		protected override string ValidateProperty(string propertyName)
		{
			if (propertyName == Prop(x => x.HotelId) && HotelId == null)
				return "Hotel Id is not filled";

			if (propertyName == Prop(x => x.RateKey) && string.IsNullOrEmpty(RateKey))
				return "Rate key is not filled";

			if (propertyName == Prop(x => x.RoomTypeCode) && string.IsNullOrEmpty(RoomTypeCode))
				return "Room type code is not filled";

			if (propertyName == Prop(x => x.RateCode) && string.IsNullOrEmpty(RateCode))
				return "Rate code is not filled";

			if (propertyName == Prop(x => x.AffiliateConfirmationId) && string.IsNullOrEmpty(AffiliateConfirmationId))
				return "Affiliate confirmation Id is not filled";

			if (propertyName == Prop(x => x.ChargeableRate) && ChargeableRate == null)
				return "Chargeable rate is not filled";

			return base.ValidateProperty(propertyName);
		}
	}
}
