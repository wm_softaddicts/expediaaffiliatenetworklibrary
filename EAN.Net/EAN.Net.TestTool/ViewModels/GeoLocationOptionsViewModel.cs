﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using AutoMapper;
using EAN.Net.Arguments;
using EAN.Net.TestTool.Common;
using EAN.Net.TestTool.Exceptions;
using Monads.NET;
using Newtonsoft.Json;

namespace EAN.Net.TestTool.ViewModels
{
	public class GeoLocationOptionsViewModel : ViewModelBase<GeoLocationOptionsViewModel>
	{
		private readonly RootViewModel _rootModel;

		private bool _isSearchByDestinationId;
		private bool _isSearchByDestinationString;
		private bool _isSearchByAddress = true;
		private bool _isSearchByAddressString;

		private string _city;
		private StateViewModel _state;
		private CountryViewModel _country;
		private string _destinationString;
		private DestinationTypeViewModel _destinationType;
		private string _destinationId;
		private HotelSearchDestinationViewModel _destination;
		private readonly RelayCommand _destinationCommand;
		private string _addressString;
		private string _addressPostalCode;
		private bool _isIgnoreSearchWeight;
		private bool _isUseGeoCoder;
		private string _results;

		private int _selectedTabIndex;
		private readonly RelayCommand _searchCommand;
		private readonly RelayCommand _backCommand;

		public GeoLocationOptionsViewModel(RootViewModel rootModel)
		{
			_rootModel = rootModel;

			_searchCommand = new RelayCommand(_ => SearchGeoLocations());
			_backCommand = new RelayCommand(_ => SelectedTabIndex = 0);

			Country = Countries.First();
			State = Country.States.FirstOrDefault();
			Destination = Destinations.First();
			DestinationType = DestinationTypes.First();

			_destinationCommand = new RelayCommand(_ =>
			{
				if (Destination == null)
					return;

				try
				{
					Mapper.DynamicMap(Destination, this);
				}
				catch (Exception exc)
				{
					Debug.WriteLine(exc);
				}
			});
		}

		public RootViewModel RootModel
		{
			get { return _rootModel; }
		}

		public IList<CountryViewModel> Countries
		{
			get { return Catalog.Countries; }
		}

		public IList<HotelSearchDestinationViewModel> Destinations
		{
			get { return Catalog.Destinations; }
		}

		public IList<DestinationTypeViewModel> DestinationTypes
		{
			get { return Catalog.DestinationTypes; }
		}

		public bool IsSearchByDestinationId
		{
			get { return _isSearchByDestinationId; }
			set
			{
				_isSearchByDestinationId = value;
				NotifyPropertyChanged(x => IsSearchByDestinationId);
				NotifyPropertyChanged(x => DestinationId);
			}
		}

		public bool IsSearchByDestinationString
		{
			get { return _isSearchByDestinationString; }
			set
			{
				_isSearchByDestinationString = value;
				NotifyPropertyChanged(x => IsSearchByDestinationString);
				NotifyPropertyChanged(x => DestinationString);
			}
		}

		public bool IsSearchByAddress
		{
			get { return _isSearchByAddress; }
			set
			{
				_isSearchByAddress = value;
				NotifyPropertyChanged(x => IsSearchByAddress);
				NotifyPropertyChanged(x => City);
			}
		}

		public bool IsSearchByAddressString
		{
			get { return _isSearchByAddressString; }
			set
			{
				_isSearchByAddressString = value;
				NotifyPropertyChanged(x => IsSearchByAddressString);
				NotifyPropertyChanged(x => AddressString);
			}
		}

		public string DestinationId
		{
			get { return _destinationId; }
			set
			{
				_destinationId = value;
				NotifyPropertyChanged(x => DestinationId);
			}
		}

		public string DestinationString
		{
			get { return _destinationString; }
			set
			{
				_destinationString = value;
				NotifyPropertyChanged(x => DestinationString);
			}
		}

		public DestinationTypeViewModel DestinationType
		{
			get { return _destinationType; }
			set
			{
				_destinationType = value;
				NotifyPropertyChanged(x => DestinationType);
			}
		}

		public string City
		{
			get { return _city; }
			set
			{
				_city = value;
				NotifyPropertyChanged(x => City);
			}
		}

		public StateViewModel State
		{
			get { return _state; }
			set
			{
				_state = value;
				NotifyPropertyChanged(x => State);
			}
		}

		public CountryViewModel Country
		{
			get { return _country; }
			set
			{
				_country = value;
				NotifyPropertyChanged(x => Country);
				NotifyPropertyChanged(x => Country.States);
			}
		}

		public HotelSearchDestinationViewModel Destination
		{
			get { return _destination; }
			set
			{
				_destination = value;
				NotifyPropertyChanged(x => x.Destination);
			}
		}

		public string AddressString
		{
			get { return _addressString; }
			set
			{
				_addressString = value;
				NotifyPropertyChanged(x => x.AddressString);
			}
		}

		public string AddressPostalCode
		{
			get { return _addressPostalCode; }
			set
			{
				_addressPostalCode = value;
				NotifyPropertyChanged(x => x.AddressPostalCode);
			}
		}

		public bool IsIgnoreSearchWeight
		{
			get { return _isIgnoreSearchWeight; }
			set
			{
				_isIgnoreSearchWeight = value;
				NotifyPropertyChanged(x => x.IsIgnoreSearchWeight);
			}
		}

		public bool IsUseGeoCoder
		{
			get { return _isUseGeoCoder; }
			set
			{
				_isUseGeoCoder = value;
				NotifyPropertyChanged(x => x.IsUseGeoCoder);
			}
		}

		public RelayCommand DestinationCommand
		{
			get { return _destinationCommand; }
		}

		public RelayCommand SearchCommand
		{
			get { return _searchCommand; }
		}

		public RelayCommand BackCommand
		{
			get { return _backCommand; }
		}

		public int SelectedTabIndex
		{
			get { return _selectedTabIndex; }
			set
			{
				_selectedTabIndex = value;
				NotifyPropertyChanged(x => x.SelectedTabIndex);
			}
		}

		public string Results
		{
			get { return _results; }
			set
			{
				_results = value;
				NotifyPropertyChanged(x => x.Results);
			}
		}

		private ILocationInfoOptions GetLocationInfoOptions()
		{
			if (IsSearchByDestinationId)
			{
				return new LocationInfoByDestinationIdOptions
				{
					DestinationId = DestinationId
				};
			}

			if (IsSearchByDestinationString)
			{
				return new LocationInfoByDestinationStringOptions
				{
					DestinationString = DestinationString,
					LocationType = DestinationType.Code
				};
			}

			if (IsSearchByAddress)
			{
				return new LocationInfoByAddressOptions
				{
					CountryCode = Country.With(c => c.Code),
					StateProvinceCode = State.With(s => s.Code),
					City = City
				};
			}

			if (IsSearchByAddressString)
			{
				return new LocationInfoByDestinationStringOptions
				{
					DestinationString = AddressString,
					LocationType = LocationType.CityLocation,
					PostalCode = AddressPostalCode
				};
			}

			return null;
		}

		private void SearchGeoLocations()
		{
			try
			{
				Mouse.OverrideCursor = Cursors.Wait;

				var client = RootModel.CreateClient();
				var session = RootModel.CreateSession(client);

				var locationInfoOptions = GetLocationInfoOptions();

				Results = string.Empty;

				var result = session.GetLocationInfo(locationInfoOptions, IsIgnoreSearchWeight, IsUseGeoCoder);

				if (result.IsError)
					throw new EanApiException(result);

				Results = JsonConvert.SerializeObject(result, Formatting.Indented);

				SelectedTabIndex = 1;
			}
			catch (Exception exc)
			{
				RootModel.ShowErrorMessage(exc);
			}
			finally
			{
				Mouse.OverrideCursor = null;
			}
		}

		protected override string ValidateProperty(string propertyName)
		{
			if (IsSearchByAddress && propertyName == Prop(x => x.City) && string.IsNullOrWhiteSpace(City))
				return "City is not filled";

			if (IsSearchByDestinationId && propertyName == Prop(x => x.DestinationId) && string.IsNullOrWhiteSpace(DestinationId))
				return "DestinationId is not filled";

			if (IsSearchByDestinationString && propertyName == Prop(x => x.DestinationString) && string.IsNullOrWhiteSpace(DestinationString))
				return "Destination String is not filled";

			if (IsSearchByAddressString && propertyName == Prop(x => x.AddressString) && string.IsNullOrWhiteSpace(AddressString))
				return "Address is not filled";

			return base.ValidateProperty(propertyName);
		}
	}
}
