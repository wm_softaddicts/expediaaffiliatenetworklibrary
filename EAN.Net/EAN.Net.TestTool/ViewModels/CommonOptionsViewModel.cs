﻿using System.Collections.Generic;
using System.Linq;

namespace EAN.Net.TestTool.ViewModels
{
	public class CommonOptionsViewModel: ViewModelBase<CommonOptionsViewModel>
	{
		private readonly RootViewModel _rootModel;
		private int _minorRev;
		private long _cId;
		private string _apiKey;
		private string _sharedSecret;
		private LanguageViewModel _language;
		private CurrencyViewModel _currency;
		private ApiExperienceViewModel _apiExperience;

		public CommonOptionsViewModel(RootViewModel rootModel)
		{
			_rootModel = rootModel;
			ApiExperience = ApiExperiences.First();
		}

		public RootViewModel RootModel
		{
			get { return _rootModel; }
		}

		public IList<LanguageViewModel> Languages
		{
			get { return Catalog.Languages; }
		}

		public IList<CurrencyViewModel> Currencies
		{
			get { return Catalog.Currencies; }
		}

		public IList<ApiExperienceViewModel> ApiExperiences
		{
			get { return Catalog.ApiExperiences; }
		}

		public int MinorRev
		{
			get { return _minorRev; }
			set
			{
				_minorRev = value;
				NotifyPropertyChanged(x => x.MinorRev);
			}
		}

		public long CId
		{
			get { return _cId; }
			set
			{
				_cId = value;
				NotifyPropertyChanged(x => x.CId);
			}
		}

		public string ApiKey
		{
			get { return _apiKey; }
			set
			{
				_apiKey = value;
				NotifyPropertyChanged(x => x.ApiKey);
			}
		}

		public string SharedSecret
		{
			get { return _sharedSecret; }
			set
			{
				_sharedSecret = value;
				NotifyPropertyChanged(x => x.SharedSecret);
			}
		}

		public LanguageViewModel Language
		{
			get { return _language; }
			set
			{
				_language = value;
				NotifyPropertyChanged(x => x.Language);
			}
		}

		public CurrencyViewModel Currency
		{
			get { return _currency; }
			set
			{
				_currency = value;
				NotifyPropertyChanged(x => x.Currency);
			}
		}

		public ApiExperienceViewModel ApiExperience
		{
			get { return _apiExperience; }
			set
			{
				_apiExperience = value;
				NotifyPropertyChanged(x => ApiExperience);
			}
		}
	}
}
