﻿using System.Collections.ObjectModel;
using System.Linq;

namespace EAN.Net.TestTool.ViewModels
{
	public class RoomOptionsViewModel : ViewModelCollection<RoomOptionsViewModel, RoomViewModel>
	{
		public RoomOptionsViewModel()
		{
			NumberOfRooms = 1;
		}

		public ObservableCollection<RoomViewModel> Rooms
		{
			get { return Items; }
		}

		public int NumberOfRooms
		{
			get { return ItemCount; }
			set
			{
				ItemCount = value;

				foreach (var x in Rooms.Select((r, i) => new { Room = r, Index = i }))
				{
					if (x.Room.RoomIndex > 0)
						continue;

					x.Room.RoomIndex = x.Index + 1;
				}

				NotifyPropertyChanged(x => x.NumberOfRooms);
			}
		}
	}
}
