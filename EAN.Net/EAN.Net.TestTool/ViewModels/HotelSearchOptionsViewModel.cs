﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using EAN.Net.Arguments;
using EAN.Net.Arguments.Common;
using EAN.Net.Arguments.Response.HotelList;
using EAN.Net.TestTool.Common;
using EAN.Net.TestTool.Exceptions;
using Monads.NET;
using Newtonsoft.Json;

namespace EAN.Net.TestTool.ViewModels
{
	public class HotelSearchOptionsViewModel : ViewModelBase<HotelSearchOptionsViewModel>
	{
		private readonly RootViewModel _rootModel;
		private readonly RoomOptionsViewModel _roomOptions = new RoomOptionsViewModel();
		private readonly HotelSearchFilteringOptionsViewModel _searchFilteringOptions = new HotelSearchFilteringOptionsViewModel();
		private readonly HotelSearchDestinationOptionsViewModel _searchDestinationOptions = new HotelSearchDestinationOptionsViewModel();
		private readonly HotelSearchCacheOptionsViewModel _cacheOptions = new HotelSearchCacheOptionsViewModel();
		private readonly HotelSearchAmenityOptionsViewModel _amenityOptions = new HotelSearchAmenityOptionsViewModel();
		private HotelListResponse _responseResults;
		private string _results = string.Empty;

		private int _selectedTabIndex;
		private readonly RelayCommand _searchCommand;
		private readonly RelayCommand _nextResultsCommand;
		private readonly RelayCommand _backCommand;

		public HotelSearchOptionsViewModel(RootViewModel rootModel)
		{
			_rootModel = rootModel;

			_searchCommand = new RelayCommand(_ => SearchHotels());
			_nextResultsCommand = new RelayCommand(_ => GetNextResults());
			_backCommand = new RelayCommand(_ => SelectedTabIndex = 0);
		}

		public RootViewModel RootModel
		{
			get { return _rootModel; }
		}

		public RoomOptionsViewModel RoomOptions
		{
			get { return _roomOptions; }
		}

		public HotelSearchFilteringOptionsViewModel SearchFilteringOptions
		{
			get { return _searchFilteringOptions; }
		}

		public HotelSearchDestinationOptionsViewModel SearchDestinationOptions
		{
			get { return _searchDestinationOptions; }
		}

		public HotelSearchCacheOptionsViewModel CacheOptions
		{
			get { return _cacheOptions; }
		}

		public HotelSearchAmenityOptionsViewModel AmenityOptions
		{
			get { return _amenityOptions; }
		}

		public RelayCommand SearchCommand
		{
			get { return _searchCommand; }
		}

		public RelayCommand NextResultsCommand
		{
			get { return _nextResultsCommand; }
		}

		public RelayCommand BackCommand
		{
			get { return _backCommand; }
		}

		public int SelectedTabIndex
		{
			get { return _selectedTabIndex; }
			set
			{
				_selectedTabIndex = value;
				NotifyPropertyChanged(x => x.SelectedTabIndex);
			}
		}

		public string Results
		{
			get { return _results; }
			set
			{
				_results = value;
				NotifyPropertyChanged(x => x.Results);
			}
		}

		public bool IsNextResultsAvailable
		{
			get { return _responseResults != null && _responseResults.IsMoreResultsAvailable; }
		}

		private string ComposeAmenities()
		{
			var amenityList = new List<int>();

			if (AmenityOptions.IsIncludeSwimmingPool)
				amenityList.Add(1);
			if (AmenityOptions.IsIncludeFitnessCenter)
				amenityList.Add(2);
			if (AmenityOptions.IsIncludeRestaurant)
				amenityList.Add(3);
			if (AmenityOptions.IsIncludeChildrenActivities)
				amenityList.Add(4);
			if (AmenityOptions.IsIncludeComplimentaryBreakfast)
				amenityList.Add(5);
			if (AmenityOptions.IsIncludeMeetingFacilities)
				amenityList.Add(6);
			if (AmenityOptions.IsIncludePetsAllowed)
				amenityList.Add(7);
			if (AmenityOptions.IsIncludeWheelchairAccessible)
				amenityList.Add(8);
			if (AmenityOptions.IsIncludeKitchenKitchenette)
				amenityList.Add(9);

			return string.Join(",", amenityList);
		}

		private IHotelListSearchOptions GetHotelListSearchOptions()
		{
			if (SearchDestinationOptions.IsSearchByAddress)
			{
				return new HotelListSearchByAddressOptions
				{
					CountryCode = SearchDestinationOptions.Country.With(c => c.Code),
					StateProvinceCode = SearchDestinationOptions.State.With(s => s.Code),
					City = SearchDestinationOptions.City
				};
			}

			if (SearchDestinationOptions.IsSearchByDestinationString)
			{
				return new HotelListSearchByDestinationStringOptions
				{
					DestinationString = SearchDestinationOptions.DestinationString
				};
			}

			if (SearchDestinationOptions.IsSearchByDestinationId)
			{
				return new HotelListSearchByDestinationIdOptions
				{
					DestinationId = SearchDestinationOptions.DestinationId
				};
			}

			if (SearchDestinationOptions.IsSearchByHotelIdList)
			{
				var hotelListSearchByHotelIdListOptions = new HotelListSearchByHotelIdListOptions();

				foreach (var id in new[] { SearchDestinationOptions.HotelId1, SearchDestinationOptions.HotelId2, SearchDestinationOptions.HotelId3 }.Where(x => x != null))
				{
					hotelListSearchByHotelIdListOptions.HotelIdList.Add(id.Value);
				}

				return hotelListSearchByHotelIdListOptions;
			}

			if (SearchDestinationOptions.IsSearchByGeoLocation)
			{
				return new HotelListSearchByGeographicalAreaOptions
				{
					Latitude = SearchDestinationOptions.Latitude,
					Longitude = SearchDestinationOptions.Longitude,
					SearchRadius = SearchDestinationOptions.SearchRadius,
					SearchRadiusUnit = SearchDestinationOptions.SearchRadiusUnit.Code
				};
			}

			return null;
		}

		private void SearchHotels()
		{
			try
			{
				Mouse.OverrideCursor = Cursors.Wait;

				var client = RootModel.CreateClient();
				var session = RootModel.CreateSession(client);

				var rooms = new List<Room>();

				foreach (var r in RoomOptions.Rooms)
				{
					var room = new Room {NumberOfAdults = r.NumberOfAdults};
					foreach (var a in r.ChildAgeOptions.ChildAges)
					{
						room.ChildAges.Add(a.Age);
					}

					rooms.Add(room);
				}

				var arrivalDate = SearchFilteringOptions.ArrivalDate;
				var departureDate = SearchFilteringOptions.DepartureDate;

				var filterOptions = new HotelListFilterOptions
				{
					MinRate = SearchFilteringOptions.Rate.MinValue,
					MaxRate = SearchFilteringOptions.Rate.MaxValue,
					MinStarRating = SearchFilteringOptions.StarRating.MinValue,
					MaxStarRating = SearchFilteringOptions.StarRating.MaxValue,
					PropertyCategory = SearchFilteringOptions.PropertyCategory.Code,
					NumberOfBedRooms = SearchFilteringOptions.NumberOfBedrooms,
					MaxRatePlanCount = SearchFilteringOptions.MaxRatePlanCount,
					Amenities = ComposeAmenities()
				};

				var searchOptions = GetHotelListSearchOptions();

				var additionalSearchOptions = new HotelListAdditionalSearchOptions
				{
					Address = SearchDestinationOptions.FilterAddress,
					PostalCode = SearchDestinationOptions.FilterPostalCode,
					PropertyName = SearchDestinationOptions.FilterPropertyName
				};

				Results = string.Empty;

				_responseResults = session.GetHotelList(arrivalDate, departureDate, rooms, searchOptions, additionalSearchOptions,
					filterOptions, numberOfResults: SearchFilteringOptions.NumberOfResults, sort: SearchDestinationOptions.Sorting.Code);

				if (_responseResults.IsError)
					throw new EanApiException(_responseResults);

				Results = JsonConvert.SerializeObject(_responseResults, Formatting.Indented);

				NotifyPropertyChanged(x => x.IsNextResultsAvailable);

				SelectedTabIndex = 1;
			}
			catch (Exception exc)
			{
				RootModel.ShowErrorMessage(exc);
			}
			finally
			{
				Mouse.OverrideCursor = null;
			}
		}

		private void GetNextResults()
		{
			try
			{
				if (_responseResults == null)
					return;

				Mouse.OverrideCursor = Cursors.Wait;

				var client = RootModel.CreateClient();
				var session = RootModel.CreateSession(client);

				Results = string.Empty;

				_responseResults = session.GetHotelListNextResults(_responseResults);

				if (_responseResults.IsError)
					throw new EanApiException(_responseResults);

				Results = JsonConvert.SerializeObject(_responseResults, Formatting.Indented);

				NotifyPropertyChanged(x => x.IsNextResultsAvailable);

				SelectedTabIndex = 1;
			}
			catch (Exception exc)
			{
				RootModel.ShowErrorMessage(exc);
			}
			finally
			{
				Mouse.OverrideCursor = null;
			}
		}
	}
}
