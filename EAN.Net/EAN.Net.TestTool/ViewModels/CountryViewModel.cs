﻿using System.Collections.Generic;

namespace EAN.Net.TestTool.ViewModels
{
	public class CountryViewModel: CodeNameViewModel<string>
	{
		private readonly IList<StateViewModel> _states = new List<StateViewModel>();

		public IList<StateViewModel> States
		{
			get { return _states; }
		}
	}
}
