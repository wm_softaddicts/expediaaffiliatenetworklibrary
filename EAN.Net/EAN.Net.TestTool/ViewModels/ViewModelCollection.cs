﻿using System.Collections.ObjectModel;
using System.Linq;

namespace EAN.Net.TestTool.ViewModels
{
	public abstract class ViewModelCollection<TViewModel, TItem> : ViewModelBase<TViewModel>
		where TViewModel: ViewModelCollection<TViewModel, TItem>
		where TItem: new()
	{
		private readonly ObservableCollection<TItem> _items = new ObservableCollection<TItem>();

		protected ObservableCollection<TItem> Items
		{
			get { return _items; }
		}

		protected int ItemCount
		{
			get { return _items.Count; }
			set
			{
				if (value < _items.Count)
				{
					var itemsToDelete = _items.Skip(value).ToArray();

					foreach (var item in itemsToDelete)
					{
						_items.Remove(item);
					}
				}
				else if (value > _items.Count)
				{
					var itemsToAdd = Enumerable.Range(0, value - _items.Count).Select(i => new TItem()).ToArray();

					foreach (var item in itemsToAdd)
					{
						_items.Add(item);
					}
				}

				NotifyPropertyChanged(x => x.ItemCount);
				NotifyPropertyChanged(x => x.IsAny);
			}
		}

		public bool IsAny
		{
			get { return ItemCount > 0; }
		}
	}
}
