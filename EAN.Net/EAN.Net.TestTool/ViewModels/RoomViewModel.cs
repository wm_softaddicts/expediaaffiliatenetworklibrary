﻿namespace EAN.Net.TestTool.ViewModels
{
	public class RoomViewModel : ViewModelBase<RoomViewModel>
	{
		private int _roomIndex;
		private int _numberOfAdults = 1;
		private readonly HotelSearchChildAgeOptionsViewModel _childAgeOptions = new HotelSearchChildAgeOptionsViewModel();

		public int RoomIndex
		{
			get { return _roomIndex; }
			set
			{
				_roomIndex = value;
				NotifyPropertyChanged(x => x.RoomIndex);
			}
		}

		public int NumberOfAdults
		{
			get { return _numberOfAdults; }
			set
			{
				_numberOfAdults = value;
				NotifyPropertyChanged(x => x.NumberOfAdults);
			}
		}

		public HotelSearchChildAgeOptionsViewModel ChildAgeOptions
		{
			get { return _childAgeOptions; }
		}
	}
}
