﻿namespace EAN.Net.TestTool.ViewModels
{
	public class HotelRoomReservationPaymentOptionsViewModel : ViewModelBase<HotelRoomReservationPaymentOptionsViewModel>
	{
		private string _email = "test@yourSite.com";
		private string _firstName = "tester";
		private string _lastName = "testing";
		private string _homePhone = "123567890";
		private string _workPhone = "0987654321";
		private string _phoneExtension;
		private string _faxPhone;
		private string _companyName;
		private string _creditCardType = "CA";
		private string _creditCardNumber = "5401999999999999";
		private string _creditCardIdentifier = "123";
		private string _creditCardExpirationMonth = "11";
		private string _creditCardExpirationYear = "2015";

		public string Email
		{
			get { return _email; }
			set
			{
				_email = value;
				NotifyPropertyChanged(x => x.Email);
			}
		}

		public string FirstName
		{
			get { return _firstName; }
			set
			{
				_firstName = value;
				NotifyPropertyChanged(x => x.Email);
			}
		}

		public string LastName
		{
			get { return _lastName; }
			set
			{
				_lastName = value;
				NotifyPropertyChanged(x => x.Email);
			}
		}

		public string HomePhone
		{
			get { return _homePhone; }
			set
			{
				_homePhone = value;
				NotifyPropertyChanged(x => x.Email);
			}
		}

		public string WorkPhone
		{
			get { return _workPhone; }
			set
			{
				_workPhone = value;
				NotifyPropertyChanged(x => x.Email);
			}
		}

		public string PhoneExtension
		{
			get { return _phoneExtension; }
			set
			{
				_phoneExtension = value;
				NotifyPropertyChanged(x => x.Email);
			}
		}

		public string FaxPhone
		{
			get { return _faxPhone; }
			set
			{
				_faxPhone = value;
				NotifyPropertyChanged(x => x.Email);
			}
		}

		public string CompanyName
		{
			get { return _companyName; }
			set
			{
				_companyName = value;
				NotifyPropertyChanged(x => x.Email);
			}
		}

		public string CreditCardType
		{
			get { return _creditCardType; }
			set
			{
				_creditCardType = value;
				NotifyPropertyChanged(x => x.Email);
			}
		}

		public string CreditCardNumber
		{
			get { return _creditCardNumber; }
			set
			{
				_creditCardNumber = value;
				NotifyPropertyChanged(x => x.Email);
			}
		}

		public string CreditCardIdentifier
		{
			get { return _creditCardIdentifier; }
			set
			{
				_creditCardIdentifier = value;
				NotifyPropertyChanged(x => x.Email);
			}
		}

		public string CreditCardExpirationMonth
		{
			get { return _creditCardExpirationMonth; }
			set
			{
				_creditCardExpirationMonth = value;
				NotifyPropertyChanged(x => x.Email);
			}
		}

		public string CreditCardExpirationYear
		{
			get { return _creditCardExpirationYear; }
			set
			{
				_creditCardExpirationYear = value;
				NotifyPropertyChanged(x => x.Email);
			}
		}

		protected override string ValidateProperty(string propertyName)
		{
			if (propertyName == Prop(x => x.Email) && string.IsNullOrEmpty(Email))
				return "Email is not filled";

			if (propertyName == Prop(x => x.FirstName) && string.IsNullOrEmpty(FirstName))
				return "First name is not filled";

			if (propertyName == Prop(x => x.LastName) && string.IsNullOrEmpty(LastName))
				return "Last name is not filled";

			if (propertyName == Prop(x => x.HomePhone) && string.IsNullOrEmpty(HomePhone))
				return "Home phone is not filled";

			if (propertyName == Prop(x => x.CreditCardType) && string.IsNullOrEmpty(CreditCardType))
				return "Credit card type is not filled";

			if (propertyName == Prop(x => x.CreditCardNumber) && string.IsNullOrEmpty(CreditCardNumber))
				return "Credit card number is not filled";

			if (propertyName == Prop(x => x.CreditCardIdentifier) && string.IsNullOrEmpty(CreditCardIdentifier))
				return "Credit card identifier is not filled";

			if (propertyName == Prop(x => x.CreditCardExpirationMonth) && string.IsNullOrEmpty(CreditCardExpirationMonth))
				return "Credit card expiration month is not filled";

			if (propertyName == Prop(x => x.CreditCardExpirationYear) && string.IsNullOrEmpty(CreditCardExpirationYear))
				return "Credit card expiration year is not filled";

			return base.ValidateProperty(propertyName);
		}
	}
}
