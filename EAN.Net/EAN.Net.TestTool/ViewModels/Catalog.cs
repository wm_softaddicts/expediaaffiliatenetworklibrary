﻿using System.Collections.Generic;
using EAN.Net.Arguments;
using EAN.Net.Enums;

namespace EAN.Net.TestTool.ViewModels
{
	public static class Catalog
	{
		#region HotelList Detail Options

		public static readonly IList<HotelListDetailOptionsViewModel> HotelListDetailOptionList = new List<HotelListDetailOptionsViewModel>
		{
			new HotelListDetailOptionsViewModel {Code = HotelListDetailOptions.Default, Name = "DEFAULT"},
			new HotelListDetailOptionsViewModel {Code = HotelListDetailOptions.HotelSummary, Name = "HOTEL_SUMMARY"},
			new HotelListDetailOptionsViewModel {Code = HotelListDetailOptions.RoomRateDetails, Name = "ROOM_RATE_DETAILS"},
			new HotelListDetailOptionsViewModel {Code = HotelListDetailOptions.DeepLinks, Name = "DEEP_LINKS"},
		};

		#endregion

		#region HotelList Detail Options

		public static readonly IList<HotelSearchSortViewModel> HotelSearchSortKinds = new List<HotelSearchSortViewModel>
		{
			new HotelSearchSortViewModel {Code = HotelListSort.NoSort, Name = "NO_SORT"},
			new HotelSearchSortViewModel {Code = HotelListSort.CityValue, Name = "CITY_VALUE"},
			new HotelSearchSortViewModel {Code = HotelListSort.OverallValue, Name = "OVERALL_VALUE"},
			new HotelSearchSortViewModel {Code = HotelListSort.Promo, Name = "PROMO"},
			new HotelSearchSortViewModel {Code = HotelListSort.Price, Name = "PRICE"},
			new HotelSearchSortViewModel {Code = HotelListSort.PriceReverse, Name = "PRICE_REVERSE"},
			new HotelSearchSortViewModel {Code = HotelListSort.PriceAverage, Name = "PRICE_AVERAGE"},
			new HotelSearchSortViewModel {Code = HotelListSort.Quality, Name = "QUALITY"},
			new HotelSearchSortViewModel {Code = HotelListSort.QualityReverse, Name = "QUALITY_REVERSE"},
			new HotelSearchSortViewModel {Code = HotelListSort.Alpha, Name = "ALPHA"},
			new HotelSearchSortViewModel {Code = HotelListSort.Proximity, Name = "PROXIMITY"},
			new HotelSearchSortViewModel {Code = HotelListSort.PostalCode, Name = "POSTAL_CODE"},
			new HotelSearchSortViewModel {Code = HotelListSort.TripAdvisor, Name = "TRIP_ADVISOR"},
		};

		#endregion

		#region API Experiences

		public static IList<ApiExperienceViewModel> ApiExperiences = new List<ApiExperienceViewModel>
		{
			new ApiExperienceViewModel { Code = EanApiExperience.PartnerCallCenter, Name = "PARTNER_CALL_CENTER"},
			new ApiExperienceViewModel { Code = EanApiExperience.PartnerWebsite, Name = "PARTNER_WEBSITE"},
			new ApiExperienceViewModel { Code = EanApiExperience.PartnerMobileWeb, Name = "PARTNER_MOBILE_WEB"},
			new ApiExperienceViewModel { Code = EanApiExperience.PartnerMobileApp, Name = "PARTNER_MOBILE_APP"},
			new ApiExperienceViewModel { Code = EanApiExperience.PartnerBotCache, Name = "PARTNER_BOT_CACHE"},
			new ApiExperienceViewModel { Code = EanApiExperience.PartnerBotReporting, Name = "PARTNER_BOT_REPORTING"},
		};

		#endregion

		#region Languages

		public static readonly IList<LanguageViewModel> Languages = new List<LanguageViewModel>
		{
			new LanguageViewModel {Code = "en_US", Name = "en_US - English"},
			new LanguageViewModel {Code = "ar_SA", Name = "ar_SA - Arabic"},
			new LanguageViewModel {Code = "cz_CS", Name = "cz_CS - Czechoslovakian"},
			new LanguageViewModel {Code = "da_DK", Name = "da_DK - Danish"},
			new LanguageViewModel {Code = "de_DE", Name = "de_DE - German"},
			new LanguageViewModel {Code = "el_GR", Name = "el_GR - Greek"},
			new LanguageViewModel {Code = "et_EE", Name = "et_EE - Estonian"},
			new LanguageViewModel {Code = "es_MX", Name = "es_MX - Spanish (Mexico)"},
			new LanguageViewModel {Code = "es_ES", Name = "es_ES - Spanish (Spain)"},
			new LanguageViewModel {Code = "fi_FI", Name = "fi_FI - Finnish"},
			new LanguageViewModel {Code = "fr_FR", Name = "fr_FR - French"},
			new LanguageViewModel {Code = "hu_HU", Name = "hu_HU - Hungarian"},
			new LanguageViewModel {Code = "hr_HR", Name = "hr_HR - Croatian"},
			new LanguageViewModel {Code = "in_ID", Name = "in_ID - Indonesian"},
			new LanguageViewModel {Code = "is_IS", Name = "is_IS - Icelandic"},
			new LanguageViewModel {Code = "it_IT", Name = "it_IT - Italian"},
			new LanguageViewModel {Code = "ja_JP", Name = "ja_JP - Japanese"},
			new LanguageViewModel {Code = "ko_KR", Name = "ko_KR - Korean"},
			new LanguageViewModel {Code = "lv_LV", Name = "lv_LV - Latvian"},
			new LanguageViewModel {Code = "lt_LT", Name = "lt_LT - Lithuanian"},
			new LanguageViewModel {Code = "ms_MY", Name = "ms_MY - Malay"},
			new LanguageViewModel {Code = "nl_NL", Name = "nl_NL - Dutch"},
			new LanguageViewModel {Code = "no_NO", Name = "no_NO - Norwegian"},
			new LanguageViewModel {Code = "pl_PL", Name = "pl_PL - Polish"},
			new LanguageViewModel {Code = "pt_BR", Name = "pt_BR - Portuguese (Brazil)"},
			new LanguageViewModel {Code = "pt_PT", Name = "pt_PT - Portuguese (Portugal)"},
			new LanguageViewModel {Code = "ru_RU", Name = "ru_RU - Russian"},
			new LanguageViewModel {Code = "sv_SE", Name = "sv_SE - Swedish"},
			new LanguageViewModel {Code = "sk_SK", Name = "sk_SK - Slovak"},
			new LanguageViewModel {Code = "th_TH", Name = "th_TH - Thai"},
			new LanguageViewModel {Code = "tr_TR", Name = "tr_TR - Turkish"},
			new LanguageViewModel {Code = "uk_UA", Name = "uk_UA - Ukranian"},
			new LanguageViewModel {Code = "vi_VN", Name = "vi_VN - Vietnamese"},
			new LanguageViewModel {Code = "zh_TW", Name = "zh_TW - Traditional Chinese"},
			new LanguageViewModel {Code = "zh_CN", Name = "zh_CN - Simplified Chinese"},
		};

		#endregion

		#region Currencies

		public static readonly List<CurrencyViewModel> Currencies = new List<CurrencyViewModel>
		{
			new CurrencyViewModel {Code = "USD", Name = "USD (U.S. Dollars)"},
			new CurrencyViewModel {Code = "AUD", Name = "AUD (Australian Dollar)"},
			new CurrencyViewModel {Code = "BRL", Name = "BRL (BRAZIL REAL)"},
			new CurrencyViewModel {Code = "GBP", Name = "GBP (British Pound)"},
			new CurrencyViewModel {Code = "CAD", Name = "CAD (Canadian Dollar)"},
			new CurrencyViewModel {Code = "CHF", Name = "CHF (Swiss Francs)"},
			new CurrencyViewModel {Code = "CNY", Name = "CNY (CHINA YUAN)"},
			new CurrencyViewModel {Code = "DKK", Name = "DKK (Danish Kroner)"},
			new CurrencyViewModel {Code = "EUR", Name = "EUR (Euros)"},
			new CurrencyViewModel {Code = "HKD", Name = "HDK (Hong Kong Dollars)"},
			new CurrencyViewModel {Code = "IDR", Name = "IDR (INDONESIA RUPIAH)"},
			new CurrencyViewModel {Code = "ILS", Name = "ILS (ISRAEL NEW SHEQEL)"},
			new CurrencyViewModel {Code = "INR", Name = "INR (INDIA RUPEES)"},
			new CurrencyViewModel {Code = "JPY", Name = "JPY (Japanese Yen)"},
			new CurrencyViewModel {Code = "KRW", Name = "KRW (KOREA WON)"},
			new CurrencyViewModel {Code = "MXN", Name = "MXN (MEXICO PESO)"},
			new CurrencyViewModel {Code = "MYR", Name = "MYR (MALAYSIA RINGGIT)"},
			new CurrencyViewModel {Code = "NZD", Name = "NZD (New Zealand Dollars)"},
			new CurrencyViewModel {Code = "NOK", Name = "NOK (Norwegian Kroner)"},
			new CurrencyViewModel {Code = "RUB", Name = "RUB (RUSSIA RUBLES)"},
			new CurrencyViewModel {Code = "SGD", Name = "SGD (Singapore Dollars)"},
			new CurrencyViewModel {Code = "SEK", Name = "SEK (Swedish Krona)"},
			new CurrencyViewModel {Code = "TWD", Name = "TWD (TAIWAN DOLLARS)"},
			new CurrencyViewModel {Code = "ARS", Name = "ARS (ARGENTINA PESOS)"},
			new CurrencyViewModel {Code = "ATS", Name = "ATS (AUSTRALIA SCHILLINGS)"},
			new CurrencyViewModel {Code = "AUD", Name = "AUD (AUSTRALIA DOLLARS)"},
			new CurrencyViewModel {Code = "BBD", Name = "BBD (BARBADOS DOLLARS)"},
			new CurrencyViewModel {Code = "BEF", Name = "BEF (BELGIUM FRANCS)"},
			new CurrencyViewModel {Code = "BGL", Name = "BGL (BULGARIA LEV)"},
			new CurrencyViewModel {Code = "BMD", Name = "BMD (BERMUDA DOLLARS)"},
			new CurrencyViewModel {Code = "BSD", Name = "BSD (BAHAMAS DOLLARS)"},
			new CurrencyViewModel {Code = "CAD", Name = "CAD (CANADA DOLLARS)"},
			new CurrencyViewModel {Code = "CHF", Name = "CHF (SWITZERLAND FRANCS)"},
			new CurrencyViewModel {Code = "CLP", Name = "CLP (CHILE PESOS)"},
			new CurrencyViewModel {Code = "CSK", Name = "CSK (CZECH REPUBLIC KORUNA)"},
			new CurrencyViewModel {Code = "CYP", Name = "CYP (CYPRUS POUNDS)"},
			new CurrencyViewModel {Code = "DEM", Name = "DEM (GERMANY DEUTSCHE MARKS)"},
			new CurrencyViewModel {Code = "DKK", Name = "DKK (DENMARK KRONER)"},
			new CurrencyViewModel {Code = "DZD", Name = "DZD (ALGERIA DINARS)"},
			new CurrencyViewModel {Code = "EGP", Name = "EGP (EGYPT POUNDS)"},
			new CurrencyViewModel {Code = "ESP", Name = "ESP (SPAIN PESETAS)"},
			new CurrencyViewModel {Code = "EUR", Name = "EUR (EURO)"},
			new CurrencyViewModel {Code = "FIM", Name = "FIM (FINLAND MARKKA)"},
			new CurrencyViewModel {Code = "FJD", Name = "FJD (FIJI DOLLARS)"},
			new CurrencyViewModel {Code = "FRF", Name = "FRF (FRANCE FRANCS)"},
			new CurrencyViewModel {Code = "GBP", Name = "GBP (UNITED KINGDOM POUNDS)"},
			new CurrencyViewModel {Code = "GRD", Name = "GRD (GREECE DRACHMAS)"},
			new CurrencyViewModel {Code = "HKD", Name = "HKD (HONG KONG DOLLARS)"},
			new CurrencyViewModel {Code = "HUF", Name = "HUF (HUNGARY FORINT)"},
			new CurrencyViewModel {Code = "IEP", Name = "IEP (IRELAND PUNT)"},
			new CurrencyViewModel {Code = "ISK", Name = "ISK (ICELAND KRONA)"},
			new CurrencyViewModel {Code = "ITL", Name = "ITL (ITALY LIRA)"},
			new CurrencyViewModel {Code = "JMD", Name = "JMD (JAMAICA DOLLARS)"},
			new CurrencyViewModel {Code = "JPY", Name = "JPY (JAPAN YEN)"},
			new CurrencyViewModel {Code = "LBP", Name = "LBP (LEBANON POUNDS)"},
			new CurrencyViewModel {Code = "LUF", Name = "LUF (LUXEMBOURG FRANCS)"},
			new CurrencyViewModel {Code = "LTL", Name = "LTL (Lithuania ?)"},
			new CurrencyViewModel {Code = "MXP", Name = "MXP (MEXICO PESOS)"},
			new CurrencyViewModel {Code = "NLG", Name = "NLG (NETHERLAND (DUTCH) GUILDERS)"},
			new CurrencyViewModel {Code = "NOK", Name = "NOK (NORWAY KRONER)"},
			new CurrencyViewModel {Code = "NZD", Name = "NZD (NEW ZEALAND DOLLARS)"},
			new CurrencyViewModel {Code = "PHP", Name = "PHP (PHILIPPINES PESOS)"},
			new CurrencyViewModel {Code = "PKR", Name = "PKR (PAKISTAN RUPEES)"},
			new CurrencyViewModel {Code = "PLN", Name = "PLN (POLAND ZLOTY)"},
			new CurrencyViewModel {Code = "PTE", Name = "PTE (PORTUGAL ESCUDO)"},
			new CurrencyViewModel {Code = "ROL", Name = "ROL (ROMANIA LEU)"},
			new CurrencyViewModel {Code = "SAR", Name = "SAR (SAUDI ARABIA RIYAL)"},
			new CurrencyViewModel {Code = "SDD", Name = "SDD (SUDAN DINAR)"},
			new CurrencyViewModel {Code = "SEK", Name = "SEK (SWEDEN KRONA)"},
			new CurrencyViewModel {Code = "SGD", Name = "SGD (SINGAPORE DOLLARS)"},
			new CurrencyViewModel {Code = "SKK", Name = "SKK (SLOVAKIA KORUNA)"},
			new CurrencyViewModel {Code = "THB", Name = "THB (THAILAND BAHT)"},
			new CurrencyViewModel {Code = "TRL", Name = "TRL (TURKEY LIRA)"},
			new CurrencyViewModel {Code = "TTD", Name = "TTD (TRINIDAD AND TABAGO DOLLARS)"},
			new CurrencyViewModel {Code = "USD", Name = "USD (UNITED STATES DOLLARS)"},
			new CurrencyViewModel {Code = "VEB", Name = "VEB (VENEZUELA BOLIVAR)"},
			new CurrencyViewModel {Code = "XCD", Name = "XCD (EASTERN CARIBBEAN DOLLARS)"},
			new CurrencyViewModel {Code = "ZAR", Name = "ZAR (SOUTH AFRICA RAND)"},
			new CurrencyViewModel {Code = "ZMK", Name = "ZMK (ZAMBIA KWACHA)"},
		};

		#endregion

		#region Property Categories

		public static readonly IList<HotelSearchPropertyCategoryViewModel> PropertyCategories = new List<HotelSearchPropertyCategoryViewModel>
		{
			new HotelSearchPropertyCategoryViewModel {Code = PropertyCategory.Hotel, Name = "Hotel (1)"},
			new HotelSearchPropertyCategoryViewModel {Code = PropertyCategory.Suite, Name = "Suite (2)"},
			new HotelSearchPropertyCategoryViewModel {Code = PropertyCategory.Resort, Name = "Resort (3)"},
			new HotelSearchPropertyCategoryViewModel {Code = PropertyCategory.VacationRental, Name = "Vacation Rental / Condo (4)"},
			new HotelSearchPropertyCategoryViewModel {Code = PropertyCategory.BedBreakfast, Name = "Bed & Breakfast (5)"},
			new HotelSearchPropertyCategoryViewModel {Code = PropertyCategory.AllInclusive, Name = "All Inclusive (6)"},
			new HotelSearchPropertyCategoryViewModel {Code = PropertyCategory.Hotel | PropertyCategory.Resort | PropertyCategory.VacationRental, Name = "Hotel + Resort + Condo (1,3,4)"},
		};

		#endregion

		#region Supplier Types

		public static readonly IList<SupplierTypeViewModel> SupplierTypes = new List<SupplierTypeViewModel>
		{
			new SupplierTypeViewModel {Code = null, Name = "All"},
			new SupplierTypeViewModel {Code = "E", Name = "Expedia Only (E)"},
		};

		#endregion

		#region Search Radius Units

		public static readonly IList<HotelSearchRadiusUnitViewModel> SearchRadiusUnits = new List<HotelSearchRadiusUnitViewModel>
		{
			new HotelSearchRadiusUnitViewModel {Code = "MI", Name = "Miles (MI)"},
			new HotelSearchRadiusUnitViewModel {Code = "KM", Name = "Kilometers (KM)"},
		};

		#endregion

		#region Countries

		public static readonly IList<CountryViewModel> Countries = new List<CountryViewModel>
		{
			new CountryViewModel
			{
				Code="US", 
				Name="United States (US)",
				States =
				{
					new StateViewModel {Code="AL", Name="Alabama (AL)"},
					new StateViewModel {Code="AS", Name="American Samoa (AS)"},
					new StateViewModel {Code="AZ", Name="Arizona (AZ)"},
					new StateViewModel {Code="AR", Name="Arkansas (AR)"},
					new StateViewModel {Code="AK", Name="Alaska (AK)"},
					new StateViewModel {Code="CA", Name="California "},
					new StateViewModel {Code="CO", Name="Colorado (CO)"},
					new StateViewModel {Code="CT", Name="Connecticut (CT)"},
					new StateViewModel {Code="DE", Name="Delaware (DE)"},
					new StateViewModel {Code="DC", Name="District of Columbia (DC)"},
					new StateViewModel {Code="FM", Name="Federated States of Micronesia (FM)"},
					new StateViewModel {Code="FL", Name="Florida (FL)"},
					new StateViewModel {Code="GA", Name="Georgia (GA)"},
					new StateViewModel {Code="GU", Name="Guam (GU)"},
					new StateViewModel {Code="HI", Name="Hawaii (HI)"},
					new StateViewModel {Code="ID", Name="Idaho (ID)"},
					new StateViewModel {Code="IL", Name="Illinois (IL)"},
					new StateViewModel {Code="IN", Name="Indiana (IN)"},
					new StateViewModel {Code="IA", Name="Iowa (IA)"},
					new StateViewModel {Code="KS", Name="Kansas (KS)"},
					new StateViewModel {Code="KY", Name="Kentucky (KY)"},
					new StateViewModel {Code="LA", Name="Louisiana (LA)"},
					new StateViewModel {Code="ME", Name="Maine (ME)"},
					new StateViewModel {Code="MH", Name="Marshall Islands (MH)"},
					new StateViewModel {Code="MD", Name="Maryland (MD)"},
					new StateViewModel {Code="MA", Name="Massachusetts (MA)"},
					new StateViewModel {Code="MI", Name="Michigan (MI)"},
					new StateViewModel {Code="MN", Name="Minnesota (MN)"},
					new StateViewModel {Code="MS", Name="Mississippi (MS)"},
					new StateViewModel {Code="MO", Name="Missouri (MO)"},
					new StateViewModel {Code="MT", Name="Montana (MT)"},
					new StateViewModel {Code="NE", Name="Nebraska (NE)"},
					new StateViewModel {Code="NV", Name="Nevada (NV)"},
					new StateViewModel {Code="NH", Name="New Hampshire (NH)"},
					new StateViewModel {Code="NJ", Name="New Jersey (NJ)"},
					new StateViewModel {Code="NM", Name="New Mexico (NM)"},
					new StateViewModel {Code="NY", Name="New York (NY)"},
					new StateViewModel {Code="NC", Name="North Carolina (NC)"},
					new StateViewModel {Code="ND", Name="North Dakota (ND)"},
					new StateViewModel {Code="MP", Name="Northern Mariana Islands (MP)"},
					new StateViewModel {Code="OH", Name="Ohio (OH)"},
					new StateViewModel {Code="OK", Name="Oklahoma (OK)"},
					new StateViewModel {Code="OR", Name="Oregon (OR)"},
					new StateViewModel {Code="PW", Name="Palau (PW)"},
					new StateViewModel {Code="PA", Name="Pennsylvania (PA)"},
					new StateViewModel {Code="PR", Name="Puerto Rico (PR)"},
					new StateViewModel {Code="RI", Name="Rhode Island (RI)"},
					new StateViewModel {Code="SC", Name="South Carolina (SC)"},
					new StateViewModel {Code="SD", Name="South Dakota (SD)"},
					new StateViewModel {Code="TN", Name="Tennessee (TN)"},
					new StateViewModel {Code="TX", Name="Texas (TX)"},
					new StateViewModel {Code="UT", Name="Utah (UT)"},
					new StateViewModel {Code="VT", Name="Vermont (VT)"},
					new StateViewModel {Code="VI", Name="Virgin Islands (VI)"},
					new StateViewModel {Code="VA", Name="Virginia (VA)"},
					new StateViewModel {Code="WA", Name="Washington (WA)"},
					new StateViewModel {Code="WV", Name="West Virginia (WV)"},
					new StateViewModel {Code="WI", Name="Wisconsin (WI)"},
					new StateViewModel {Code="WY", Name="Wyoming (WY)"},
				}
			},

			new CountryViewModel
			{
				Code="CA", 
				Name="Canada (CA)",
				States =
				{
					new StateViewModel {Code="AB", Name="Alberta (AB)"},
					new StateViewModel {Code="BC", Name="British Columbia (BC)"},
					new StateViewModel {Code="MB", Name="Manitoba (MB)"},
					new StateViewModel {Code="NB", Name="New Brunswick (NB)"},
					new StateViewModel {Code="NF", Name="Newfoundland (NF)"},
					new StateViewModel {Code="NT", Name="Northwest Territories (NT)"},
					new StateViewModel {Code="NS", Name="Nova Scotia (NS)"},
					new StateViewModel {Code="ON", Name="Ontario (ON)"},
					new StateViewModel {Code="PE", Name="Prince Edward Island (PE)"},
					new StateViewModel {Code="PQ", Name="Quebec (PQ)"},
					new StateViewModel {Code="SP", Name="St. Pierre &amp; Miguelon (SP)"},
					new StateViewModel {Code="SK", Name="Saskatchewan (SK)"},
					new StateViewModel {Code="YU", Name="Yukon (YU)"},
				}
			},

			new CountryViewModel
			{
				Code="AU", 
				Name="Australia (AU)",
				States =
				{
					new StateViewModel {Code="AC", Name="Australian Capital (AC)"},
					new StateViewModel {Code="NW", Name="New South Wales (NW)"},
					new StateViewModel {Code="NO", Name="Northern Territory (NO)"},
					new StateViewModel {Code="QL", Name="Queensland (QL)"},
					new StateViewModel {Code="SA", Name="South Australia (SA)"},
					new StateViewModel {Code="TS", Name="Tasmania (TS)"},
					new StateViewModel {Code="VC", Name="Victoria (VC)"},
					new StateViewModel {Code="WT", Name="Western Australia (WT)"},
				}
			},

			new CountryViewModel {Code="GB", Name="Great Britain (GB)"},
			new CountryViewModel {Code="AF", Name="Afghanistan (AF)"},
			new CountryViewModel {Code="AL", Name="Albania (AL)"},
			new CountryViewModel {Code="DZ", Name="Algeria (DZ)"},
			new CountryViewModel {Code="AS", Name="American Samoa (AS)"},
			new CountryViewModel {Code="AD", Name="Andorra (AD)"},
			new CountryViewModel {Code="AO", Name="Angola (AO)"},
			new CountryViewModel {Code="AI", Name="Anguilla (AI)"},
			new CountryViewModel {Code="AQ", Name="Antarctica (AQ)"},
			new CountryViewModel {Code="AG", Name="Antigua And Barbuda (AG)"},
			new CountryViewModel {Code="AR", Name="Argentina (AR)"},
			new CountryViewModel {Code="AM", Name="Armenia (AM)"},
			new CountryViewModel {Code="AW", Name="Aruba (AW)"},
			new CountryViewModel {Code="AT", Name="Austria (AT)"},
			new CountryViewModel {Code="AZ", Name="Azerbaijan (AZ)"},
			new CountryViewModel {Code="BS", Name="Bahamas (BS)"},
			new CountryViewModel {Code="BH", Name="Bahrain (BH)"},
			new CountryViewModel {Code="BD", Name="Bangladesh (BD)"},
			new CountryViewModel {Code="BB", Name="Barbados (BB)"},
			new CountryViewModel {Code="BY", Name="Belarus (BY)"},
			new CountryViewModel {Code="BE", Name="Belgium (BE)"},
			new CountryViewModel {Code="BZ", Name="Belize (BZ)"},
			new CountryViewModel {Code="BJ", Name="Benin (BJ)"},
			new CountryViewModel {Code="BM", Name="Bermuda (BM)"},
			new CountryViewModel {Code="BT", Name="Bhutan (BT)"},
			new CountryViewModel {Code="BO", Name="Bolivia (BO)"},
			new CountryViewModel {Code="BA", Name="Bosnia And Herzegovinia (BA)"},
			new CountryViewModel {Code="BW", Name="Botswana (BW)"},
			new CountryViewModel {Code="BV", Name="Bouvet Island (BV)"},
			new CountryViewModel {Code="BR", Name="Brazil (BR)"},
			new CountryViewModel {Code="IO", Name="British Indian Ocean Territory (IO)"},
			new CountryViewModel {Code="BN", Name="Brunei Darussalam (BN)"},
			new CountryViewModel {Code="BG", Name="Bulgaria (BG)"},
			new CountryViewModel {Code="BF", Name="Burkina Faso (BF)"},
			new CountryViewModel {Code="BI", Name="Burundi (BI)"},
			new CountryViewModel {Code="KH", Name="Cambodia (KH)"},
			new CountryViewModel {Code="CM", Name="Cameroon (CM)"},
			new CountryViewModel {Code="CV", Name="Cape Verde (CV)"},
			new CountryViewModel {Code="KY", Name="Cayman Islands (KY)"},
			new CountryViewModel {Code="CF", Name="Central African Republic (CF)"},
			new CountryViewModel {Code="TD", Name="Chad (TD)"},
			new CountryViewModel {Code="CL", Name="Chile (CL)"},
			new CountryViewModel {Code="CN", Name="China (CN)"},
			new CountryViewModel {Code="CX", Name="Christmas Island (CX)"},
			new CountryViewModel {Code="CC", Name="Cocos (Keeling) Islands (CC)"},
			new CountryViewModel {Code="CO", Name="Colombia (CO)"},
			new CountryViewModel {Code="KM", Name="Comoros (KM)"},
			new CountryViewModel {Code="CG", Name="Congo (CG)"},
			new CountryViewModel {Code="CD", Name="Congo, DR Of The (CD)"},
			new CountryViewModel {Code="CK", Name="Cook Islands (CK)"},
			new CountryViewModel {Code="CR", Name="Costa Rica (CR)"},
			new CountryViewModel {Code="CI", Name="Cote D'ivoire (CI)"},
			new CountryViewModel {Code="HR", Name="Croatia (HR)"},
			new CountryViewModel {Code="CU", Name="Cuba (CU)"},
			new CountryViewModel {Code="CY", Name="Cyprus (CY)"},
			new CountryViewModel {Code="CZ", Name="Czech Republic (CZ)"},
			new CountryViewModel {Code="DK", Name="Denmark (DK)"},
			new CountryViewModel {Code="DJ", Name="Djibouti (DJ)"},
			new CountryViewModel {Code="DM", Name="Dominica (DM)"},
			new CountryViewModel {Code="DO", Name="Dominican Republic (DO)"},
			new CountryViewModel {Code="TP", Name="East Timor (TP)"},
			new CountryViewModel {Code="EC", Name="Ecuador (EC)"},
			new CountryViewModel {Code="EG", Name="Egypt (EG)"},
			new CountryViewModel {Code="SV", Name="El Salvador (SV)"},
			new CountryViewModel {Code="GQ", Name="Equatorial Guinea (GQ)"},
			new CountryViewModel {Code="ER", Name="Eritrea (ER)"},
			new CountryViewModel {Code="EE", Name="Estonia (EE)"},
			new CountryViewModel {Code="ET", Name="Ethiopia (ET)"},
			new CountryViewModel {Code="FK", Name="Falkland Islands (FK)"},
			new CountryViewModel {Code="FO", Name="Faroe Islands (FO)"},
			new CountryViewModel {Code="FJ", Name="Fiji (FJ)"},
			new CountryViewModel {Code="FI", Name="Finland (FI)"},
			new CountryViewModel {Code="FR", Name="France (FR)"},
			new CountryViewModel {Code="FX", Name="France, Metropolitan (FX)"},
			new CountryViewModel {Code="GF", Name="French Guiana (GF)"},
			new CountryViewModel {Code="PF", Name="French Polynesia (PF)"},
			new CountryViewModel {Code="TF", Name="French Southern Territories (TF)"},
			new CountryViewModel {Code="GA", Name="Gabon (GA)"},
			new CountryViewModel {Code="GM", Name="Gambia (GM)"},
			new CountryViewModel {Code="GE", Name="Georgia (GE)"},
			new CountryViewModel {Code="DE", Name="Germany (DE)"},
			new CountryViewModel {Code="GH", Name="Ghana (GH)"},
			new CountryViewModel {Code="GI", Name="Gibraltar (GI)"},
			new CountryViewModel {Code="GR", Name="Greece (GR)"},
			new CountryViewModel {Code="GL", Name="Greenland (GL)"},
			new CountryViewModel {Code="GD", Name="Grenada (GD)"},
			new CountryViewModel {Code="GP", Name="Guadeloupe (GP)"},
			new CountryViewModel {Code="GU", Name="Guam (GU)"},
			new CountryViewModel {Code="GT", Name="Guatemala (GT)"},
			new CountryViewModel {Code="GN", Name="Guinea (GN)"},
			new CountryViewModel {Code="GW", Name="Guinea-Bissau (GW)"},
			new CountryViewModel {Code="GY", Name="Guyana (GY)"},
			new CountryViewModel {Code="HT", Name="Haiti (HT)"},
			new CountryViewModel {Code="HM", Name="Heard And Mcdonald Islands (HM)"},
			new CountryViewModel {Code="HN", Name="Honduras (HN)"},
			new CountryViewModel {Code="HK", Name="Hong Kong (HK)"},
			new CountryViewModel {Code="HU", Name="Hungary (HU)"},
			new CountryViewModel {Code="IS", Name="Iceland (IS)"},
			new CountryViewModel {Code="IN", Name="India (IN)"},
			new CountryViewModel {Code="ID", Name="Indonesia (ID)"},
			new CountryViewModel {Code="IR", Name="Iran (IR)"},
			new CountryViewModel {Code="IQ", Name="Iraq (IQ)"},
			new CountryViewModel {Code="IE", Name="Ireland (IE)"},
			new CountryViewModel {Code="IL", Name="Israel (IL)"},
			new CountryViewModel {Code="IT", Name="Italy (IT)"},
			new CountryViewModel {Code="JM", Name="Jamaica (JM)"},
			new CountryViewModel {Code="JP", Name="Japan (JP)"},
			new CountryViewModel {Code="JO", Name="Jordan (JO)"},
			new CountryViewModel {Code="KZ", Name="Kazakhstan (KZ)"},
			new CountryViewModel {Code="KE", Name="Kenya (KE)"},
			new CountryViewModel {Code="KI", Name="Kiribati (KI)"},
			new CountryViewModel {Code="KP", Name="Korea, DPR Of (KP)"},
			new CountryViewModel {Code="KR", Name="Korea, Republic Of (KR)"},
			new CountryViewModel {Code="KW", Name="Kuwait (KW)"},
			new CountryViewModel {Code="KG", Name="Kyrgyzstan (KG)"},
			new CountryViewModel {Code="LA", Name="Laos (LA)"},
			new CountryViewModel {Code="LV", Name="Latvia (LV)"},
			new CountryViewModel {Code="LB", Name="Lebanon (LB)"},
			new CountryViewModel {Code="LS", Name="Lesotho (LS)"},
			new CountryViewModel {Code="LR", Name="Liberia (LR)"},
			new CountryViewModel {Code="LY", Name="Libyan Arab Jamahiriya (LY)"},
			new CountryViewModel {Code="LI", Name="Liechtenstein (LI)"},
			new CountryViewModel {Code="LT", Name="Lithuania (LT)"},
			new CountryViewModel {Code="LU", Name="Luxembourg (LU)"},
			new CountryViewModel {Code="MO", Name="Macau (MO)"},
			new CountryViewModel {Code="MK", Name="Macedonia, FYR Of (MK)"},
			new CountryViewModel {Code="MG", Name="Madagascar (MG)"},
			new CountryViewModel {Code="MW", Name="Malawi (MW)"},
			new CountryViewModel {Code="MY", Name="Malaysia (MY)"},
			new CountryViewModel {Code="MV", Name="Maldives (MV)"},
			new CountryViewModel {Code="ML", Name="Mali (ML)"},
			new CountryViewModel {Code="MT", Name="Malta (MT)"},
			new CountryViewModel {Code="MH", Name="Marshall Islands (MH)"},
			new CountryViewModel {Code="MQ", Name="Martinique (MQ)"},
			new CountryViewModel {Code="MR", Name="Mauritania (MR)"},
			new CountryViewModel {Code="MU", Name="Mauritius (MU)"},
			new CountryViewModel {Code="YT", Name="Mayotte (YT)"},
			new CountryViewModel {Code="MX", Name="Mexico (MX)"},
			new CountryViewModel {Code="FM", Name="Micronesia, FS Of (FM)"},
			new CountryViewModel {Code="MD", Name="Moldova, Republic Of (MD)"},
			new CountryViewModel {Code="MC", Name="Monaco (MC)"},
			new CountryViewModel {Code="MN", Name="Mongolia (MN)"},
			new CountryViewModel {Code="MS", Name="Montserrat (MS)"},
			new CountryViewModel {Code="MA", Name="Morocco (MA)"},
			new CountryViewModel {Code="MZ", Name="Mozambique (MZ)"},
			new CountryViewModel {Code="MM", Name="Myanmar (MM)"},
			new CountryViewModel {Code="NA", Name="Namibia (NA)"},
			new CountryViewModel {Code="NR", Name="Nauru (NR)"},
			new CountryViewModel {Code="NP", Name="Nepal (NP)"},
			new CountryViewModel {Code="NL", Name="Netherlands (NL)"},
			new CountryViewModel {Code="AN", Name="Netherlands Antilles (AN)"},
			new CountryViewModel {Code="NC", Name="New Caledonia (NC)"},
			new CountryViewModel {Code="NZ", Name="New Zealand (NZ)"},
			new CountryViewModel {Code="NI", Name="Nicaragua (NI)"},
			new CountryViewModel {Code="NE", Name="Niger (NE)"},
			new CountryViewModel {Code="NG", Name="Nigeria (NG)"},
			new CountryViewModel {Code="NU", Name="Niue (NU)"},
			new CountryViewModel {Code="NF", Name="Norfolk Island (NF)"},
			new CountryViewModel {Code="MP", Name="Northern Mariana Islands (MP)"},
			new CountryViewModel {Code="NO", Name="Norway (NO)"},
			new CountryViewModel {Code="OM", Name="Oman (OM)"},
			new CountryViewModel {Code="PK", Name="Pakistan (PK)"},
			new CountryViewModel {Code="PW", Name="Palau (PW)"},
			new CountryViewModel {Code="PA", Name="Panama (PA)"},
			new CountryViewModel {Code="PG", Name="Papua New Guinea (PG)"},
			new CountryViewModel {Code="PY", Name="Paraguay (PY)"},
			new CountryViewModel {Code="PE", Name="Peru (PE)"},
			new CountryViewModel {Code="PH", Name="Philippines (PH)"},
			new CountryViewModel {Code="PN", Name="Pitcairn (PN)"},
			new CountryViewModel {Code="PL", Name="Poland (PL)"},
			new CountryViewModel {Code="PT", Name="Portugal (PT)"},
			new CountryViewModel {Code="PR", Name="Puerto Rico (PR)"},
			new CountryViewModel {Code="QA", Name="Qatar (QA)"},
			new CountryViewModel {Code="RE", Name="Reunion (RE)"},
			new CountryViewModel {Code="RO", Name="Romania (RO)"},
			new CountryViewModel {Code="RU", Name="Russian Federation (RU)"},
			new CountryViewModel {Code="RW", Name="Rwanda (RW)"},
			new CountryViewModel {Code="KN", Name="Saint Kitts And Nevis (KN)"},
			new CountryViewModel {Code="LC", Name="Saint Lucia (LC)"},
			new CountryViewModel {Code="WS", Name="Samoa (WS)"},
			new CountryViewModel {Code="SM", Name="San Marino (SM)"},
			new CountryViewModel {Code="ST", Name="Sao Tome And Principe (ST)"},
			new CountryViewModel {Code="SA", Name="Saudi Arabia (SA)"},
			new CountryViewModel {Code="SN", Name="Senegal (SN)"},
			new CountryViewModel {Code="SC", Name="Seychelles (SC) "},
			new CountryViewModel {Code="SL", Name="Sierra Leone (SL)"},
			new CountryViewModel {Code="SG", Name="Singapore (SG)"},
			new CountryViewModel {Code="SK", Name="Slovakia (Slovak Republic) (SK)"},
			new CountryViewModel {Code="SI", Name="Slovenia (SI)"},
			new CountryViewModel {Code="SB", Name="Solomon Islands (SB)"},
			new CountryViewModel {Code="SO", Name="Somalia (SO)"},
			new CountryViewModel {Code="ZA", Name="South Africa (ZA)"},
			new CountryViewModel {Code="GS", Name="South Georgia (GS)"},
			new CountryViewModel {Code="ES", Name="Spain (ES)"},
			new CountryViewModel {Code="LK", Name="Sri Lanka (LK)"},
			new CountryViewModel {Code="VC", Name="St Vincent And The Grenadines (VC)"},
			new CountryViewModel {Code="SH", Name="St. Helena (SH)"},
			new CountryViewModel {Code="PM", Name="St. Pierre And Miquelon (PM)"},
			new CountryViewModel {Code="SD", Name="Sudan (SD)"},
			new CountryViewModel {Code="SR", Name="Suriname (SR)"},
			new CountryViewModel {Code="SJ", Name="Svalbard And Jan Mayen Islands (SJ)"},
			new CountryViewModel {Code="SZ", Name="Swaziland (SZ)"},
			new CountryViewModel {Code="SE", Name="Sweden (SE)"},
			new CountryViewModel {Code="CH", Name="Switzerland (CH)"},
			new CountryViewModel {Code="SY", Name="Syrian Arab Republic (SY)"},
			new CountryViewModel {Code="TW", Name="Taiwan (TW)"},
			new CountryViewModel {Code="TJ", Name="Tajikistan (TJ)"},
			new CountryViewModel {Code="TZ", Name="Tanzania (TZ)"},
			new CountryViewModel {Code="TH", Name="Thailand (TH)"},
			new CountryViewModel {Code="TG", Name="Togo (TG)"},
			new CountryViewModel {Code="TK", Name="Tokelau (TK)"},
			new CountryViewModel {Code="TO", Name="Tonga (TO)"},
			new CountryViewModel {Code="TT", Name="Trinidad And Tobago (TT)"},
			new CountryViewModel {Code="TN", Name="Tunisia (TN)"},
			new CountryViewModel {Code="TR", Name="Turkey (TR)"},
			new CountryViewModel {Code="TM", Name="Turkmenistan (TM)"},
			new CountryViewModel {Code="TC", Name="Turks And Caicos Islands (TC)"},
			new CountryViewModel {Code="TV", Name="Tuvalu (TV)"},
			new CountryViewModel {Code="UG", Name="Uganda (UG)"},
			new CountryViewModel {Code="UA", Name="Ukraine (UA)"},
			new CountryViewModel {Code="AE", Name="United Arab Emirates (AE)"},
			new CountryViewModel {Code="UK", Name="United Kingdom (UK)"},
			new CountryViewModel {Code="UY", Name="Uruguay (UY)"},
			new CountryViewModel {Code="UM", Name="US Minor Outlying Islands (UM)"},
			new CountryViewModel {Code="UZ", Name="Uzbekistan (UZ)"},
			new CountryViewModel {Code="VU", Name="Vanuatu (VU)"},
			new CountryViewModel {Code="VA", Name="Vatican City State (VA)"},
			new CountryViewModel {Code="VE", Name="Venezuela (VE)"},
			new CountryViewModel {Code="VN", Name="Viet Nam (VN)"},
			new CountryViewModel {Code="VG", Name="Virgin Islands - British (VG)"},
			new CountryViewModel {Code="VI", Name="Virgin Islands - U.S. (VI)"},
			new CountryViewModel {Code="WF", Name="Wallis And Futuna Islands (WF)"},
			new CountryViewModel {Code="EH", Name="Western Sahara (EH)"},
			new CountryViewModel {Code="YE", Name="Yemen (YE)"},
			new CountryViewModel {Code="YU", Name="Yugoslavia (YU)"},
			new CountryViewModel {Code="ZM", Name="Zambia (ZM)"},
			new CountryViewModel {Code="ZW", Name="Zimbabwe (ZW)"},
		};

		#endregion

		#region Destinations

		public static IList<HotelSearchDestinationViewModel> Destinations = new List<HotelSearchDestinationViewModel>
		{
			new HotelSearchDestinationViewModel("Amsterdam","NL"),
			new HotelSearchDestinationViewModel("Barcelona","ES"),
			new HotelSearchDestinationViewModel("Beijing","CN"),
			new HotelSearchDestinationViewModel("Boston","MA","US"),
			new HotelSearchDestinationViewModel("Chicago","IL","US"),
			new HotelSearchDestinationViewModel("Dallas","TX","US"),
			new HotelSearchDestinationViewModel("Hong Kong","HK"),
			new HotelSearchDestinationViewModel("Las Vegas","NV","US"),
			new HotelSearchDestinationViewModel("London","GB"),
			new HotelSearchDestinationViewModel("Los Angeles","CA","US"),
			new HotelSearchDestinationViewModel("New York","NY","US"),
			new HotelSearchDestinationViewModel("Orlando","FL","US"),
			new HotelSearchDestinationViewModel("Paris","FR"),
			new HotelSearchDestinationViewModel("San Diego","CA","US"),
			new HotelSearchDestinationViewModel("Seattle","WA","US"),
			new HotelSearchDestinationViewModel("St Louis","MO","US"),
			new HotelSearchDestinationViewModel("Toronto","ON","CA"),
			new HotelSearchDestinationViewModel("Washington","DC","US"),
			new HotelSearchDestinationViewModel("Prague","CZ"),
			new HotelSearchDestinationViewModel("Rome","IT"),
			new HotelSearchDestinationViewModel("Shanghai","CN"),
			new HotelSearchDestinationViewModel("Singapore","SG"),
			new HotelSearchDestinationViewModel("Stockholm","SE"),
			new HotelSearchDestinationViewModel("Sydney","NW","AU"),
		};

		#endregion

		#region Supplier Cache Tolerance

		public static IList<string> SupplierCacheToleranceValues = new List<string>
		{
			"MIN",
			"MIN_ENHANCED",
			"MED",
			"MED_ENHANCED",
			"MAX",
			"MAX_ENHANCED",
		};

		#endregion

		#region DestinationTypes

		public static IList<DestinationTypeViewModel> DestinationTypes = new List<DestinationTypeViewModel>
		{
			new DestinationTypeViewModel {Code = LocationType.CityLocation, Name = "Location"},
			new DestinationTypeViewModel {Code = LocationType.LandmarkLocation, Name = "Landmarks"},
		};

		#endregion

		#region CancellationReasons

		public static IList<CancellationReasonViewModel> CancellationReasons = new List<CancellationReasonViewModel>
		{
			new CancellationReasonViewModel {Code = CancellationReason.ChangeOfPlans, Name = "Change of plans"},
			new CancellationReasonViewModel {Code = CancellationReason.IllnessOrInjury, Name = "Illness / injury"},
			new CancellationReasonViewModel {Code = CancellationReason.DeathInTheFamily, Name = "Death in the family"},
			new CancellationReasonViewModel {Code = CancellationReason.Other, Name = "Other"},
		};

		#endregion

		#region PriceTypes

		public static IList<PriceTypeViewModel> PriceTypes = new List<PriceTypeViewModel>
		{
			new PriceTypeViewModel {Code = PriceType.PricesAreLowerThanOriginalRate, Name = "Prices are lower than original rate"},
			new PriceTypeViewModel {Code = PriceType.PricesAreHigherThanOriginalRate, Name = "Prices are higher than original rate"},
		};

		#endregion

		#region HotelInformation Detail Options

		public static readonly IList<HotelInformationDetailsViewModel> HotelInformationDetailOptionList = new List<HotelInformationDetailsViewModel>
		{
			new HotelInformationDetailsViewModel { Code = HotelInformationDetailOptions.Default, Name = "Default"},
			new HotelInformationDetailsViewModel { Code = HotelInformationDetailOptions.HotelSummary, Name = "Hotel summary"},
			new HotelInformationDetailsViewModel { Code = HotelInformationDetailOptions.HotelDetails, Name = "Hotel details"},
			new HotelInformationDetailsViewModel { Code = HotelInformationDetailOptions.Suppliers, Name = "Suppliers"},
			new HotelInformationDetailsViewModel { Code = HotelInformationDetailOptions.RoomTypes, Name = "Room types"},
			new HotelInformationDetailsViewModel { Code = HotelInformationDetailOptions.RoomAmenities, Name = "Room amenities"},
			new HotelInformationDetailsViewModel { Code = HotelInformationDetailOptions.PropertyAmenities, Name = "Property amenities"},
			new HotelInformationDetailsViewModel { Code = HotelInformationDetailOptions.HotelImages, Name = "Hotel images"},
		};

		#endregion
	}
}
