﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using EAN.Net.Arguments;
using EAN.Net.Arguments.Common;
using EAN.Net.TestTool.Common;
using EAN.Net.TestTool.Exceptions;
using Newtonsoft.Json;

namespace EAN.Net.TestTool.ViewModels
{
	public class HotelRoomAvailabilityOptionsViewModel : HotelRoomOptionsViewModel<HotelRoomAvailabilityOptionsViewModel>
	{
		private int? _numberOfBedrooms = 1;
		private bool _isIncludeDetails;
		private bool _isIncludeRoomImages;
		private bool _isIncludeHotelFeeBreakdown;
		private readonly HotelRoomAvailabilityDetailsOptionsViewModel _detailOptions = new HotelRoomAvailabilityDetailsOptionsViewModel();

		private string _results = string.Empty;

		private int _selectedTabIndex;
		private readonly RelayCommand _searchCommand;
		private readonly RelayCommand _backCommand;

		public HotelRoomAvailabilityOptionsViewModel(RootViewModel rootModel)
			: base(rootModel)
		{
			_searchCommand = new RelayCommand(_ => CheckRoomAvailability());
			_backCommand = new RelayCommand(_ => SelectedTabIndex = 0);
		}

		public int? NumberOfBedrooms
		{
			get { return _numberOfBedrooms; }
			set
			{
				_numberOfBedrooms = value;
				NotifyPropertyChanged(x => x.NumberOfBedrooms);
			}
		}

		public bool IsIncludeDetails
		{
			get { return _isIncludeDetails; }
			set
			{
				_isIncludeDetails = value;
				NotifyPropertyChanged(x => IsIncludeDetails);
			}
		}

		public bool IsIncludeRoomImages
		{
			get { return _isIncludeRoomImages; }
			set
			{
				_isIncludeRoomImages = value;
				NotifyPropertyChanged(x => IsIncludeRoomImages);
			}
		}

		public bool IsIncludeHotelFeeBreakdown
		{
			get { return _isIncludeHotelFeeBreakdown; }
			set
			{
				_isIncludeHotelFeeBreakdown = value;
				NotifyPropertyChanged(x => IsIncludeHotelFeeBreakdown);
			}
		}

		public HotelRoomAvailabilityDetailsOptionsViewModel DetailOptions
		{
			get { return _detailOptions; }
		}

		public RelayCommand SearchCommand
		{
			get { return _searchCommand; }
		}

		public RelayCommand BackCommand
		{
			get { return _backCommand; }
		}

		public int SelectedTabIndex
		{
			get { return _selectedTabIndex; }
			set
			{
				_selectedTabIndex = value;
				NotifyPropertyChanged(x => x.SelectedTabIndex);
			}
		}

		public string Results
		{
			get { return _results; }
			set
			{
				_results = value;
				NotifyPropertyChanged(x => x.Results);
			}
		}

		private void CheckRoomAvailability()
		{
			try
			{
				Mouse.OverrideCursor = Cursors.Wait;

				var client = RootModel.CreateClient();
				var session = RootModel.CreateSession(client);

				var rooms = new List<Room>();

				foreach (var r in RoomOptions.Rooms)
				{
					var room = new Room {NumberOfAdults = r.NumberOfAdults};
					foreach (var a in r.ChildAgeOptions.ChildAges)
					{
						room.ChildAges.Add(a.Age);
					}

					rooms.Add(room);
				}

				Results = string.Empty;

				var additionalOptions = new HotelRoomAvailabilityAdditionalOptions
				{
					NumberOfBedRooms = NumberOfBedrooms,
					RateKey = RateKey,
					RoomTypeCode = RoomTypeCode,
					//SupplierType = SupplierType
				};

				var detailOptions = HotelRoomAvailabilityDetailOptions.Default;

				if (!DetailOptions.IsIncludeHotelDetails)
				{
					detailOptions &= ~ HotelRoomAvailabilityDetailOptions.HotelDetails;
				}
				if (!DetailOptions.IsIncludeRoomTypes)
				{
					detailOptions &= ~HotelRoomAvailabilityDetailOptions.RoomTypes;
				}
				if (!DetailOptions.IsIncludeRoomAmenities)
				{
					detailOptions &= ~HotelRoomAvailabilityDetailOptions.RoomAmenities;
				}
				if (!DetailOptions.IsIncludePropertyAmenities)
				{
					detailOptions &= ~HotelRoomAvailabilityDetailOptions.PropertyAmenities;
				}
				if (!DetailOptions.IsIncludeHotelImages)
				{
					detailOptions &= ~HotelRoomAvailabilityDetailOptions.HotelImages;
				}

				var result = session.GetHotelRoomAvailability(ArrivalDate, DepartureDate, HotelId ?? 0, rooms, 
					additionalOptions, detailOptions, 
					IsIncludeDetails, IsIncludeHotelFeeBreakdown, IsIncludeRoomImages);

				if (result.IsError)
					throw new EanApiException(result);

				Results = JsonConvert.SerializeObject(result, Formatting.Indented);

				SelectedTabIndex = 1;
			}
			catch (Exception exc)
			{
				RootModel.ShowErrorMessage(exc);
			}
			finally
			{
				Mouse.OverrideCursor = null;
			}
		}

		protected override string ValidateProperty(string propertyName)
		{
			if (propertyName == Prop(x => x.HotelId) && HotelId == null)
				return "Hotel Id is not filled";

			return base.ValidateProperty(propertyName);
		}
	}
}
