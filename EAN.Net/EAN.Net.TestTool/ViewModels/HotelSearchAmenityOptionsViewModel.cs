﻿using System;
using System.Linq;
using System.Reflection;

namespace EAN.Net.TestTool.ViewModels
{
	public class HotelSearchAmenityOptionsViewModel: ViewModelBase<HotelSearchAmenityOptionsViewModel>
	{
		[AttributeUsage(AttributeTargets.Property)]
		private class AmenityAttribute: Attribute { } 

		private bool _isIncludeFitnessCenter;
		private bool _isIncludeSwimmingPool;
		private bool _isIncludeComplimentaryBreakfast;
		private bool _isIncludeKitchenKitchenette;
		private bool _isIncludeMeetingFacilities;
		private bool _isIncludePetsAllowed;
		private bool _isIncludeRestaurant;
		private bool _isIncludeChildrenActivities;
		private bool _isIncludeWheelchairAccessible;

		[Amenity]
		public bool IsIncludeFitnessCenter
		{
			get { return _isIncludeFitnessCenter; }
			set
			{
				_isIncludeFitnessCenter = value;
				NotifyPropertyChanged(x => x.IsIncludeFitnessCenter);
				NotifyPropertyChanged(x => x.IsFilled);
			}
		}

		[Amenity]
		public bool IsIncludeSwimmingPool
		{
			get { return _isIncludeSwimmingPool; }
			set
			{
				_isIncludeSwimmingPool = value;
				NotifyPropertyChanged(x => x.IsIncludeSwimmingPool);
				NotifyPropertyChanged(x => x.IsFilled);
			}
		}

		[Amenity]
		public bool IsIncludeComplimentaryBreakfast
		{
			get { return _isIncludeComplimentaryBreakfast; }
			set
			{
				_isIncludeComplimentaryBreakfast = value;
				NotifyPropertyChanged(x => x.IsIncludeComplimentaryBreakfast);
				NotifyPropertyChanged(x => x.IsFilled);
			}
		}

		[Amenity]
		public bool IsIncludeKitchenKitchenette
		{
			get { return _isIncludeKitchenKitchenette; }
			set
			{
				_isIncludeKitchenKitchenette = value;
				NotifyPropertyChanged(x => x.IsIncludeKitchenKitchenette);
				NotifyPropertyChanged(x => x.IsFilled);
			}
		}

		[Amenity]
		public bool IsIncludeMeetingFacilities
		{
			get { return _isIncludeMeetingFacilities; }
			set
			{
				_isIncludeMeetingFacilities = value;
				NotifyPropertyChanged(x => x.IsIncludeMeetingFacilities);
				NotifyPropertyChanged(x => x.IsFilled);
			}
		}

		[Amenity]
		public bool IsIncludePetsAllowed
		{
			get { return _isIncludePetsAllowed; }
			set
			{
				_isIncludePetsAllowed = value;
				NotifyPropertyChanged(x => x.IsIncludePetsAllowed);
				NotifyPropertyChanged(x => x.IsFilled);
			}
		}

		[Amenity]
		public bool IsIncludeRestaurant
		{
			get { return _isIncludeRestaurant; }
			set
			{
				_isIncludeRestaurant = value;
				NotifyPropertyChanged(x => x.IsIncludeRestaurant);
				NotifyPropertyChanged(x => x.IsFilled);
			}
		}

		[Amenity]
		public bool IsIncludeChildrenActivities
		{
			get { return _isIncludeChildrenActivities; }
			set
			{
				_isIncludeChildrenActivities = value;
				NotifyPropertyChanged(x => x.IsIncludeChildrenActivities);
				NotifyPropertyChanged(x => x.IsFilled);
			}
		}

		[Amenity]
		public bool IsIncludeWheelchairAccessible
		{
			get { return _isIncludeWheelchairAccessible; }
			set
			{
				_isIncludeWheelchairAccessible = value;
				NotifyPropertyChanged(x => x.IsIncludeWheelchairAccessible);
				NotifyPropertyChanged(x => x.IsFilled);
			}
		}

		public bool IsFilled
		{
			get
			{
				var count = GetType()
					.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.SetProperty)
					.Where(p => p.PropertyType == typeof (bool) && p.GetCustomAttributes(typeof(AmenityAttribute), true).Any())
					.Select(p => (bool) p.GetValue(this, null))
					.Count(x => x);

				return count >= 3;
			}
		}
	}
}
