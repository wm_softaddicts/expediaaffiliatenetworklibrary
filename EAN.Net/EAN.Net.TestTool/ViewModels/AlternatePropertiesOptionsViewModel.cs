﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using EAN.Net.Arguments.Common;
using EAN.Net.TestTool.Common;
using EAN.Net.TestTool.Exceptions;
using Newtonsoft.Json;

namespace EAN.Net.TestTool.ViewModels
{
	public class AlternatePropertiesOptionsViewModel : ViewModelBase<AlternatePropertiesOptionsViewModel>
	{
		private readonly RootViewModel _rootModel;

		private long? _hotelId = 113690;

		private string _results = string.Empty;
		private readonly RoomOptionsViewModel _roomOptions = new RoomOptionsViewModel();
		private DateTime _arrivalDate = DateTime.Now;
		private DateTime _departureDate = DateTime.Now.AddDays(2);
		private float? _averageNightlyRate = 180;
		private PriceTypeViewModel _priceType;

		private int _selectedTabIndex;
		private readonly RelayCommand _searchCommand;
		private readonly RelayCommand _backCommand;

		public AlternatePropertiesOptionsViewModel(RootViewModel rootModel)
		{
			_rootModel = rootModel;

			PriceType = PriceTypes.First();

			_searchCommand = new RelayCommand(_ => GetAlternateProperties());
			_backCommand = new RelayCommand(_ => SelectedTabIndex = 0);
		}

		public IList<PriceTypeViewModel> PriceTypes
		{
			get { return Catalog.PriceTypes; }
		}

		public RootViewModel RootModel
		{
			get { return _rootModel; }
		}

		public long? HotelId
		{
			get { return _hotelId; }
			set
			{
				_hotelId = value;
				NotifyPropertyChanged(x => x.HotelId);
			}
		}

		public RelayCommand SearchCommand
		{
			get { return _searchCommand; }
		}

		public RelayCommand BackCommand
		{
			get { return _backCommand; }
		}

		public int SelectedTabIndex
		{
			get { return _selectedTabIndex; }
			set
			{
				_selectedTabIndex = value;
				NotifyPropertyChanged(x => x.SelectedTabIndex);
			}
		}

		public string Results
		{
			get { return _results; }
			set
			{
				_results = value;
				NotifyPropertyChanged(x => x.Results);
			}
		}

		public RoomOptionsViewModel RoomOptions
		{
			get { return _roomOptions; }
		}

		public DateTime ArrivalDate
		{
			get { return _arrivalDate; }
			set
			{
				_arrivalDate = value;
				NotifyPropertyChanged(x => x.ArrivalDate);
			}
		}

		public DateTime DepartureDate
		{
			get { return _departureDate; }
			set
			{
				_departureDate = value;
				NotifyPropertyChanged(x => x.DepartureDate);
			}
		}

		public float? AverageNightlyRate
		{
			get { return _averageNightlyRate; }
			set
			{
				_averageNightlyRate = value;
				NotifyPropertyChanged(x => x.AverageNightlyRate);
			}
		}

		public PriceTypeViewModel PriceType
		{
			get { return _priceType; }
			set
			{
				_priceType = value;
				NotifyPropertyChanged(x => x.PriceType);
			}
		}

		private void GetAlternateProperties()
		{
			try
			{
				Mouse.OverrideCursor = Cursors.Wait;

				var client = RootModel.CreateClient();
				var session = RootModel.CreateSession(client);

				var rooms = new List<Room>();

				foreach (var r in RoomOptions.Rooms)
				{
					var room = new Room { NumberOfAdults = r.NumberOfAdults };
					foreach (var a in r.ChildAgeOptions.ChildAges)
					{
						room.ChildAges.Add(a.Age);
					}

					rooms.Add(room);
				}

				Results = string.Empty;

				var result = session.GetHotelAlternateProperties(HotelId ?? 0, ArrivalDate, DepartureDate, rooms, AverageNightlyRate ?? 0, PriceType.Code);

				if (result.IsError)
					throw new EanApiException(result);

				Results = JsonConvert.SerializeObject(result, Formatting.Indented);

				SelectedTabIndex = 1;
			}
			catch (Exception exc)
			{
				RootModel.ShowErrorMessage(exc);
			}
			finally
			{
				Mouse.OverrideCursor = null;
			}
		}

		protected override string ValidateProperty(string propertyName)
		{
			if (propertyName == Prop(x => x.HotelId) && HotelId == null)
				return "Hotel Id is not filled";

			if (propertyName == Prop(x => x.AverageNightlyRate) && AverageNightlyRate == null)
				return "Average nightly rate is not filled";

			return base.ValidateProperty(propertyName);
		}
	}
}
