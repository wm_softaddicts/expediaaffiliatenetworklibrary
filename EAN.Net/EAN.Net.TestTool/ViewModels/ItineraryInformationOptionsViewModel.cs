﻿using System;
using System.Linq;
using System.Windows.Input;
using EAN.Net.Arguments;
using EAN.Net.Arguments.Response.Itinerary;
using EAN.Net.TestTool.Common;
using EAN.Net.TestTool.Exceptions;
using Newtonsoft.Json;

namespace EAN.Net.TestTool.ViewModels
{
	public class ItineraryInformationOptionsViewModel : ViewModelBase<ItineraryInformationOptionsViewModel>
	{
		private readonly RootViewModel _rootModel;
		private long? _itineraryId;
		private string _lastName;
		private string _email;
		private string _affiliateConfirmationId;
		private DateTime? _creationDateStart;
		private DateTime? _creationDateEnd;
		private DateTime? _departureDateStart;
		private DateTime? _departureDateEnd;
		private string _results;

		private int _selectedTabIndex;
		private readonly RelayCommand _searchCommand;
		private readonly RelayCommand _backCommand;

		public ItineraryInformationOptionsViewModel(RootViewModel rootModel)
		{
			_rootModel = rootModel;

			ItineraryId = 191573050;
			LastName = "Test Booking";
			Email = "test@yourSite.com";

			CreationDateStart = DateTime.Now - TimeSpan.FromDays(2);
			CreationDateEnd = DateTime.Now - TimeSpan.FromDays(1);

			_searchCommand = new RelayCommand(_ => GetItineraryInformation());
			_backCommand = new RelayCommand(_ => SelectedTabIndex = 0);
		}
		public RootViewModel RootModel
		{
			get { return _rootModel; }
		}

		public long? ItineraryId
		{
			get { return _itineraryId; }
			set
			{
				_itineraryId = value;
				NotifyPropertyChanged(x => ItineraryId);
			}
		}

		public string LastName
		{
			get { return _lastName; }
			set
			{
				_lastName = value;
				NotifyPropertyChanged(x => LastName);
			}
		}

		public string Email
		{
			get { return _email; }
			set
			{
				_email = value;
				NotifyPropertyChanged(x => Email);
			}
		}

		public string AffiliateConfirmationId
		{
			get { return _affiliateConfirmationId; }
			set
			{
				_affiliateConfirmationId = value;
				NotifyPropertyChanged(x => AffiliateConfirmationId);
			}
		}

		public DateTime? CreationDateStart
		{
			get { return _creationDateStart; }
			set
			{
				_creationDateStart = value;
				NotifyPropertyChanged(x => CreationDateStart);
				NotifyPropertyChanged(x => CreationDateEnd);
				NotifyPropertyChanged(x => DepartureDateStart);
				NotifyPropertyChanged(x => DepartureDateEnd);
			}
		}

		public DateTime? CreationDateEnd
		{
			get { return _creationDateEnd; }
			set
			{
				_creationDateEnd = value;
				NotifyPropertyChanged(x => CreationDateStart);
				NotifyPropertyChanged(x => CreationDateEnd);
				NotifyPropertyChanged(x => DepartureDateStart);
				NotifyPropertyChanged(x => DepartureDateEnd);
			}
		}

		public DateTime? DepartureDateStart
		{
			get { return _departureDateStart; }
			set
			{
				_departureDateStart = value;
				NotifyPropertyChanged(x => CreationDateStart);
				NotifyPropertyChanged(x => CreationDateEnd);
				NotifyPropertyChanged(x => DepartureDateStart);
				NotifyPropertyChanged(x => DepartureDateEnd);
			}
		}

		public DateTime? DepartureDateEnd
		{
			get { return _departureDateEnd; }
			set
			{
				_departureDateEnd = value;
				NotifyPropertyChanged(x => CreationDateStart);
				NotifyPropertyChanged(x => CreationDateEnd);
				NotifyPropertyChanged(x => DepartureDateStart);
				NotifyPropertyChanged(x => DepartureDateEnd);
			}
		}

		public RelayCommand SearchCommand
		{
			get { return _searchCommand; }
		}

		public RelayCommand BackCommand
		{
			get { return _backCommand; }
		}

		public int SelectedTabIndex
		{
			get { return _selectedTabIndex; }
			set
			{
				_selectedTabIndex = value;
				NotifyPropertyChanged(x => x.SelectedTabIndex);
			}
		}

		public string Results
		{
			get { return _results; }
			set
			{
				_results = value;
				NotifyPropertyChanged(x => x.Results);
			}
		}

		private void GetItineraryInformation()
		{
			try
			{
				Mouse.OverrideCursor = Cursors.Wait;

				var client = RootModel.CreateClient();
				var session = RootModel.CreateSession(client);

				Results = string.Empty;

				var result = session.GetHotelItineraries(ItineraryId, AffiliateConfirmationId, Email, LastName,
					creationDateStart: CreationDateStart, creationDateEnd: CreationDateEnd,
					departureDateStart: DepartureDateStart, departureDateEnd: DepartureDateEnd);

				if (result.IsError)
					throw new EanApiException(result);

				Results = JsonConvert.SerializeObject(result, Formatting.Indented);

				SelectedTabIndex = 1;
			}
			catch (Exception exc)
			{
				RootModel.ShowErrorMessage(exc);
			}
			finally
			{
				Mouse.OverrideCursor = null;
			}
		}

		protected override string ValidateProperty(string propertyName)
		{
			if (propertyName == Prop(x => x.CreationDateStart) && CreationDateStart == null && (DepartureDateStart == null || DepartureDateEnd == null))
				return "Creation Date Start is not filled";

			if (propertyName == Prop(x => x.CreationDateEnd) && CreationDateEnd == null && (DepartureDateStart == null || DepartureDateEnd == null))
				return "Creation Date End is not filled";

			if (propertyName == Prop(x => x.DepartureDateStart) && DepartureDateStart == null && (CreationDateStart == null || CreationDateEnd == null))
				return "Departure Date Start is not filled";

			if (propertyName == Prop(x => x.DepartureDateEnd) && DepartureDateEnd == null && (CreationDateStart == null || CreationDateEnd == null))
				return "Departure Date End is not filled";

			return base.ValidateProperty(propertyName);
		}
	}
}
