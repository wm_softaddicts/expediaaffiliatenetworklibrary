﻿using System;
using System.Windows.Input;
using EAN.Net.TestTool.Common;
using EAN.Net.TestTool.Exceptions;
using Newtonsoft.Json;

namespace EAN.Net.TestTool.ViewModels
{
	public class HotelRoomImagesOptionsViewModel: ViewModelBase<HotelRoomImagesOptionsViewModel>
	{
		private readonly RootViewModel _rootModel;

		private long? _hotelId = 113690;

		private string _results = string.Empty;

		private int _selectedTabIndex;
		private readonly RelayCommand _searchCommand;
		private readonly RelayCommand _backCommand;

		public HotelRoomImagesOptionsViewModel(RootViewModel rootModel)
		{
			_rootModel = rootModel;

			_searchCommand = new RelayCommand(_ => SearchImages());
			_backCommand = new RelayCommand(_ => SelectedTabIndex = 0);
		}

		public RootViewModel RootModel
		{
			get { return _rootModel; }
		}

		public long? HotelId
		{
			get { return _hotelId; }
			set
			{
				_hotelId = value;
				NotifyPropertyChanged(x => x.HotelId);
			}
		}

		public RelayCommand SearchCommand
		{
			get { return _searchCommand; }
		}

		public RelayCommand BackCommand
		{
			get { return _backCommand; }
		}

		public int SelectedTabIndex
		{
			get { return _selectedTabIndex; }
			set
			{
				_selectedTabIndex = value;
				NotifyPropertyChanged(x => x.SelectedTabIndex);
			}
		}

		public string Results
		{
			get { return _results; }
			set
			{
				_results = value;
				NotifyPropertyChanged(x => x.Results);
			}
		}

		private void SearchImages()
		{
			try
			{
				Mouse.OverrideCursor = Cursors.Wait;

				var client = RootModel.CreateClient();
				var session = RootModel.CreateSession(client);

				
				Results = string.Empty;

				var result = session.GetRoomImages(HotelId ?? 0);

				if (result.IsError)
					throw new EanApiException(result);

				Results = JsonConvert.SerializeObject(result, Formatting.Indented);

				SelectedTabIndex = 1;
			}
			catch (Exception exc)
			{
				RootModel.ShowErrorMessage(exc);
			}
			finally
			{
				Mouse.OverrideCursor = null;
			}
		}

		protected override string ValidateProperty(string propertyName)
		{
			if (propertyName == Prop(x => x.HotelId) && HotelId == null)
				return "Hotel Id is not filled";

			return base.ValidateProperty(propertyName);
		}
	}
}
