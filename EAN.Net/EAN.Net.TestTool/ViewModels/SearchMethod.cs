﻿namespace EAN.Net.TestTool.ViewModels
{
	public enum SearchMethod
	{
		Address,
		DestinationString,
		DestinationId,
		HotelIdList,
		GeographicalArea
	}
}
