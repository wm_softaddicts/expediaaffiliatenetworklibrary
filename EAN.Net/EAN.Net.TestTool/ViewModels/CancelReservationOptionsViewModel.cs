﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using EAN.Net.TestTool.Common;
using EAN.Net.TestTool.Exceptions;

namespace EAN.Net.TestTool.ViewModels
{
	public class CancelReservationOptionsViewModel : ViewModelBase<CancelReservationOptionsViewModel>
	{
		private readonly RootViewModel _rootModel;

		private long? _itineraryId;
		private string _confirmationNumber;
		private string _email;
		private CancellationReasonViewModel _cancellationReason;
		private readonly RelayCommand _cancelReservationCommand;
		private readonly RelayCommand _fillWithTestDataCommand;
		private string _result;

		public CancelReservationOptionsViewModel(RootViewModel rootModel)
		{
			_rootModel = rootModel;

			CancellationReason = CancellationReasons.First();

			_cancelReservationCommand = new RelayCommand(_ => CancelReservation());
			_fillWithTestDataCommand = new RelayCommand(_ => FillWithTestData());
		}

		public RootViewModel RootModel
		{
			get { return _rootModel; }
		}

		public IList<CancellationReasonViewModel> CancellationReasons
		{
			get { return Catalog.CancellationReasons; }
		}

		public long? ItineraryId
		{
			get { return _itineraryId; }
			set
			{
				_itineraryId = value;
				NotifyPropertyChanged(x => ItineraryId);
			}
		}

		public string ConfirmationNumber
		{
			get { return _confirmationNumber; }
			set
			{
				_confirmationNumber = value;
				NotifyPropertyChanged(x => ConfirmationNumber);
			}
		}

		public string Email
		{
			get { return _email; }
			set
			{
				_email = value;
				NotifyPropertyChanged(x => Email);
			}
		}

		public CancellationReasonViewModel CancellationReason
		{
			get { return _cancellationReason; }
			set
			{
				_cancellationReason = value;
				NotifyPropertyChanged(x => CancellationReason);
			}
		}

		public string Result
		{
			get { return _result; }
			set
			{
				_result = value;
				NotifyPropertyChanged(x => x.Result);
			}
		}

		public RelayCommand CancelReservationCommand
		{
			get { return _cancelReservationCommand; }
		}

		public RelayCommand FillWithTestDataCommand
		{
			get { return _fillWithTestDataCommand; }
		}

		private void CancelReservation()
		{
			try
			{
				Mouse.OverrideCursor = Cursors.Wait;

				var client = RootModel.CreateClient();
				var session = RootModel.CreateSession(client);

				var result = session.CancelReservation(ItineraryId, Email, ConfirmationNumber, CancellationReason.Code);

				if (result.IsError)
					throw new EanApiException(result);

				Result = result.CancellationNumber != null
					? "The reservation is cancelled"
					: "The reservation is NOT cancelled";
			}
			catch (Exception exc)
			{
				RootModel.ShowErrorMessage(exc);
			}
			finally
			{
				Mouse.OverrideCursor = null;
			}
		}

		private void FillWithTestData()
		{
			try
			{
				Mouse.OverrideCursor = Cursors.Wait;

				var client = RootModel.CreateClient();
				var session = RootModel.CreateSession(client);

				var result = session.GetHotelItineraries(creationDateStart: DateTime.Now - TimeSpan.FromDays(2),
					creationDateEnd: DateTime.Now - TimeSpan.FromDays(2));

				if (result.IsError)
					throw new EanApiException(result);

				var itineraries = result.ItineraryList.Where(it => it.HotelConfirmations.Any(c => c.ConfirmationNumber == "1234" || c.ConfirmationNumber == "1235")).ToArray();
				var itinerary = itineraries.Skip(new Random().Next(itineraries.Count())).First();
				var confirmation = itinerary.HotelConfirmations.First();

				ItineraryId = itinerary.ItineraryId;
				ConfirmationNumber = confirmation.ConfirmationNumber;
				Email = itinerary.Customer.Email;
			}
			catch (Exception exc)
			{
				RootModel.ShowErrorMessage(exc);
			}
			finally
			{
				Mouse.OverrideCursor = null;
			}	
		}

		protected override string ValidateProperty(string propertyName)
		{
			if (propertyName == Prop(x => x.ItineraryId) && ItineraryId == null)
				return "ItineraryId is not filled";

			if (propertyName == Prop(x => x.ConfirmationNumber) && string.IsNullOrEmpty(ConfirmationNumber))
				return "Confirmation Number is not filled";

			if (propertyName == Prop(x => x.Email) && string.IsNullOrEmpty(Email))
				return "Email is not filled";

			return base.ValidateProperty(propertyName);
		}
	}
}
