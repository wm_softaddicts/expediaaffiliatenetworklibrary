﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EAN.Net.TestTool.ViewModels;

namespace EAN.Net.TestTool
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			var rootModel = new RootViewModel();

			rootModel.CommonOptions.MinorRev = 28;
			rootModel.CommonOptions.CId = 55505;
			rootModel.CommonOptions.ApiKey = "ff8qz52nn63a6yc72qyse36a";
			rootModel.CommonOptions.SharedSecret = "vbHuk6bf";
			rootModel.CommonOptions.Language = rootModel.CommonOptions.Languages.FirstOrDefault();
			rootModel.CommonOptions.Currency = rootModel.CommonOptions.Currencies.FirstOrDefault();

			MainGrid.DataContext = rootModel;
		}
	}
}
